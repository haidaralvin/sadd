<?php if (empty($this->session->userdata('level'))){ 
        redirect('link/login_form');}?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Administrator</title>

    <!-- Core CSS - Include with every page -->
    <link href="<?php echo base_url();?>styles_admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>styles_admin/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="<?php echo base_url();?>styles_admin/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>styles_admin/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?php echo base_url();?>styles_admin/css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <p class="navbar-brand"><a href="<?php echo base_url();?>link/logindata">Administrator</a></p>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li><a href="<?php echo base_url();?>link/logout"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
                    
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <?php if($this->session->userdata('level') == "superadmin"){?>
                        
                        <li>
                            <a href="<?php echo base_url();?>link/see_kategori"><i class="fa fa-edit fa-fw"></i> Kategori</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>link/see_sub_kategori"><i class="fa fa-edit fa-fw"></i> Sub Kategori</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>link/see_posisi"><i class="fa fa-edit fa-fw"></i> Posisi</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>link/see_kecamatan"><i class="fa fa-edit fa-fw"></i> Kecamatan</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>link/see_admin"><i class="fa fa-edit fa-fw"></i> Admin</a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-edit fa-fw"></i> Bursa Kerja <span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
							<ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url();?>link/see_pencaker">Pencari Kerja</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>link/see_industri">Industri</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>link/see_lowongan">Lowongan</a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if($this->session->userdata('level') == "kecamatan"){?>
                        
                        <li>
                            <a href="<?php echo base_url();?>link/konfirmasi_pencaker"><i class="fa fa-edit fa-fw"></i> Konfirmasi</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>link/laporan_kecamatan"><i class="fa fa-edit fa-fw"></i> Laporan</a>
                        </li>
                        <?php } ?>

                        <?php if($this->session->userdata('level') == "disnaker"){?>
                        <li>
                            <a href=""><i class="fa fa-edit fa-fw"></i> Konfirmasi <span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                            <ul class="nav nav-second-level">
                                
                                <li>
                                    <a href="<?php echo base_url();?>link/konfirmasi_lamar">Lamar</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>link/konfirmasi_minat">Minat</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-edit fa-fw"></i> Pencari Kerja<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?php echo base_url();?>link/disnaker_sudah">Sudah Diterima</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>link/disnaker_belum">Belum Diterima</a>
                                </li>
                                
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>link/laporan_disnaker"><i class="fa fa-edit fa-fw"></i> Laporan</a>
                        </li>
                        <?php } ?>

                        <?php if($this->session->userdata('level') == "disperindag"){?>
                        <li>
                            <a href="<?php echo base_url();?>link/disperindag_industri"><i class="fa fa-edit fa-fw"></i> Industri</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>link/konfirmasi_industri"><i class="fa fa-edit fa-fw"></i> Konfirmasi</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>link/laporan_disperindag"><i class="fa fa-edit fa-fw"></i> Laporan</a>
                        </li>
                        <?php } ?>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>