<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Tentang Kami</h5>
                    <p>Bursa Kerja Malang pada dasarnya sebagai penghubung informasi antara Pencari Kerja dengan Industri. Akhir-akhir ini, Disnaker dan Disperindag mulai tergeserkan fungsinya.</p>
                    <p>Adanya sistem informasi ini diharapkan memberikan laporan secara real time data pencari kerja dan industri.</p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Layanan</h5>
                    <ul class="link-list">
                    	<li><a href="<?php echo base_url();?>home/home" style="text-decoration:none">Home</a></li>
                        <li><a href="<?php echo base_url();?>home/industri" style="text-decoration:none">Profil Industri</a></li>
                        <li><a href="<?php echo base_url();?>home/pencaker_confirm" style="text-decoration:none">Profil Pencari Kerja</a></li>
                        <li><a href="<?php echo base_url();?>home/bursa_kerja" style="text-decoration:none">Lowongan</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Disnaker</h5>
                    <address>
                     Jalan Trunojoyo Kav. 3 Kepanjen Malang.</address>
                    <p>
                        <i class="icon-phone"></i> (0341) 393933 Fax : (0341) 393932/34 <br>
                        <i class="icon-envelope-alt"></i> disnaker@malangkab.go.id
                    </p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Disperindag</h5>
                    <address>
                     Jl. Mayjend Sungkono, Malang 65132</address>
                    <p>
                        <i class="icon-phone"></i> Telp./Fax. 0341 751544 <br>
                        <i class="icon-envelope-alt"></i> disperindag@malangkab.go.id
                    </p>
            </div>
        </div>
    </div>
    <div id="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="copyright">
                        <p>
                            <span>Copyright &copy; Bursa Kerja Online 2015.</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <ul class="social-network">
                        <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url();?>styles/js/jquery.js"></script>
<script src="<?php echo base_url();?>styles/js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url();?>styles/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>styles/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url();?>styles/js/jquery.fancybox-media.js"></script>
<script src="<?php echo base_url();?>styles/js/google-code-prettify/prettify.js"></script>
<script src="<?php echo base_url();?>styles/js/portfolio/jquery.quicksand.js"></script>
<script src="<?php echo base_url();?>styles/js/portfolio/setting.js"></script>
<script src="<?php echo base_url();?>styles/js/jquery.flexslider.js"></script>
<script src="<?php echo base_url();?>styles/js/animate.js"></script>
<script src="<?php echo base_url();?>styles/js/custom.js"></script>
</body>
</html>