        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tambah Admin</h1>
                </div>
                <!-- /.col-lg-12 -->
               
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>link/pro_add_admin">
                                <div class="form-group">                                    
                                    <div class="col-sm-8">
                                        <input type="text" required="required" class="form-control" name="username" placeholder="Username">
                                    </div>
                                </div>
                                <div class="form-group">                                    
                                    <div class="col-sm-8">
                                        <input type="password" required="required" class="form-control" name="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-8">
                                  <select class="form-control" name="level">
                                    <option selected value="disnaker">Disnaker</option>
                                    <option value="disperindag">Disperindag</option>
                                    <option value="kecamatan">Kecamatan</option>                           
                                    
                                  </select>
                                  </div>
                                </div>
                                <div class="form-group">                                    
                                    <div class="col-sm-8">
                                        <input type="text" required="required" class="form-control" name="nama_admin" placeholder="Nama">
                                    </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-8">
                                    <select name="id_kecamatan" class="form-control">

                                        <?php foreach ($kecamatan as $row): ?>
                                            <option value="<?php echo $row->id_kecamatan ;?>">&nbsp;<?php echo $row->nama_kecamatan; ?></option>
                                         <?php endforeach;?>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-sm-2 col-sm-offset-2">
                                        <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                                    </div>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                