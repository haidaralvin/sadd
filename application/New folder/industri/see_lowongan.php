<section id="contact-page" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="center gap">
                  <h2>Lowongan</h2>
                </div>               
            </div>
        </div>
        <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-theme btn-lg" onclick="window.location.href='<?php echo base_url();?>industri/add_lowongan'" >Buat Lowongan</button>
                    <br/><br/>
                    <table class="table table-striped">    
                            

                                <tr class="success">
                                    <td>Nama Lowongan </td>
                                    <td>Posisi </td>
                                    <td>Tanggal Expired</td>
                                    <td>Aksi</td>

                                </tr>
                               <?php 
                                foreach($lowongan as $row)
                            { ?>
                                <tr>
                                   
                                    <td> <?php echo $row->nama_lowongan; ?> </td>
                                    <td> <?php echo $row->nama_posisi; ?> </td>

                                    <?php $today= date("Y-m-d");

                                    if($today > $row->deadline){

                                    ?>
                                    <td><font color="#CC3300"> Expired </font> </td>

                                    <?php } else{

                                    ?>
                                    <td> <?php echo $row->deadline; }?></td>
                                    
                                   <td><a href="<?php echo site_url('industri/detail_lowongan?id_lowongan='.$row->id_lowongan)?>"><span class="glyphicon glyphicon-zoom-in"></span> Detail</a></td>
                                </tr>
                                
                             
                            <?php } ?>
                               
                            </table>        
                </div>
            </div>
    </section>