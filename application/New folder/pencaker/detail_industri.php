    <section id="contact-page" class="container">
        
		<div class="row">
                <div class="col-md-9">
                    <center><img class="img-rounded" width="250" height="250" src="<?php echo base_url();?>upload/foto_industri/<?php echo $industri->foto_industri;?>"/></center>
					<table class="table table-striped" align="center">                                
                                
                                
                                <tr>
                                    
                                    <td>Nama : <?php echo $industri->nama_perusahaan; ?></td>
                                </tr>                                
                                <tr>

                                    <td>Alamat : <?php echo $industri->alamat; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Kecamatan : <?php echo $industri->nama_kecamatan;?> </td>
                                    
                                </tr>
                                
                                <tr>
                                    <td>Pendiri : <?php echo $industri->nama_pemimpin; ?></td>
                                </tr>
                                <tr>
                                    <td>Tahun Berdiri : <?php echo $industri->tahun_berdiri; ?></td>
                                </tr>
                                <tr>
                                    <td>Bidang Usaha : <?php echo $industri->nama_sub;?> </td>
                                </tr>
                                <tr>
                                    <td>Keterangan : <?php echo $industri->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Terdaftar : <?php echo $industri->tanggal; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url("pencaker/see_industri")?>"><span class="glyphicon glyphicon-home"></span>Back</a>
                                    </td>
                                </tr>
                                
                            </table>    
                </div>
            </div>
    </section>