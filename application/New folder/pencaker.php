<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pencaker extends CI_Controller {

	function __construct()
	{

		parent:: __construct();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
    	$this->output->set_header("Pragma: no-cache");

		
	}
  private $limit = 10;
	public function index()
	{
		$session = $this->session->userdata('isLogin');

		if($session == FALSE)
      {
        redirect('pencaker/login_form');
        
      }else
      {
      	redirect('pencaker/see_pencaker');
      }

	}
  

	function login_form()
  	{
    $this->form_validation->set_rules('username', 'username', 'required');
    $this->form_validation->set_rules('password', 'password', 'required');
    $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
      if($this->form_validation->run()==FALSE)
      {
      	$vals = array(
                  'img_path'   => './captcha/',
                  'img_url'    => base_url().'captcha/',
                  'img_width'  => '300',
                  'img_height' => 80,
                  'border' => 50,
                  'expiration' => 300
              );
		  $cap = create_captcha($vals);
		  $data['image'] = $cap['image'];
		  $this->session->set_userdata('mycaptcha', $cap['word']);

      	$this->load->view('header');
        $this->load->view('pencaker/login_pencaker',$data);
        $this->load->view('footer');

      }else
      {
      	if ($this->input->post('captcha') == $this->session->userdata('mycaptcha'))
      	{
	       $username = $this->input->post('username');
	       $password = md5($this->input->post('password'));
	       //$is_confrim = '1';
	       $cek = $this->m_pencaker->ambil_username($username, $password);
        
	        if($cek)
	        {
          
	          $this->session->set_userdata('isLogin', TRUE);
	          $this->session->set_userdata('username',$username);
	          $this->session->set_userdata('id_pencaker',$cek['id_pencaker']);
	          $this->session->set_userdata('nama_lengkap',$cek['nama_lengkap']);
    			  $this->session->set_userdata('tanggal_lahir',$cek['tanggal_lahir']);
    			  $this->session->set_userdata('tempat_lahir',$cek['tempat_lahir']);
	          $this->session->set_userdata('jk',$cek['jk']);
    			  $this->session->set_userdata('no_ktp',$cek['no_ktp']);
    			  $this->session->set_userdata('status',$cek['status']);
    			  $this->session->set_userdata('agama',$cek['agama']);
    			  $this->session->set_userdata('alamat',$cek['alamat']);
    			  $this->session->set_userdata('telepon',$cek['telepon']);
	          $this->session->set_userdata('email',$cek['email']);
    			  $this->session->set_userdata('keterangan',$cek['keterangan']);
    			  $this->session->set_userdata('pengalaman',$cek['pengalaman']);
	          $this->session->set_userdata('foto',$cek['foto']);
    			  $this->session->set_userdata('scanijazah',$cek['scanijazah']);
    			  $this->session->set_userdata('scanktp',$cek['scanktp']);
    			  $this->session->set_userdata('pendidikan_terakhir',$cek['pendidikan_terakhir']);
	          $this->session->set_userdata('kls',$cek['kls']);
            $this->session->set_userdata('thn',$cek['thn']);
            $this->session->set_userdata('sd',$cek['sd']);
            $this->session->set_userdata('smp',$cek['smp']);
            $this->session->set_userdata('sma',$cek['sma']);
            $this->session->set_userdata('d_ii',$cek['d_ii']);
            $this->session->set_userdata('akta_ii',$cek['akta_ii']);
            $this->session->set_userdata('akta_iii',$cek['akta_iii']);
            $this->session->set_userdata('akta_iv',$cek['akta_iv']);
            $this->session->set_userdata('akta_v',$cek['akta_v']);
    			  $this->session->set_userdata('id_kecamatan',$cek['id_kecamatan']);
    			  $this->session->set_userdata('id_posisi',$cek['id_posisi']);
	          $this->session->set_userdata('is_confirm',$cek['is_confirm']);
          
         //echo "berhasil";

          if($this->session->userdata('is_confirm') == '0'){
        	
        	echo " <script>
                alert('Maaf Anda Belum Di Konfirmasi');
                history.go(-1);
              </script>";

          
        		}else{
        			redirect('pencaker/see_pencaker');
        		}
       
	        }
	        else
	        {
	         echo " <script>
	                alert('Username Password Salah');
	                history.go(-1);
	              </script>";        
	        }
          }
	      else{
	        echo " <script>
	                alert('Captcha Salah');
	                history.go(-1);
	              </script>"; 
	      }  
      }  
	}

	function lihat_pass(){
    $this->m_pencaker->lihat_pass();
	}

	function oh_saya_lupa(){
		$this->session->sess_destroy();
		$this->load->view('header');
		$this->load->view('pencaker/lupa_password');
		$this->load->view('footer');
	}

	function result_pass(){
		$this->session->sess_destroy();
		$this->load->view('pencaker/login_form');
	}

	public function logout()
	{
		$this->session->sess_destroy();
	   
		redirect('pencaker/login_form');
	}

	public function reset()
    {
      $user = $this->session->userdata('username');
      $user = $this->session->userdata('email');
  		$user = $this->session->userdata('id_pencaker');
  		$data['pencaker'] = $this->m_pencaker->data_pencaker($user);

  		$this->load->view('pencaker/header');
  		$this->load->view('pencaker/reset_password',$data);
  		$this->load->view('pencaker/footer');
    }

    public function pro_reset()
    {
      $email= $this->input->post('email');

  		$this->load->helper('string');
  		
  		$data = array(
  					      'password' => md5($this->input->post('password'))
  		            );

  		$this->db->where('email', $email);
  		$this->db->update('pencaker', $data);

      $user = $this->session->userdata('username');
      $user = $this->session->userdata('id_pencaker');
      $id_kec = $this->session->userdata('id_kecamatan');
      $id_pos = $this->session->userdata('id_posisi');
      $data['pencaker'] = $this->m_pencaker->data_pencaker($user);
      $data['kecamatan'] = $this->m_kecamatan->ambil_kecamatan($id_kec);
      $data['posisi'] = $this->m_posisi->ambil_posisi($id_pos);
      
      $this->load->view('pencaker/header');
      $this->load->view('pencaker/see_pencaker',$data);
      $this->load->view('footer');
      echo "<script>alert('Password Berhasil Di Reset');</script>";
  }


//       public function gfp()
//       {
       
//       $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
 
//        if ($this->form_validation->run() == FALSE)
//       {
//              $this->load->view('pencaker/email_check');
//        }
//        else
//       {
//          $email= $this->input->post('email');

// $this->load->helper('string');
// $password= random_string('alnum', 12);

// $data = array(
//                'password' => $password
//             );
// $this->db->where('email', $email);
// $this->db->update('pencaker', $data);

//now we will send an email

//               $config['protocol'] = 'smtp';
//               $config['smtp_host'] = 'ssl://smtp.googlemail.com';
//               $config['smtp_port'] = 465;
//               $config['smtp_user'] = 'andredwi93@gmail.com';
//               $config['smtp_pass'] = 'andriansyah';
       
          
//               $this->load->library('email', $config);

// $this->email->from('andredwi93@gmail.com', 'andre');
// $this->email->to($email);

// $this->email->subject('Get your forgotten Password');
// $this->email->message('Please go to this link to get your password.
//        http://localhost/bursa_kerja/pencaker/get_password/'.$password );

// $this->email->send();
// echo "Please check your email address.";
//        }


//       function email_check($str)
//       {
// $query = $this->db->get_where('users', array('email' => $str), 1);
 
//       if ($query->num_rows()== 1)
//       {
//              return true;
//             }
//             else
//             {    
//              $this->form_validation->set_message('email_check', 'This Email does not exist.');
//        return false;

//       }
//    }    
// }
//   public function get_password($password=FALSE)
//   {
//       $this->load->database();
//     $this->load->helper(array('form', 'url'));
//     $this->load->library('form_validation');
  
//     $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[7]|max_length[20]|matches[passconf]|md5');
//       $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
 
//    if ($this->form_validation->run() == FALSE)
//      {
//                   echo form_open();
//                   $this->load->view('pencaker/gp_form');
//     }
//    else
//     {
//             $query=$this->db->get_where('pencaker', array('password' => $password), 1);
 
//        if ($query->num_rows() == 0)
//        {
//       show_error('Sorry!!! Invalid Request!');
//        }  
//       else
//       {
//       $data = array(
//             'password' => $this->input->post('password')
//       );
     
//       $where=$this->db->where('password', $password);
     
//       $where->update('pencaker',$data);
     
//       echo "Congratulations!";
//       }
   
//   }

//  }
//end Reset Password
	function reg_pencaker(){
    $this->load->database();
    $this->load->model("m_pendidikan","m_industri"); 
		$data['kecamatan']  =$this->m_industri->get_data_kecamatan();  
		$data['posisi']     =$this->m_industri->get_data_posisi(); 
  //  $data['pendidikan_terakhir'] = $this->m_pendidikan->get_data_pendidikan();
    $data['pendidikan_terakhir'] = $this->db->query("SELECT * from pendidikan_terakhir")->result();
    $data['bidang_keahlian'] = $this->db->query("SELECT * from bidang_keahlian")->result();
    $data['pengalaman'] = $this->db->query("SELECT * from pengalaman")->result();
    $data['gaji'] = $this->db->query("SELECT * from gaji")->result();

        //echo "<pre>";print_r($test); die();

		$this->load->view('header');
		$this->load->view('pencaker/reg_pencaker', $data);
		$this->load->view('footer');
	}

  	function upload_cv(){

    $config['upload_path'] = './upload/cv';
        $config['allowed_types'] = 'doc|docx|pdf';
        $config['max_size'] = '0';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = FALSE;
        $config['encrypt_name'] = FALSE;

        $this->upload->initialize($config);
        $field_name = 'cv';

        if (!$this->upload->do_upload($field_name)) {
          $data['error'] = $this->upload->display_errors();
          $data['kecamatan']=$this->m_industri->get_data_kecamatan();
	        $data['posisi']=$this->m_industri->get_data_posisi();   

          $data['pendidikan_terakhir'] = $this->db->query("SELECT * from pendidikan_terakhir")->result();
          $data['bidang_keahlian'] = $this->db->query("SELECT * from bidang_keahlian")->result();
          $data['pengalaman'] = $this->db->query("SELECT * from pengalaman")->result();
          $data['gaji'] = $this->db->query("SELECT * from gaji")->result();

  	      $this->load->view('header');
  	      $this->load->view('pencaker/reg_pencaker', $data);
  	      $this->load->view('footer');
         }else{
          $cv_name = $this->upload->data();
          $b=$cv_name[file_name];
         
         }

  }
	function upload_foto(){

		$config['upload_path'] = './upload/foto_pencaker';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '0';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = FALSE;

        $this->upload->initialize($config);
        $field_name = 'foto';

        if (!$this->upload->do_upload($field_name)) {
            $data['error'] = $this->upload->display_errors();
         	  $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      			$data['posisi']=$this->m_industri->get_data_posisi();

            $data['pendidikan_terakhir'] = $this->db->query("SELECT * from pendidikan_terakhir")->result();
            $data['bidang_keahlian'] = $this->db->query("SELECT * from bidang_keahlian")->result();
            $data['pengalaman'] = $this->db->query("SELECT * from pengalaman")->result();
            $data['gaji'] = $this->db->query("SELECT * from gaji")->result();

      			$this->load->view('header');
      			$this->load->view('pencaker/reg_pencaker', $data);
      			$this->load->view('footer');
         }else{
         	$image_name = $this->upload->data();
         	$b=$image_name[file_name];       
         }

	}

	 function upload_scanijazah(){

		$config['upload_path'] = './upload/scanijazah';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '0';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = FALSE;

        $this->upload->initialize($config);
        $field_name1 = 'scanijazah';

        if (!$this->upload->do_upload($field_name1)) {
            $data['error'] = $this->upload->display_errors();
            $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      			$data['posisi']=$this->m_industri->get_data_posisi();

            $data['pendidikan_terakhir'] = $this->db->query("SELECT * from pendidikan_terakhir")->result();
            $data['bidang_keahlian'] = $this->db->query("SELECT * from bidang_keahlian")->result();
            $data['pengalaman'] = $this->db->query("SELECT * from pengalaman")->result();
            $data['gaji'] = $this->db->query("SELECT * from gaji")->result();

      			$this->load->view('header');
      			$this->load->view('pencaker/reg_pencaker', $data);
      			$this->load->view('footer');
      			echo "Gambar gagal";
		
         }else{
         	$image_name1 = $this->upload->data();
         	$a=$image_name1[file_name];         	
         }

	}

	 function upload_scanktp(){

		    $config['upload_path'] = './upload/scanktp';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '0';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = FALSE;

        $this->upload->initialize($config);
        $field_name1 = 'scanktp';


        if (!$this->upload->do_upload($field_name1)) {
            $data['error'] = $this->upload->display_errors();
            $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      			$data['posisi']=$this->m_industri->get_data_posisi();

            $data['pendidikan_terakhir'] = $this->db->query("SELECT * from pendidikan_terakhir")->result();
            $data['bidang_keahlian'] = $this->db->query("SELECT * from bidang_keahlian")->result();
            $data['pengalaman'] = $this->db->query("SELECT * from pengalaman")->result();
            $data['gaji'] = $this->db->query("SELECT * from gaji")->result();

      			$this->load->view('header');
      			$this->load->view('pencaker/reg_pencaker', $data);
      			$this->load->view('footer');
      			echo "Gambar gagal";
		
         }else{
         	$image_name1 = $this->upload->data();
         	$a=$image_name1[file_name];
         	
         }

	}

	function pro_add_pencaker()
	{
		
            $data = array(
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('pass')),
                'no_ktp' => $this->input->post('no_ktp'),
                'alamat' => $this->input->post('alamat'),
                'nama_lengkap' => $this->input->post('nama_lengkap'),
                'tanggal_lahir' => $this->input->post('tanggal_lahir'),
              	'tempat_lahir' => $this->input->post('tempat_lahir'),
              	'jk' => $this->input->post('jk'),
              	'agama' => $this->input->post('agama'),
              	'alamat' => $this->input->post('alamat'),
              	'email' => $this->input->post('email'),
                'telepon' => $this->input->post('telepon'),
                'id_pendidikan' => $this->input->post('pendidikan_terakhir'),
                'kls' => $this->input->post('kls'),
                'thn' => $this->input->post('thn'),
                'sd' => $this->input->post('sd'),
                'smp' => $this->input->post('smp'),
                'sma' => $this->input->post('sma'),
                'd_ii' => $this->input->post('d_ii'),
                'akta_ii' => $this->input->post('akta_ii'),
                'akta_iii' => $this->input->post('akta_iii'),
                'akta_iv' => $this->input->post('akta_iv'),
                'akta_v' => $this->input->post('akta_v'),
                'id_pengalaman' => $this->input->post('pengalaman'),
                'keterangan' => $this->input->post('keterangan'),
                'status' => $this->input->post('status'),
                'cv' => $_FILES['cv']['name'],
                'foto' => $_FILES['foto']['name'],
                'scanijazah' => $_FILES['scanijazah']['name'],
                'scanktp' => $_FILES['scanktp']['name'],
                'id_kecamatan' => $this->input->post('id_kecamatan'),
                'id_bidang' => $this->input->post('bidang_keahlian'),
                'id_gaji' => $this->input->post('gaji'),
                'id_posisi' => $this->input->post('id_posisi')
            );
            
            if (!is_numeric($this->input->post('gaji'))) {
              echo "<script>
                alert('ERROR: Inputan Harus Berupa Angka');
                history.go(-1);
              </script>";
            }elseif(empty($_FILES['scanijazah']['name'])){
                echo " <script>
                alert('Maaf Masukkan Foto Ijazah');
                history.go(-1);
              </script>";
            }elseif(empty($_FILES['foto']['name']))
            {
                echo " <script>
                alert('Maaf Masukkan Foto Anda');
                history.go(-1);
              </script>";
            }
            elseif(empty($_FILES['scanktp']['name']))
            {
                echo " <script>
                alert('Maaf Masukkan Foto KTP');
                history.go(-1);
              </script>";
            }
            elseif(empty($_FILES['cv']['name']))
            {
                echo " <script>
                alert('Maaf Masukkan CV');
                history.go(-1);
              </script>";
            }
			elseif (isset($_POST['Submit'])) {
				echo "<pre>";
				print_r($_POST);
				echo "</pre>";
			}
			else{
				$cek_username=mysql_num_rows(mysql_query("SELECT username FROM pencaker WHERE username='$_POST[username]'"));
				$cek_email=mysql_num_rows(mysql_query("SELECT email FROM pencaker WHERE email='$_POST[email]'"));
        $cek_ktp=mysql_num_rows(mysql_query("SELECT no_ktp FROM pencaker WHERE no_ktp='$_POST[no_ktp]'"));
				// Kalau username sudah ada yang pakai
				if ($cek_username > 0){
				  	echo "<script>alert('Username sudah ada yang pakai. Ulangi lagi');history.go(-1);</script>";
				}elseif ($cek_ktp > 0) {
          echo "<script>alert('NIK sudah ada yang pakai. Ulangi lagi');history.go(-1);</script>";
        }
				elseif ($cek_email > 0) {
					echo "<script>alert('Email sudah ada yang pakai. Ulangi lagi');history.go(-1);</script>";
				}
				else{
	            $this->upload_foto();
	            $this->upload_scanijazah();
	            $this->upload_scanktp();
	            $this->upload_cv();
           
    		    $res=$this->m_pencaker->add_pencaker($data);
            
            	if($res){
                    redirect('pencaker');
            	}else{
                    echo "Insert Failed<br>";
                    redirect('pencaker/reg_pencaker');
            	}

        		}
			}
            
    }

    public function see_pencaker()
    {     
    	if($this->session->userdata('isLogin') == FALSE)
	    {
	      redirect('pencaker/login_form');
	    }
	    else
	    {       
 
      $id_pencaker = $this->session->userdata('id_pencaker');
      $data['profile'] = $this->m_pencaker->get_detail_pencaker($id_pencaker);
			
			$this->load->view('pencaker/header');
			$this->load->view('pencaker/see_pencaker',$data);
			$this->load->view('pencaker/footer');
    	}  
    } 


    //   function cetak_kartu(){
    //   $id_pencaker=$this->session->userdata('id_pencaker');
    //   $data['pencaker'] = $this->m_pencaker->data_pencaker_id($id_pencaker);
    //   $this->load->view('pencaker/kartu_kuning', $data);
    // }

	
	public function see_industri($offset=0)
    {
 
         $this->load->model("m_profile_matching"); 
         $data['industri']=$this->m_profile_matching->rekomendasi_lowongan();

    	  $this->load->view('pencaker/header');
        $this->load->view('pencaker/see_industri', $data);
        $this->load->view('pencaker/footer');
    }
	
  public function detail_industri()
    {
        $param=array('id_industri'=>$_GET['id_industri']);
        $data['industri']=$this->m_industri->get_data_industriadmin_by_id($param);
        $this->load->view('pencaker/header');
        $this->load->view('pencaker/detail_industri', $data);
        $this->load->view('footer');
    }
  public function detail_lowongan()
    {
// ambil data lowongan sesuai dengan id_lowongan yang didapatkan , yang nanti akan digunakan sebagai info di detail lowongan
        $param=array('id_lowongan'=>$_GET['id_lowongan']);
        $data['lowongan']=$this->m_pencaker->get_data_lowongan_by_id_lowongan($param);

             $this->load->view('pencaker/header');
        $this->load->view('pencaker/detail_lowongan', $data);
        $this->load->view('footer');
    }
	
	public function see_lowongan()
    {
    	
    	$data['lowongan']= $this->m_pencaker->get_data_lowongan_by_id_pencaker();
		  $this->load->view('pencaker/header');
        $this->load->view('pencaker/see_lowongan', $data);
        $this->load->view('footer');
    }
	
	public function lamar_industri()
    {       
    	$id_lowongan= array('id_lowongan'=>$_GET['id_lowongan']);
  		$data['lowongan']=$this->m_pencaker->get_data_lowongan_by_id_lowongan($id_lowongan);
  		$id_pencaker=$this->session->userdata('id_pencaker');
  		$data['pencaker']= $this->m_pencaker->get_data_industri_by_id_pencaker($id_pencaker);
        $this->load->view('pencaker/header');
        $this->load->view('pencaker/add_lamar', $data);
        $this->load->view('footer');
    } 
	
	public function pro_add_lamar()
    {

        $data1 = array(
  			  'id_lowongan' => $_POST['lowongan'],
          'id_industri' => $_POST['id_industri'],
          'id_pencaker' => $_POST['pencaker']			
        );                 
  
        $res=$this->m_lowongan->add_lamar($data1);
        
        if($res){
          // jika berhasi melamar maka redirect kepecaker
              echo "<script>alert('Terimakasih. Lamaran Anda Berhasil Ditambahkan.');
window.location='".site_url('pencaker/lowongan_posisi')."';
            </script>";
        }else{
          // jika gagal maka tampilkan alert, kemudian di redirect ke daftar lowongan
              echo "<script>alert(' Anda tidak dapat melamar lebih dari 1 kali pada lowongan yang sama.');
window.location='".site_url('pencaker/lowongan_posisi')."';
            </script>";

        }
        
    }
	
      public function see_notifikasi()
       {

/*____________________
   code start
**********************/ 
// membuat variable $id_pencaker di beri value id_pencaker dari session userdata (id_pencaker yang login saat ini)
          $id_pencaker=$this->session->userdata('id_pencaker');
// mengambil data dari table minat yang sudah di konfirmasi  disnaker, dengan parameter id_pencaker yang login saat ini.
          $data['minat']= $this->m_pencaker->get_data_minat_pencaker_confirm($id_pencaker);
// mengambil jumlah notifikasi minat yang belum dibaca, digunakan untuk menampilkan ada notifikasi baru yang  ada background merahnya di tampilan
          $data['jumlah_minat']= $this->m_pencaker->get_jumlah_minat_pencaker_confirm_unread($id_pencaker);
// mengambil data dari table lamar yang sudah di konfirmasi disnaker, dengan parameter id_pencaker yang login saat ini.
          $data['lamar']= $this->m_pencaker->get_data_lamar_pencaker_confirm($id_pencaker);
// mengambil jumlah notifikasi lamar yang belum dibaca, digunakan untuk menampilkan ada notifikasi baru yang  ada background merahnya di tampilan
          $data['jumlah_lamar']= $this->m_pencaker->get_jumlah_lamar_pencaker_confirm_unread($id_pencaker);


          $this->load->view('pencaker/header');
          $this->load->view('pencaker/see_notifikasi', $data);
          $this->load->view('footer');

       }
       public function detail_industri_minat()
    {
/*____________________
   code start
**********************/ 
//1. membuat variable $isi dan $param yang digunakan sebagai parameter saat memanggil method  edit_minat_read_by_pencaker
// sebagai penanda bahwa notifikasi sudah dibaca saya gunakan field is_read_by_pencaker untuk pencaker sedangkan yang is_read di gunakan oleh industri
          $isi=array(
            'is_read_by_pencaker'=> 1
        );
 
        $param=array('id_minat'=>$_GET['id_minat']); 

// 2. edit is_read_by_pencaker ditable minat menjadi 1.         
        $data['minat']=$this->m_pencaker->edit_minat_read_by_pencaker($param, $isi);

// 3. ambil data industri sesuai dengan id_minat yang didapatkan , yang nanti akan digunakan sebagai info di detail notifikasi
        $data['industri']=$this->m_pencaker->get_data_det_industri_notif($param);
         $this->load->view('pencaker/header');
        $this->load->view('pencaker/see_detail_notifikasi_minat', $data);
        $this->load->view('footer');
    }


    public function detail_industri_lamar()
    {
/*____________________
   code start
**********************/ 
//1. membuat variable $isi dan $param yang digunakan sebagai parameter saat memanggil method  edit_lamar_read_by_pencaker
// sebagai penanda bahwa notifikasi sudah dibaca saya gunakan field is_read_by_pencaker untuk pencaker sedangkan yang is_read di gunakan oleh industri
        $isi=array(
            'is_read_by_pencaker'=> 1
        );


        $param=array('id_lamar'=>$_GET['id_lamar']);

// 2. edit is_read_by_pencaker ditable minat menjadi 1.   
        $data['lamar']=$this->m_pencaker->edit_lamar_read_by_pencaker($param, $isi);

// 3. ambil data industri sesuai dengan id_lamar yang didapatkan , yang nanti akan digunakan sebagai info di detail notifikasi
        $data['industri']=$this->m_pencaker->get_data_det_industri_notif_lamar($param);
        $data['lowongan']=$this->m_pencaker->get_data_det_lowongan_notif_lamar($param);
        // print_r($data);
        $this->load->view('pencaker/header');
        $this->load->view('pencaker/see_detail_notifikasi_lamar', $data);
        $this->load->view('footer');
    }

    public function lowongan_posisi()
  { 

        $this->load->model("m_profile_matching"); 
    $data['lowongan']=$this->m_profile_matching->rekomendasi_lowongan();
    
    $this->load->view('pencaker/header');
    $this->load->view('pencaker/see_lowongan_home', $data);
    $this->load->view('pencaker/footer');
  }

  public function det_low_industri()
  {
    $id_industri=$_GET['id_industri'];
   
    $data['industri']=$this->m_industri->get_data_industri();
    $data['lowongan']=$this->m_lowongan->get_data_lowongan_by_id4($id_industri);
    $this->load->view('pencaker/header');
    $this->load->view('pencaker/see_det_low',$data);
    $this->load->view('footer');
  }

  public function det_low_kategori()
  {
    $param=array('id_kategori'=>$_GET['id_kategori']);
    $data['a']=$_GET['id_kategori'];
    $data['kategori']= $this->m_kategori->get_data_kategori();
    $data['sub_kategori']= $this->m_sub_kategori->get_data_sub_kategori($param) ;
    $this->load->view('pencaker/header');
    $this->load->view('pencaker/see_det_low_kategori',$data);
    $this->load->view('footer');
  }

  public function det_low_posisi()
  {
    $id_posisi=$_GET['id_posisi'];
   
    $data['posisi']=$this->m_posisi->get_data_posisi();
    $data['lowongan']=$this->m_lowongan->get_data_lowongan_by_id2($id_posisi);
        
    $this->load->view('pencaker/header');
    $this->load->view('pencaker/see_det_low',$data);
    $this->load->view('footer');
  }
  public function det_low_kecamatan()
  {
    $id_kecamatan=$_GET['id_kecamatan'];
   
    $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
    $data['lowongan']=$this->m_lowongan->get_data_lowongan_by_id3($id_kecamatan);
        
    $this->load->view('pencaker/header');
    $this->load->view('pencaker/see_det_low',$data);
    $this->load->view('footer');
  }
  public function pencaker_posisi()
  { 
    $data['pencaker'] = $this->m_pencaker->get_data_pencaker_ordinary();
    $data['posisi'] = $this->m_pencaker->get_data_posisi();
    $data['kecamatan'] = $this->m_pencaker->get_data_kecamatan();
    $this->load->view('pencaker/header');
    $this->load->view('pencaker/see_pencaker_home', $data);
    $this->load->view('footer');
  }  



  public function saran(){
      /*DATA PENCARI KERJA*/
      $id=$this->session->userdata('id_pencaker');

      $pencaker = $this->db->query("SELECT c.nama_lengkap,c.pendidikan_terakhir, c.pengalaman,k.nama_kecamatan, c.bidang_keahlian,c.pendidikan_terakhir, c.gaji,p.nama_posisi
                                    FROM pencaker c
                                    INNER JOIN kecamatan k ON c.id_kecamatan = k.id_kecamatan
                                    INNER JOIN posisi p ON c.id_posisi = p.id_posisi
                                    WHERE c.id_pencaker = $id ")->row(); 

                                    //echo "<pre>"; print_r($pencaker);

      $lowongan = $this->db->query("SELECT l.id_lowongan,i.nama_perusahaan, l.nama_lowongan,p.nama_posisi, k.nama_kecamatan,l.bidang_keahlian, l.pendidikan_terakhir,l.pengalaman, l.gaji
                                    FROM lowongan l
                                    INNER JOIN posisi p ON l.id_posisi = p.id_posisi
                                    INNER JOIN kecamatan k ON l.id_kecamatan = k.id_kecamatan
                                    INNER JOIN industri i ON l.id_industri = i.id_industri
                                    order by l.id_lowongan ASC")->result();
                                    //echo "<pre>"; print_r($lowongan); 
      
      $kriteria = array("nama_kecamatan", "bidang_keahlian", "pengalaman", "gaji", "nama_posisi", "pendidikan_terakhir"); //tambahkan kriteria

      
      //echo "<pre>"; print_r($kriteria); die();
      
      //Delete Nilai digunakan jika ada perubahan data
      $this->db->delete('hasil_perhitungan', array('id_pencaker' => $id));

      foreach ($lowongan as $data) {
              $benar = array();
              $salah = array();

              for ($i=0; $i < count($kriteria) ; $i++) { 
                  
                  if ($data->$kriteria[$i] == $pencaker->$kriteria[$i]) {
                      $benar[$kriteria[$i]] = 1;
                  }else{
                      $salah[$kriteria[$i]] = 0;
                  }
              }

              $mirip   = count($benar);
              $beda    = count($salah);

          //PROSES RUMUS SIMILARITY MEASURE
              $proses = round($mirip/($mirip+$beda),2);
             // echo "<h3>".$data->id_lowongan.". HASIL PROSES = ".$mirip."/(".$mirip." + ".$beda.") = ".$proses."</h3>";

              
          //Insert Nilai
              $hasil = array(
                  'id_pencaker' => $id ,
                  'id_lowongan' => $data->id_lowongan ,
                  'hasil' => $proses
              );

              $this->db->insert('hasil_perhitungan', $hasil); 

              //break;
          }

            $hasil_lowongan = $this->db->query("SELECT i.foto_industri, i.id_industri, i.nama_perusahaan, 
                            k.nama_kecamatan, p.nama_posisi, COUNT(l.id_lowongan) as jml
                            FROM hasil_perhitungan h
                            INNER JOIN lowongan l ON l.id_lowongan = h.id_lowongan
                            INNER JOIN industri i on l.id_industri = i.id_industri
                            INNER JOIN kecamatan k ON k.id_kecamatan = l.id_kecamatan
                            INNER JOIN posisi p ON p.id_posisi = l.id_posisi
                            WHERE h.hasil = (SELECT MAX(hasil) FROM hasil_perhitungan WHERE id_pencaker = $id)
                            GROUP BY l.id_lowongan order by jml ASC");
                            //echo "<pre>"; print_r($hasil_lowongan); die();
        $a["hasil"]       = $hasil_lowongan;
        
        $this->load->view('pencaker/header');
        $this->load->view('pencaker/saran', $a);
        $this->load->view('pencaker/footer');
    }


    public function konversi_usia()
    {
      $id_c        = $this->session->userdata('id_pencaker');
     // echo "<pre>"; print_r($id); die();

      $date = $this->db->query("SELECT tanggal_lahir FROM pencaker WHERE id_pencaker = $id_c")->row();
      $tgl_lahir = $date->tanggal_lahir;
      //$tgl_lahir1 = "1992-02-01";
      //echo "<pre>"; print_r($tgl_lahir); die();
      //echo $tgl_lahir1 = (string)$tgl_lahir; die(); 
     // echo "<pre>"; print_r($tgl_lahir); 
      $rubah = date_format(date_create($tgl_lahir), 'Y');
      $thn_skrg = date('Y');
      $usia = $thn_skrg - $rubah;
      echo 'Tanggal lahir Anda <b>'.$tgl_lahir.'</b>, Usia Anda sekarang adalah <b>'.$usia.'</b> thn';


      if ($usia >= 50 && $usia <= 64) {
        $skor = 8;
        $range = "50 - 64 tahun";
      }elseif ($usia >= 35 && $usia <= 49) {
        $skor = 7;
      }elseif ($usia >= 30 && $usia <= 34) {
        $skor = 6;
      }elseif ($usia >= 25 && $usia <= 29) {
        $skor = 5;
      }elseif ($usia >= 20 && $usia <= 24) {
        $skor = 4;
        $range = "20 - 24 tahun";
      }elseif ($usia >= 16 && $usia <= 19) {
        $skor = 3;
      }elseif ($usia >= 13 && $usia <= 15) {
        $skor = 2;
      }elseif ($usia < 13) {
        $skor = 1;
      }

      //echo $skor;
      $this->db->query("Insert INTO pencaker (usia) VALUES ('$range')");

    }


    public function profile_matching()
    {      
      $id = $this->session->userdata('id_pencaker');

      $pencaker = $this->db->query("SELECT p.id_pencaker, p.nama_lengkap, q.nama_pendidikan, r.pengalaman, k.nama_kecamatan, b.bidang_keahlian, g.gaji, s.nama_posisi
                                  FROM pencaker p
                                  INNER JOIN kecamatan k ON k.id_kecamatan = p.id_kecamatan
                                  INNER JOIN posisi s ON s.id_posisi = p.id_posisi
                                  INNER JOIN pendidikan_terakhir q ON q.id_pendidikan = p.id_pendidikan
                                  INNER JOIN pengalaman r ON r.id_pengalaman = p.id_pengalaman
                                  INNER JOIN bidang_keahlian b ON b.id_bidang = p.id_bidang
                                  INNER JOIN gaji g ON g.id_gaji = p.id_gaji
                                  WHERE p.id_pencaker = $id")->row();


      $lowongan = $this->db->query("SELECT l.id_lowongan, i.nama_perusahaan, l.nama_lowongan, p.nama_posisi, k.nama_kecamatan, b.bidang_keahlian, c.nama_pendidikan, d.pengalaman, g.gaji

                                   FROM lowongan l
                                   INNER JOIN industri i ON i.id_industri = l.id_industri
                                   INNER JOIN posisi p ON p.id_posisi = l.id_posisi
                                   INNER JOIN kecamatan k ON k.id_kecamatan = l.id_kecamatan
                                   INNER JOIN bidang_keahlian b ON b.id_bidang = l.id_bidang
                                   INNER JOIN pendidikan_terakhir c ON c.id_pendidikan = l.id_pendidikan
                                   INNER JOIN pengalaman d ON d.id_pengalaman = l.id_pengalaman
                                   INNER JOIN gaji g ON g.id_gaji = l.id_gaji
                                   order by l.id_lowongan ASC")->result();
    }



}

?>