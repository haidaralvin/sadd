        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detail Lowongan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                               <table class="table table-striped" align="center">                                
                                               
                                
                                <tr>
                                    
                                    <td>Nama Perusahaan: 
                                    <a href="<?php echo site_url('link/see_detail_industri?id_industri='.$lowongan->id_industri)?>"><?php echo $lowongan->nama_perusahaan;?></a></td>
                                </tr>                                
                                <tr>
                                    
                                    <td>Nama  Lowongan: <?php echo $lowongan->nama_lowongan; ?></td>
                                </tr>                                
                                <tr>
                                    
                                    <td>Kecamatan  : <?php echo $lowongan->nama_kecamatan; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Posisi  : <?php echo $lowongan->nama_posisi; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Gaji : <?php echo $lowongan->gaji; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Pengalaman Dibutuhkan: <?php echo $lowongan->pengalaman; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Bidang Keahlian: <?php echo $lowongan->bidang_keahlian; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Pendidikan: <?php echo $lowongan->nama_pendidikan; ?></td>
                                </tr>                 
                                <tr>
                                    <td>Tanggal: <?php echo $lowongan->tanggal; ?></td>
                                </tr>
                                <tr>
                                    <td>Deadline : <?php echo $lowongan->deadline; ?></td>
                                </tr>
                              
                                <tr>
                                    <td>Isi : <?php echo $lowongan->isi; ?></td>
                                </tr>

                                <tr>
                                    <td>
                        <a class="btn btn-primary" href="javascript:history.back()"> <span class="
                        glyphicon glyphicon-home"></span> Back</a>

                                    </td>
                                </tr>

                            </table>    
                


                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>