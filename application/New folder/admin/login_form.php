<section id="registration" class="container">
        <form class="form-horizontal center" role="form" method="post" action="<?php echo base_url();?>link/login_form">
            <div class="form-group">
            <div class="col-sm-4">
                    <input type="text" class="form-control" required="required" name="username" placeholder="Username">
            </div>
            </div>
            <div class="form-group">
            <div class="col-sm-4">
                    <input type="password" class="form-control" required="required" name="password" placeholder="Password">
            </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <?php echo $image;?>

                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <input type="text" name="captcha" class="form-control" required="required" placeholder="Captcha">

                </div>
            </div>
            <div class="form-group">
            <div class="col-sm-4">
                    <button type="submit" class="btn btn-theme">LOG IN</button>
            </div>
            </div>
           
        </form>
        
    </section><!--/#registration-->