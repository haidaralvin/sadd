<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Link extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");

    }

    private $limit = 10;

    // LOGIN
    public function login()
  {

    $session = $this->session->userdata('isLogin');
    
      if($session == FALSE)
      {
        redirect('link/login_form');
      }else
      {
       redirect('link/logindata');
      }
  }
  
  public function logindata()
  {
    if($this->session->userdata('isLogin') == FALSE)
    {
      redirect('link/login_form');
    }else
    {
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $this->load->view('admin/header');
        $this->load->view('admin/welcome');
        $this->load->view('admin/footer', $data);
    }
  } 
  
  public function login_form()
  {
    $this->form_validation->set_rules('username', 'username', 'required');
    $this->form_validation->set_rules('password', 'password', 'required');
    $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
  if($this->form_validation->run()==FALSE)
      {
        $vals = array(
                'font_path'  => './styles/fonts/Roboto-Bold.ttf',
                  'img_path'   => './captcha/',
                  'img_url'    => base_url().'captcha/',
                  'img_width'  => '300',
                  'img_height' => 100,
                  'border' => 0,
                  'expiration' => 300
              );
          $cap = create_captcha($vals);
          $data['image'] = $cap['image'];
          $this->session->set_userdata('mycaptcha', $cap['word']);

        $this->load->view('header');
        $this->load->view('admin/login_form',$data);
        $this->load->view('footer');
      }else
      {
        if ($this->input->post('captcha') == $this->session->userdata('mycaptcha'))
        {
           $username = $this->input->post('username');
           $password = MD5($this->input->post('password'));
           $cek = $this->m_login->ambilPengguna($username, $password);
            
            if($cek)
            {
              
              $this->session->set_userdata('isLogin', TRUE);
              $this->session->set_userdata('username',$username);
              $this->session->set_userdata('id_admin',$cek['id_admin']);
              $this->session->set_userdata('nama_admin',$cek['nama_admin']);
              $this->session->set_userdata('level',$cek['level']);
              $this->session->set_userdata('id_kecamatan',$cek['id_kecamatan']);
             
              redirect('link/logindata');
            }
            else
            {
             echo " <script>
                    alert('Maaf, Username / Password Salah');
                    history.go(-1);
                  </script>";        
            }

        }
        else{
            echo " <script>
                    alert('Captcha Salah');
                    history.go(-1);
                  </script>"; 
        }  
    }
}

  
  public function logout()
  {
   $this->session->sess_destroy();
   
   redirect('link/login_form');
  }

function oh_saya_lupa(){
        $this->session->sess_destroy();
        $this->load->view('header');
        $this->load->view('admin/lupa_password');
        $this->load->view('footer');
    }
    // END LOGIN


    //KATEGORI

    public function see_kategori($offset=0)
    {       
        $data['offset'] = $offset;
        $data["data"] = $this->m_kategori->get_datakategori($this->limit, $offset);

        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $config["base_url"] = site_url("link/see_kategori");
        
        $this->pagination->initialize($config);

        $this->load->view('admin/header');
        $this->load->view('admin/see_kategori', $data);
        $this->load->view('admin/footer');
    }    
    public function get_data_kategori()
    {       
        $res=$this->m_kategori->get_datakategori();
        echo json_encode($res);
    }

    public function add_kategori()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/add_kategori');
        $this->load->view('admin/footer');
    }
    public function edit_kategori()
    {
        $param=array('id_kategori'=>$_GET['id_kategori']);
        $data['kategori']=$this->m_kategori->get_data_kategori_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/edit_kategori', $data);
        $this->load->view('admin/footer');
    }
    
    public function pro_add_kategori()
    {

        $data = array(
            'nama_kategori' => $_POST['nama_kategori']
        );                  

        $res=$this->m_kategori->add_kategori($data);
        //echo json_encode($res);
        if($res){
                redirect('link/see_kategori');
        }else{
                echo "Insert Failed<br>";
                echo '<a href="'.site_url('link/add_kategori').'">Back</a>';
        }
        
    }

    function pro_edit_kategori()
    {
        $data=array(
            'id_kategori'=>$_POST['id_kategori'],
            'nama_kategori'=>$_POST['nama_kategori']
        );
        $res=$this->m_kategori->edit_kategori($data);
        //echo json_encode($res);
        if($res){
                redirect('link/see_kategori');
        }else{
                echo "Update Failed<br>";
                echo '<a href="'.site_url('link/edit_kategori').'">Back</a>';
        }
    }

    function delete_kategori()
    {
        $data=array(
                'id_kategori'=>$_GET['id_kategori']
        );
        $res=$this->m_kategori->delete_kategori($data);
        if($res){
                redirect('link/see_kategori');
        }else{
                echo "Delete Failed <br>";
                echo '<a href="'.site_url('link/see_kategori').'">Back</a>';
        }
    }

    //END KATEGORI


    // SUB KATEGORI

    public function see_sub_kategori($offset=0)
    {   
        $data['offset'] = $offset;
        $data["data"] = $this->m_sub_kategori->get_data_subkategori($this->limit, $offset);
        
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $config["base_url"] = site_url("link/see_sub_kategori");
        
        $this->pagination->initialize($config);
   
        $this->load->view('admin/header');
        $this->load->view('admin/see_sub_kategori', $data);
        $this->load->view('admin/footer');
    }    
    public function get_data_sub_kategori()
    {       
        $res=$this->m_sub_kategori->get_data_subkategori();
        echo json_encode($res);
    }

    public function add_sub_kategori()
    {
        $data['kategori']=$this->m_kategori->get_data_kategori();
        $this->load->view('admin/header');
        $this->load->view('admin/add_sub_kategori',$data);
        $this->load->view('admin/footer');
    }
    public function edit_sub_kategori()
    {
        $param=array('id_subkategori'=>$_GET['id_subkategori']);
        $data['sub_kategori']=$this->m_sub_kategori->get_data_sub_kategori_by_id($param);
        $data['kategori']=$this->m_kategori->get_data_kategori();
        $this->load->view('admin/header');
        $this->load->view('admin/edit_sub_kategori', $data);
        $this->load->view('admin/footer');
    }
    
    public function pro_add_sub_kategori()
    {

        $data = array(
            'id_kategori' => $_POST['id_kategori'],
            'nama_sub' => $_POST['nama_sub']
        );                  

        $res=$this->m_sub_kategori->add_sub_kategori($data);
        
        if($res){
                redirect('link/see_sub_kategori');
        }else{
                echo "Insert Failed<br>";
                echo '<a href="'.site_url('link/add_sub_kategori').'">Back</a>';
        }
        
    }

    function pro_edit_sub_kategori()
    {
        $data=array(
            'id_subkategori'=>$_POST['id_subkategori'],
            'id_kategori'=>$_POST['id_kategori'],
            'nama_sub'=>$_POST['nama_sub']
        );
        $res=$this->m_sub_kategori->edit_sub_kategori($data);
        //echo json_encode($res);
        if($res){
                redirect('link/see_sub_kategori');
        }else{
                echo "Update Failed<br>";
                echo '<a href="'.site_url('link/edit_sub_kategori').'">Back</a>';
        }
    }

    function delete_sub_kategori()
    {
        $data=array(
                'id_subkategori'=>$_GET['id_subkategori']
        );
        $res=$this->m_sub_kategori->delete_sub_kategori($data);
        if($res){
                redirect('link/see_sub_kategori');
        }else{
                echo "Delete Failed <br>";
                echo '<a href="'.site_url('link/see_sub_kategori').'">Back</a>';
        }
    }

    //END SUB KATEGORI

    //POSISI

    public function see_posisi($offset=0)
    {       
        $data['offset'] = $offset;
        $data["data"] = $this->m_posisi->get_all_posisi($this->limit, $offset);
        
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $config["base_url"] = site_url("link/see_posisi");
        
        $this->pagination->initialize($config);

        $this->load->view('admin/header');
        $this->load->view('admin/see_posisi', $data);
        $this->load->view('admin/footer');
    }    
    public function get_data_posisi()
    {       
        $res=$this->m_posisi->get_all_posisi();
        echo json_encode($res);
    }

    public function add_posisi()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/add_posisi');
        $this->load->view('admin/footer');
    }
    public function edit_posisi()
    {
        $param=array('id_posisi'=>$_GET['id_posisi']);
        $data['posisi']=$this->m_posisi->get_data_posisi_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/edit_posisi', $data);
        $this->load->view('admin/footer');
    }
    
    public function pro_add_posisi()
    {

        $data = array(
            'nama_posisi' => $_POST['nama_posisi']
        );                  

        $res=$this->m_posisi->add_posisi($data);
        //echo json_encode($res);
        if($res){
                redirect('link/see_posisi');
        }else{
                echo "Insert Failed<br>";
                echo '<a href="'.site_url('link/add_posisi').'">Back</a>';
        }
        
    }

    function pro_edit_posisi()
    {
        $data=array(
            'id_posisi'=>$_POST['id_posisi'],
            'nama_posisi'=>$_POST['nama_posisi']
        );
        $res=$this->m_posisi->edit_posisi($data);
        //echo json_encode($res);
        if($res){
                redirect('link/see_posisi');
        }else{
                echo "Update Failed<br>";
                echo '<a href="'.site_url('link/edit_posisi').'">Back</a>';
        }
    }

    function delete_posisi()
    {
        $data=array(
                'id_posisi'=>$_GET['id_posisi']
        );
        $res=$this->m_posisi->delete_posisi($data);
        if($res){
                redirect('link/see_posisi');
        }else{
                echo "Delete Failed <br>";
                echo '<a href="'.site_url('link/see_posisi').'">Back</a>';
        }
    }

    //END POSISI


//KECAMATAN

    public function see_kecamatan($offset=0)
    {   
        $data['offset'] = $offset;
        $data["data"] = $this->m_kecamatan->get_data_kecamatan_paging($this->limit, $offset);
        
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $config["base_url"] = site_url("link/see_kecamatan");
        
        $this->pagination->initialize($config);

        $this->load->view('admin/header');
        $this->load->view('admin/see_kecamatan', $data);
        $this->load->view('admin/footer');
    }    
    public function get_data_kecamatan()
    {       
        $res=$this->m_kecamatan->get_data_kecamatan();
        echo json_encode($res);
    }

    public function add_kecamatan()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/add_kecamatan');
        $this->load->view('admin/footer');
    }
    
    public function edit_kecamatan()
    {
        $param=array('id_kecamatan'=>$_GET['id_kecamatan']);
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/edit_kecamatan', $data);
        $this->load->view('admin/footer');
    }
    
    public function pro_add_kecamatan()
    {

        $data = array(
            'nama_kecamatan' => $_POST['nama_kecamatan']
        );                  

        $res=$this->m_kecamatan->add_kecamatan($data);
        //echo json_encode($res);
        if($res){
                redirect('link/see_kecamatan');
        }else{
                echo "Insert Failed<br>";
                echo '<a href="'.site_url('link/add_kecamatan').'">Back</a>';
        }
        
    }

    function pro_edit_kecamatan()
    {
        $data=array(
            'id_kecamatan'=>$_POST['id_kecamatan'],
            'nama_kecamatan'=>$_POST['nama_kecamatan']
        );
        $res=$this->m_kecamatan->edit_kecamatan($data);
        if($res){
                redirect('link/see_kecamatan');
        }else{
                echo "Update Failed<br>";
                echo '<a href="'.site_url('link/edit_kecamatan').'">Back</a>';
        }
    }

    function delete_kecamatan()
    {
        $data=array(
                'id_kecamatan'=>$_GET['id_kecamatan']
        );
        $res=$this->m_kecamatan->delete_kecamatan($data);
        if($res){
                redirect('link/see_kecamatan');
        }else{
                echo "Delete Failed <br>";
                echo '<a href="'.site_url('link/see_kecamatan').'">Back</a>';
        }
    }

    //END KECAMATAN

    //ADMIN
    public function see_admin($offset = 0)
    {       
        
        $data['offset'] = $offset;
        $data["data"] = $this->m_admin->get_data_admin($this->limit, $offset);
        $config["base_url"] = site_url("link/see_admin");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);

        
        $this->load->view('admin/header');
        $this->load->view('admin/see_admin', $data);
        $this->load->view('admin/footer');
    }    
    public function get_data_admin()
    {       
        $res=$this->m_admin->get_data_admin();
        echo json_encode($res);
    }

    public function add_admin()
    {
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $this->load->view('admin/header');
        $this->load->view('admin/add_admin', $data);
        $this->load->view('admin/footer');
    }
    public function edit_admin()
    {
        $param=array('id_admin'=>$_GET['id_admin']);
        $data['admin']=$this->m_admin->get_data_admin_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/edit_admin', $data);
        $this->load->view('admin/footer');
    }
    
    public function pro_add_admin()
    {

        $data = array(
            'username' => $_POST['username'],
            'password' => md5($this->input->post('password')),
            'level' => $_POST['level'],
            'nama_admin' => $_POST['nama_admin']
        );                  

        $res=$this->m_admin->add_admin($data);
        //echo json_encode($res);
        if($res){
                redirect('link/see_admin');
        }else{
                echo "Insert Failed<br>";
                echo '<a href="'.site_url('link/add_admin').'">Back</a>';
        }
        
    }

    function pro_edit_admin()
    {
        $data=array(
            'id_admin'=>$_POST['id_admin'],
            'username' => $_POST['username'],
            'password' => md5($this->input->post('password')),
            'level' => $_POST['level'],
            'nama_admin' => $_POST['nama_admin']
        );
        $res=$this->m_admin->edit_admin($data);
        //echo json_encode($res);
        if($res){
                redirect('link/see_admin');
        }else{
                echo "Update Failed<br>";
                echo '<a href="'.site_url('link/edit_admin').'">Back</a>';
        }
    }

    function delete_admin()
    {
        $data=array(
                'id_admin'=>$_GET['id_admin']
        );
        $res=$this->m_admin->delete_admin($data);
        if($res){
                redirect('link/see_admin');
        }else{
                echo "Delete Failed <br>";
                echo '<a href="'.site_url('link/see_admin').'">Back</a>';
        }
    }

    //END ADMIN

    // PENCAKER

    public function see_pencaker($offset=0)
    {       
        $data['offset'] = $offset;
        $data["data"] = $this->m_pencaker->get_data_pencaker_confirmed($this->limit, $offset);
        $config["base_url"] = site_url("link/see_pencaker");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);
        $this->load->view('admin/header');
        $this->load->view('admin/see_pencaker', $data);
        $this->load->view('admin/footer');
    }    
    public function get_data_pencaker()
    {       
        $res=$this->m_pencaker->get_data_pencaker();
        echo json_encode($res);
    }
    public function see_detail_pencaker()
    {
        $param=array('id_pencaker'=>$_GET['id_pencaker']);
        $data['pencaker']=$this->m_pencaker->get_data_pencaker_by_id($param);
             $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();

        $this->load->view('admin/header');
        $this->load->view('admin/see_detail_pencaker', $data);
        $this->load->view('admin/footer');
    }
    function delete_pencaker()
    {
        $data=array(
                'id_pencaker'=>$_GET['id_pencaker']
        );
        $res=$this->m_pencaker->delete_pencaker($data);
        if($res){
                redirect('link/konfirmasi_pencaker');
        }else{
                echo "Delete Failed <br>";
                echo '<a href="'.site_url('link/konfirmasi_pencaker').'"></a>';
        }
    }

    // END PENCAKER

    // INDUSTRI

    public function see_industri($offset=0)
    {   
        $data['offset'] = $offset;
        $data["data"] = $this->m_industri->get_all_industri($this->limit, $offset);
        $config["base_url"] = site_url("link/see_industri");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);

        $this->load->view('admin/header');
        $this->load->view('admin/see_industri', $data);
        $this->load->view('admin/footer');
    }    
    public function get_data_industri()
    {       
        $res=$this->m_industri->get_all_industri();
        echo json_encode($res);
    }
    public function see_detail_industri()
    {
        
        $param=array('id_industri'=>$_GET['id_industri']);
        $data['industri']=$this->m_industri->get_data_industriadmin_by_id($param);
        
        $this->load->view('admin/header');
        $this->load->view('admin/see_detail_industri', $data);
        $this->load->view('admin/footer');
    }

    // END INDUSTRI

    // LOWONGAN

    public function see_lowongan($offset=0)
    {    
        $data['offset'] = $offset;
        
        $data["data"] = $this->m_lowongan->get_data_lowongan($this->limit, $offset);
        $config["base_url"] = site_url("link/see_lowongan");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);

        $this->load->view('admin/header');
        $this->load->view('admin/see_lowongan', $data);
        $this->load->view('admin/footer');
    }    
    public function get_data_lowongan()
    {       
        $res=$this->m_lowongan->get_data_lowongan();
        echo json_encode($res);
    }
    public function see_detail_lowongan()
    {
        $param=array('id_lowongan'=>$_GET['id_lowongan']);
        $data['lowongan']=$this->m_lowongan->get_data_lowongan_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/see_detail_lowongan', $data);
        $this->load->view('admin/footer');
    }
    function delete_lowongan()
    {
        $data=array(
                'id_lowongan'=>$_GET['id_lowongan']
        );
        $res=$this->m_lowongan->delete_lowongan($data);
        if($res){
                redirect('link/see_lowongan');
        }else{
                echo "Delete Failed <br>";
                echo '<a href="'.site_url('link/see_lowongan').'">Back</a>';
        }
    }

    // LOWONGAN


// END SUPERADMIN


    // KECAMATAN

    public function konfirmasi_pencaker($offset = 0)

    {

        $id_kecamatan = $this->session->userdata('id_kecamatan');
        $data['offset'] = $offset;
        $data["data"] = $this->m_pencaker->get_data_pencaker_kec_toconfirm($this->limit, $offset, $id_kecamatan);
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $config["base_url"] = site_url("link/konfirmasi_pencaker");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;

        $this->pagination->initialize($config);

        //echo $id_kecamatan;

        $this->load->view('admin/header');
        $this->load->view('admin/konfirmasi_pencaker', $data);
        $this->load->view('admin/footer'); 
    }    
    
    public function konfirmasi_detail_pencaker()
    {
        $param=array('id_pencaker'=>$_GET['id_pencaker']);
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $data['pencaker']=$this->m_pencaker->get_data_pencaker_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/konfirmasi_detail_pencaker', $data);
        $this->load->view('admin/footer');
    }
    function pro_konfirmasi_pencaker()
    {   
        $date = date('Y-m-d');
        $data=array(
            'id_pencaker'=>$_GET['id_pencaker'],
            'is_confirm' => '1',
            'tgl_conf' => $date
        );
        $res=$this->m_pencaker->edit_pencaker($data);
        //echo json_encode($res);
        if($res){
                redirect('link/konfirmasi_pencaker');
        }else{
                echo "Update Failed<br>";
                echo '<a href="'.site_url('link/konfirmasi_detail_pencaker').'">Back</a>';
        }
    }
    public function laporan_kecamatan($offset=0)
    {       
        $data['offset'] = $offset;
        $data["data"] = $this->m_pencaker->get_data_pencaker_paging($this->limit, $offset);
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $config["base_url"] = site_url("link/laporan_kecamatan");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);
        $this->load->view('admin/header');
        $this->load->view('admin/laporan_kecamatan', $data);
        $this->load->view('admin/footer');
    }

    public function laporan_kecamatan_bytgl($offset=0)
    {    
        $awal= $_POST['awal'];
        $akhir = $_POST['akhir'];       
        $data['offset'] = $offset;
        $data["data"] = $this->m_pencaker->get_data_pencaker_paging_by_tgl($this->limit, $offset, $awal, $akhir);
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $config["base_url"] = site_url("link/laporan_kecamatan");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);
        $this->load->view('admin/header');
        $this->load->view('admin/laporan_kecamatan', $data);
        $this->load->view('admin/footer');
    }

    public function cetak_kartu(){
      $id_pencaker = $_GET['id_pencaker'];
      $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
      $data['pencaker'] = $this->m_pencaker->data_pencaker_id($id_pencaker);
      $this->load->view('admin/kartu_kuning', $data);
    }

    public function lap_detail_pencaker()
    {
        $id_pencaker =array('id_pencaker'=>$_GET['id_pencaker']);
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $data['pencaker']=$this->m_pencaker->get_data_pencaker_by_id($id_pencaker);
        $this->load->view('admin/header');
        $this->load->view('admin/detail_pencaker_laporan', $data);
        $this->load->view('admin/footer');
    }
    // END KECAMATAN

    //DISNAKER    

    public function laporan_disnaker()
    {       
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $data['pencaker']=$this->m_pencaker->get_data_pencaker();
        $this->load->view('admin/header');
        $this->load->view('admin/laporan_disnaker', $data);
        $this->load->view('admin/footer');
    }  

    public function disnaker_belum($offset=0)
    {       
        $data['offset'] = $offset;
        $data["data"] = $this->m_pencaker->get_data_pencaker_no_rekrut($this->limit, $offset);
        $config["base_url"] = site_url("link/disnaker_belum");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);
        $this->load->view('admin/header');
        $this->load->view('admin/disnaker_belum', $data);
        $this->load->view('admin/footer');
    }    
    public function disnaker_sudah($offset=0)
    {       
        $data['offset'] = $offset;
        $data["data"] = $this->m_pencaker->get_data_pencaker_yes_rekrut($this->limit, $offset);
        $config["base_url"] = site_url("link/disnaker_sudah");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);
        $this->load->view('admin/header');
        $this->load->view('admin/disnaker_sudah', $data);
        $this->load->view('admin/footer');
    }
    
    public function disnaker_detail()
    {
        $param=array('id_pencaker'=>$_GET['id_pencaker']);
        $data['pencaker']=$this->m_pencaker->get_data_pencaker_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/disnaker_detail', $data);
        $this->load->view('admin/footer');
    }

    public function disnaker_detail_sudah()
    {
        $param=array('id_pencaker'=>$_GET['id_pencaker']);
        $data['pencaker']=$this->m_pencaker->get_data_pencaker_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/disnaker_detail_sudah', $data);
        $this->load->view('admin/footer');
    }

    public function konfirmasi_lamar($offset = 0)
    {       

        $data['offset'] = $offset;
        $data["data"] = $this->m_pencaker->get_data_lamar($this->limit, $offset);
        $config["base_url"] = site_url("link/konfirmasi_lamar");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;

        $this->pagination->initialize($config);

        $this->load->view('admin/header');
        $this->load->view('admin/konfirmasi_lamar', $data);
        $this->load->view('admin/footer');
    }   

    public function konfirmasi_minat($offset = 0)
    {       

        $data['offset'] = $offset;
        $data["data"] = $this->m_pencaker->get_data_minat($this->limit, $offset);
        $config["base_url"] = site_url("link/konfirmasi_minat");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;

        $this->pagination->initialize($config);

        $this->load->view('admin/header');
        $this->load->view('admin/konfirmasi_minat', $data);
        $this->load->view('admin/footer');
    }   
    function pro_konfirmasi_lamar()
    {
        $data=array(
            'id_lamar'=>$_GET['id_lamar'],
            'is_confirm' => '1'
        );

        $res=$this->m_pencaker->edit_lamar($data);
        $lamar=$this->m_pencaker->get_lamar_by_id($_GET['id_lamar']);
        $lowongan=$this->m_lowongan->get_data_lowongan_by_id(array('id_lowongan'=>$lamar->id_lowongan));
        $industri=$this->m_industri->get_data_industri_by_id(array('id_industri'=>$lamar->id_industri));
        $pencaker=$this->m_pencaker->get_detail_pencaker($lamar->id_pencaker);
        //echo json_encode($res);
        if($res){

                redirect('link/konfirmasi_lamar');
        }else{
                echo "Update Failed<br>";
                echo '<a href="'.site_url('link/konfirmasi_lamar').'">Back</a>';
        }
    }
    function pro_konfirmasi_minat()
    {
        $data=array(
            'id_minat'=>$_GET['id_minat'],
            'is_confirm' => '1'
        );
        $res=$this->m_pencaker->edit_minat($data);

        $minat=$this->m_pencaker->get_lamar_by_id($_GET['id_minat']);
        $industri=$this->m_industri->get_data_industri_by_id(array('id_industri'=>$minat->id_industri));
        $pencaker=$this->m_pencaker->get_detail_pencaker($minat->id_pencaker);

        //echo json_encode($res);
        if($res){
// send email ke industri
         $receiver=array($industri->email);
         $subject="Minat anda ke pencaker ".$pencaker->nama_lengkap."  Telah disetujui Disnaker";
          $message='  <div style="width:800px;margin:10px auto 0px; padding:10px;background:#f1f1f1; border:solid 1px #ddd; font-size:14px;color:#555;font-family: arial;"> 
<p style="font-size:14px;color:#555;font-family: arial;">
Hello '.$industri->nama_perusahaan.',
 <br/>Minat anda ke  <b>'.$pencaker->nama_lengkap.'</b> telah disetujui disnaker. <br/> Informasi pencaker sebagai berikut :
<br/>   </p>

<table style="font-size:14px;color:#555; padding:10px;" width="100%" bgcolor="#FFFFFF" border="0" cellpadding="5" cellspacing="0">

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Nama Lengkap
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$pencaker->nama_lengkap.'
  </td>
</tr>
<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Email
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$pencaker->email.'
  </td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Telepon
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$pencaker->telepon.'
  </td>
</tr>
<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Kecamatan
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$pencaker->nama_kecamatan.'
  </td>
</tr>
<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Usia
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.getOld($pencaker->tanggal_lahir).' Tahun
  </td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Posisi
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$pencaker->nama_posisi.'
  </td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
Gaji diharapkan
</td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$pencaker->gaji.'
</td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
Pengalaman
</td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$pencaker->pengalaman.'
</td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
Bidang Keahlian
</td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$pencaker->bidang_keahlian.'
</td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
Pendidikan</td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$pencaker->nama_pendidikan.'
</td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Keterangan
  </td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$pencaker->keterangan.'
  </td>
</tr>

</table>

<p style="font-size:14px;color:#555;font-family: arial;">
Mohon segera menghubungi pencaker tersebut untuk informasi lebih lanjut. Demikian, Terima kasih.
</p>

</div>';

              sendMail($receiver,$subject,$message);
 


// send email ke pencaker
              $receiver=array($pencaker->email);
             $subject="Perusahaan ".$industri->nama_perusahaan." berminat kepada anda ";

              $message='  <div style="width:800px;margin:10px auto 0px; padding:10px;background:#f1f1f1; border:solid 1px #ddd; font-size:14px;color:#555;font-family: arial;"> 

<p style="font-size:14px;color:#555;font-family: arial;">
Hello '.$pencaker->nama_lengkap.',
<br/>Perusahaan<b> '.$industri->nama_perusahaan.'</b> Berminat ke anda. Informasi perusahaan sebagai berikut : <br/>
</p>
<table style="font-size:14px;color:#555; padding:10px;" width="100%" bgcolor="#FFFFFF" border="0" cellpadding="5" cellspacing="0">

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Nama Perusahaan
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$industri->nama_perusahaan.'
  </td>
</tr>
<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Nama Pimpinan
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$industri->nama_pemimpin.'
  </td>
</tr>
<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Tahun Berdiri
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$industri->tahun_berdiri.'
  </td>
</tr>
<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Email
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$industri->email.'
  </td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Telepon
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$industri->telepon.'
  </td>
</tr>
<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Kecamatan
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$industri->nama_kecamatan.'
  </td>
</tr>


<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Alamat
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$industri->alamat.'
  </td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
kategori</td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$industri->nama_sub.'
</td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Keterangan
  </td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$industri->keterangan.'
  </td>
</tr>

</table>

<p style="font-size:14px;color:#555;font-family: arial;">
Selanjutnya silahkan tunggu perusahaan tersebut menghubungi anda.
Demikian, Terima kasih.
</p>

</div>';
// print_r($message);
              sendMail($receiver,$subject,$message);


                redirect('link/konfirmasi_minat');
        }else{
                echo "Update Failed<br>";
                echo '<a href="'.site_url('link/konfirmasi_minat').'">Back</a>';
        }
    }

    // END DISNAKER

    // DISPERINDAG

    public function konfirmasi_industri($offset=0)
    {       
        $data['offset'] = $offset;
        $data["data"] = $this->m_industri->get_data_industri_konfirmasi($this->limit, $offset);
        $config["base_url"] = site_url("link/konfirmasi_industri");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);
        $this->load->view('admin/header');
        $this->load->view('admin/konfirmasi_industri', $data);
        $this->load->view('admin/footer');
    }

    
    public function disperindag_industri($offset=0)
    {       
        $data['offset'] = $offset;
        $data["data"] = $this->m_industri->get_data_industri_disperindag($this->limit, $offset);
        $config["base_url"] = site_url("link/disperindag_industri");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);
        $this->load->view('admin/header');
        $this->load->view('admin/disperindag_industri', $data);
        $this->load->view('admin/footer');
    }

    public function laporan_disperindag($offset=0)
    {       
        $data['offset'] = $offset;
        $data["data"] = $this->m_industri->get_data_industri_disperindag($this->limit, $offset);
        $config["base_url"] = site_url("link/laporan_disperindag");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);

        $this->load->view('admin/header');
        $this->load->view('admin/laporan_disperindag', $data);
        $this->load->view('admin/footer');
    }

     public function laporan_disperindag_bytgl($offset=0)
    {    
        $awal= $_POST['awal'];
        $akhir = $_POST['akhir'];       
        $data['offset'] = $offset;
        $data["data"] = $this->m_industri->get_data_industri_disperindag_by_tgl($this->limit, $offset, $awal, $akhir);
        $config["base_url"] = site_url("link/laporan_disperindag");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);

        $this->load->view('admin/header');
        $this->load->view('admin/laporan_disperindag', $data);
        $this->load->view('admin/footer');
    }  

    public function disperindag_detail_industri()
    {       
        $param=array('id_industri'=>$_GET['id_industri']);
        $data['sub_kategori']=$this->m_sub_kategori->get_all_sub_kategori();
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $data['industri']=$this->m_industri->get_data_industriadmin_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/disperindag_detail_industri', $data);
        $this->load->view('admin/footer');
    }  
    
    public function konfirmasi_detail_industri()
    {
        $param=array('id_industri'=>$_GET['id_industri']);
        $data['sub_kategori']=$this->m_sub_kategori->get_all_sub_kategori();
        $data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
        $data['industri']=$this->m_industri->get_data_industriadmin_by_id($param);
        $this->load->view('admin/header');
        $this->load->view('admin/konfirmasi_detail_industri', $data);
        $this->load->view('admin/footer');
    }
    function pro_konfirmasi_industri()
    {   $date = date('Y-m-d');
        $data=array(
            'id_industri'=>$_GET['id_industri'],
            'is_confirm' => '1',
            'tgl_conf' => $date
        );
        $res=$this->m_industri->edit_industri($data);
        
        if($res){
                redirect('link/konfirmasi_industri');
        }else{
                echo "Update Failed<br>";
                echo '<a href="'.site_url('link/konfirmasi_detail_industri').'">Back</a>';
        }
    }


    function delete_industri()
    {
        $data=array(
                'id_industri'=>$_GET['id_industri']
        );
        $res=$this->m_industri->delete_industri($data);
        if($res){
                redirect('link/disperindag_industri');
        }else{
                echo "Delete Failed <br>";
                echo '<a href="'.site_url('link/konfirmasi_industri').'"></a>';
        }
    }

    // END DISPERINDAG


    //PENDIDIKAN TERAKHIR
    public function get_data_pendidikan()
    {       
        $res=$this->m_pendidikan->ambil_pendidikan();
        echo json_encode($res);
    }



    //start perhitungan cbr

    function cbr(){
        $data[] = "selamat datang";
        $this->load->view('admin/header');
        $this->load->view('admin/view_cbr', $data);
        $this->load->view('admin/footer');
    }

    //end perhitungan cbr

}?>