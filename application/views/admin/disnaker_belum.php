        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Belum Diterima</h1>
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table" align="center">
                                <?php $no = $offset;
                                // Memastikan jika data tidak kosong
                                if ($data['query']->num_rows() > 0) {
                                foreach ($data['query']->result() as $row) {
                                ?>

                                <tr>
                                        <td>Nama: <?php echo $row->nama_lengkap; ?></td>
                                        <td rowspan="5"><img class="pull-right" width="200" height="200"src="<?php echo base_url();?>upload/foto_pencaker/<?php echo $row->foto;?>"</td>
                                        
                                </tr>
                                
                                <tr>
                                    <td>TTL: <?php echo $row->tempat_lahir; ?>, <?php echo $row->tanggal_lahir; ?></td>
                                </tr>
                                <tr>
                                     <td>Pendidikan: <?php echo $row->nama_pendidikan; ?></td>
                                </tr>
                                <tr>
                                    <td>Pengalaman: <?php echo $row->pengalaman; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url('link/see_detail_pencaker?id_pencaker='.$row->id_pencaker)?>"><span class="glyphicon glyphicon-zoom-in"></span> Detail</a>
                                    </td>
                                </tr>
                                  
                                <?php  } }
                                // jika data masih kosong, tampilkan pesan
                                else {
                                    echo "<tr><td align='center'>Data masih kosong !</td></tr>";
                                }
                                ?>
                                
                                
                            </table>   
                            <div class="btn-toolbar" role="toolbar"><div class="btn-group"><?php echo $this->pagination->create_links(); ?></div></div>  
                        
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                