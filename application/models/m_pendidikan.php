<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pendidikan extends CI_Model { 
        private $table = "pendidikan_terakhir";

        function get_data_pendidikan(){
                   $this->db->select('*');                   
                   $query = $this->db->get('pendidikan_terakhir');
                   
                   return $query->row();
        }
        
        function ambil_pendidikan($id_pend)
                {
                   $this->db->select('*');
                   $this->db->where('id_pendidikan', $id_pend);
                   $query = $this->db->get('pendidikan_terakhir');
                   
                   return $query->row();
        }

}?>
