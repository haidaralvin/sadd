<?php if (empty($this->session->userdata('username'))){ 
    redirect('pencaker/login_form');}?>
<!DOCTYPE html>
<html>
<head>
<!-- TinyMCE -->
<script type="text/javascript" src="<?php echo base_url('tiny_mce/tiny_mce.js'); ?>"></script>
<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft,visualblocks",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "css/content.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Style formats
        style_formats : [
            {title : 'Bold text', inline : 'b'},
            {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
            {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
            {title : 'Example 1', inline : 'span', classes : 'example1'},
            {title : 'Example 2', inline : 'span', classes : 'example2'},
            {title : 'Table styles'},
            {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ],

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
    });
</script>

<style type="text/css">
.content_pm  {
    padding:2px !important;
    text-align:center
}

</style>
<!-- /TinyMCE -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Bursa Kerja Malang</title>
    <link href="<?php echo base_url();?>styles/css/main.css" rel="stylesheet">
    <link href="<?php echo base_url();?>styles/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>styles/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="<?php echo base_url();?>styles/css/jcarousel.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>styles/css/flexslider.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>styles/css/style.css" rel="stylesheet" />


    <!-- Theme skin -->
    <link href="<?php echo base_url();?>styles/skins/default.css" rel="stylesheet" />
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url();?>styles/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>styles/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>styles/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>styles/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>styles/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->
<body>
<div id="wrapper">
    <!-- start header -->
    <header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href=""><img src="<?php echo base_url();?>styles/images/logofix.png"></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo base_url();?>pencaker/see_pencaker">Profil</a></li>
                        <!-- <li><a href="<?php echo base_url();?>pencaker/saran">Saran</a></li> -->
                        <li><a href="<?php echo base_url();?>pencaker/reset">Reset Password</a></li>
                        <li><a href="<?php echo base_url();?>pencaker/see_industri">Industri</a></li>
                        <li><a href="<?php echo base_url();?>pencaker/lowongan_posisi">Lowongan</a></li>
                        <li><a href="<?php echo base_url();?>pencaker/see_notifikasi">Notifikasi</a></li>
                        <li><a href="<?php echo base_url();?>pencaker/logout">Log Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <!-- <section id="title" class="emerald">
        <div class="container">
            <div class="row">
                
                <div class="col-sm-8">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url();?>pencaker/see_pencaker">Profil</a></li>
                        <li><a href="<?php echo base_url();?>pencaker/see_industri">Industri</a></li>
                        <li><a href="<?php echo base_url();?>pencaker/lowongan_posisi">Lowongan</a></li>
                        <!--<?php $angka= $this->session->userdata('id_pencaker');
                                        $sql="select count(*) as total from minat where id_pencaker = $angka and is_read = 0 and is_confirm= 1" ; 
                                        $result= mysql_query($sql); 
                                        $result= mysql_fetch_assoc($result);
                                        $total= $result['total']; 


                                        $sql1="select count(*) as total1 from lamar where id_pencaker = $angka and is_read = 0 and is_confirm= 1" ; 
                                        $result1= mysql_query($sql1); 
                                        $result1= mysql_fetch_assoc($result1);
                                        $total1= $result1['total1'];

                                        $totalall = $total+$total1 ;
                                        ?>
                         <li ><a href="<?php echo base_url();?>pencaker/see_notifikasi">Notifikasi <span STYLE="background-color: #CC3300" class="badge" ><?php echo $totalall;?></span></a></li>
                         -->
                         <!-- <li><a href="<?php echo base_url();?>pencaker/logout">Log Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section> --><!--/#title-->