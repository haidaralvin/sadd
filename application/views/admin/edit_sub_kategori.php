        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Sub Kategori</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>link/pro_edit_sub_kategori">
								
                                <div class="form-group">
                                    
                                    <div class="col-sm-8">
                                        <select multiple class="form-control" name="id_kategori">
                                        
                                          <?php foreach ($kategori as $row): ?>
                                            <option <?php if($row->id_kategori == $sub_kategori->id_kategori){?> selected <?php } ?> 
                                                value="<?php echo $row->id_kategori ;?>">&nbsp;<?php echo $row->nama_kategori; ?></option>
                                         <?php endforeach;?>
                                        
                                            
                                        </select>
                                    </div>
                                </div>
                               
                                <div class="form-group">
									
									<div class="col-sm-8">
                                        <input type="hidden" class="form-control" name="id_subkategori" value="<?php echo $sub_kategori->id_subkategori ?>">
										<input type="text" class="form-control" required="required" name="nama_sub" value="<?php echo $sub_kategori->nama_sub?>">
									</div>
                                    <label class="control-label">Sub Kategori</label>
								</div>
                                
								<div class="col-sm-2 col-sm-offset-2">
										<button type="submit" class="btn btn-primary btn-lg">Submit</button>
									</div>
							</form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
				