<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kategori extends CI_Model {  
        private $table = "kategori";

        function get_data_kategori(){
                $q=$this->db->get('kategori');
                $data=$q->result();
                return $data;
        }
        function get_datakategori($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table}")->row()->total_rows;
                $data['query'] = $this->db->limit($limit, $offset)->get($this->table);
                return $data;
        }
        function get_all_kategori(){
                $q=$this->db->get($this->table);
                $data=$q->result();
                return $data;
                
        }
        function get_data_nmkategori($id_kategori){
                 $this->db->select('*');
                $this->db->where('id_kategori', $id_kategori);
                $query = $this->db->get('kategori');
                $data=$query->result();
                return $data;
        }
        function get_data_kategori_lowongan(){
                $this->db->select('kategori.id_kategori, kategori.nama_kategori, sub_kategori.id_kategori, sub_kategori.nama_sub');
                $this->db->join('sub_kategori','sub_kategori.id_kategori = kategori.id_kategori');
                $q=$this->db->get_where('kategori');
                $data=$q->result();
                return $data;
        }
        function get_data_kategori_by_id($data){
                $this->db->where($data);
                $q=$this->db->get($this->table);
                
                $data=$q->first_row();
                return $data;
        }
        function add_kategori($data){
                $q=$this->db->insert($this->table,$data);
                return $q;
        }

        function edit_kategori($data){
                $this->db->where('id_kategori',$data['id_kategori']);
                $q=$this->db->update($this->table,$data);
                return $q;
        }
        
        function delete_kategori($data)
        {
                $q=$this->db->delete($this->table,$data);
                return $q;
        }

        function get_data_nmsub(){
                $this->db->select('sub_kategori.id_subkategori, sub_kategori.nama_sub, kategori.nama_kategori')
                        ->join('sub_kategori','sub_kategori.id_kategori= kategori.id_kategori');
                       
                $query = $this->db->get('kategori');
                return $query->result();
        }

        

}?>