<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pencaker extends CI_Model {  
        private $table = "pencaker";
// start profil pencaker
        function get_detail_pencaker($id_pencaker){

                $this->db->select('p.*,k.nama_kecamatan,ps.nama_posisi,pt.nama_pendidikan,b.bidang_keahlian, g.gaji,pl.pengalaman');
            
                $this->db->join('kecamatan k','k.id_kecamatan=p.id_kecamatan');
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=p.id_pendidikan');
                $this->db->join('posisi ps','ps.id_posisi=p.id_posisi');
                $this->db->join('bidang_keahlian b','b.id_bidang=p.id_bidang');
                $this->db->join('gaji g','g.id_gaji=p.id_gaji');
                $this->db->join('pengalaman pl','pl.id_pengalaman=p.id_pengalaman');

                $this->db->where("p.id_pencaker",$id_pencaker);
                $query = $this->db->get('pencaker p');
                return $query->row();
                
        }

        function get_lamar_by_id($id_lamar){
                $this->db->select('l.*');
                $this->db->where("l.id_lamar",$id_lamar);
                $query = $this->db->get('lamar l');
                return $query->row();
        }

        function get_minat_by_id($id_minat){
                $this->db->select('l.*');
                $this->db->where("l.id_minat",$id_minat);
                $query = $this->db->get('minat l');
                return $query->row();
        }

          function get_data_minat_pencaker_confirm($id_pencaker)
          {
            $this->db->select('minat.*,industri.nama_perusahaan, industri.id_industri');
            $this->db->join('industri','industri.id_industri = minat.id_industri');
            $this->db->where('minat.is_confirm',1);
            $this->db->where('minat.id_pencaker',$id_pencaker);
            $this->db->order_by('minat.tanggal_minat','desc');

            $query= $this->db->get('minat');
            $data=$query->result();
            return $data;
          }

            function get_jumlah_minat_pencaker_confirm_unread($id_pencaker)
          {
            $this->db->select('count(minat.id_minat) jumlah');

            $this->db->join('industri','industri.id_industri = minat.id_industri');
            $this->db->where('minat.is_confirm',1);
            $this->db->where('minat.is_read_by_pencaker',0);
            $this->db->where('minat.id_pencaker',$id_pencaker);
            $query= $this->db->get('minat');
            $data=$query->row();
            return $data;
          }

          
          function get_data_lamar_pencaker_confirm($id_pencaker)
          {
            $this->db->select('lamar.*,industri.nama_perusahaan, lowongan.nama_lowongan');
            $this->db->join('industri','industri.id_industri = lamar.id_industri');
            $this->db->join('lowongan','lowongan.id_lowongan = lamar.id_lowongan');
            $this->db->where('lamar.is_confirm',1);
            $this->db->where('lamar.id_pencaker',$id_pencaker);
            $this->db->order_by('lamar.tanggal_lamar','desc');
            $query= $this->db->get('lamar');
            $data=$query->result();
            return $data;
          }

            function get_jumlah_lamar_pencaker_confirm_unread($id_pencaker)
          {
            $this->db->select('count(lamar.id_lamar) jumlah');

            $this->db->join('industri','industri.id_industri = lamar.id_industri');
            $this->db->where('lamar.is_confirm',1);
            $this->db->where('lamar.is_read_by_pencaker',0);
            $this->db->where('lamar.id_pencaker',$id_pencaker);
            $query= $this->db->get('lamar');
            $data=$query->row();
            return $data;
          }

        function get_data_det_industri_notif($data){
            $this->db->select('sub_kategori.nama_sub, kecamatan.nama_kecamatan,industri.id_industri, industri.username,industri.nama_perusahaan,industri.alamat,industri.id_kecamatan,industri.tahun_berdiri,industri.telepon,industri.email,industri.id_subkategori,industri.nama_pemimpin,industri.keterangan,industri.tanggal,industri.foto_industri,industri.scan_ijin_usaha,industri.is_confirm');
            $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori','inner');
            $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan','inner');
            $this->db->join('minat','minat.id_industri= industri.id_industri');
            $this->db->where($data);
            $q=$this->db->get('industri');
            $data=$q->first_row();
            return $data;
            }

       function edit_minat_read_by_pencaker($param, $isi){
                $this->db->where($param);
                return $this->db->update('minat',$isi);
                
        }

       
        function get_data_det_industri_notif_lamar($data){
                $this->db->select('sub_kategori.nama_sub, kecamatan.nama_kecamatan,industri.id_industri, industri.username,industri.nama_perusahaan,industri.alamat,industri.id_kecamatan,industri.tahun_berdiri,industri.telepon,industri.email,industri.id_subkategori,industri.nama_pemimpin,industri.keterangan,industri.tanggal,industri.foto_industri,industri.scan_ijin_usaha,industri.is_confirm');
                $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori','inner');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan','inner');
                $this->db->join('lamar','lamar.id_industri= industri.id_industri');
                $this->db->where($data);
                $q=$this->db->get('industri');
                $data=$q->first_row();
        return $data;
        }

       
        function get_data_det_lowongan_notif_lamar($data){
                $this->db->select('p.*,k.nama_kecamatan,ps.nama_posisi,pt.nama_pendidikan,b.bidang_keahlian, g.gaji,pl.pengalaman');
                $this->db->join('kecamatan k','k.id_kecamatan=p.id_kecamatan');
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=p.id_pendidikan');
                $this->db->join('posisi ps','ps.id_posisi=p.id_posisi');
                $this->db->join('bidang_keahlian b','b.id_bidang=p.id_bidang');
                $this->db->join('gaji g','g.id_gaji=p.id_gaji');
                $this->db->join('pengalaman pl','pl.id_pengalaman=p.id_pengalaman');

                $this->db->join('lamar lm','lm.id_lowongan= p.id_lowongan');
                
                $this->db->where($data);
                $q=$this->db->get('lowongan p');
                $data=$q->first_row();
        return $data;
        }



       function edit_lamar_read_by_pencaker($param, $isi){
                $this->db->where($param);
                return $this->db->update('lamar',$isi);
                
        }

// end
        function add_pencaker($data){
            $q=$this->db->insert('pencaker',$data);
            return $q;        
        }
        function get_data_lamar($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM lamar WHERE lamar.is_confirm=0")->row()->total_rows;
                $this->db->select('lamar.*, lowongan.nama_lowongan, industri.nama_perusahaan, pencaker.nama_lengkap');
                $this->db->join('industri','industri.id_industri = lamar.id_industri');
                $this->db->join('pencaker','pencaker.id_pencaker= lamar.id_pencaker');
                $this->db->join('lowongan','lowongan.id_lowongan= lamar.id_lowongan');
                $this->db->order_by('lamar.tanggal_lamar','desc');
                
                $data['query'] = $this->db->get('lamar', $limit, $offset);
                return $data;
        }
        function get_data_minat($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM minat WHERE minat.is_confirm=0")->row()->total_rows;
                $this->db->select('minat.*, industri.nama_perusahaan, pencaker.nama_lengkap');
                $this->db->join('industri','industri.id_industri = minat.id_industri');
                $this->db->join('pencaker','pencaker.id_pencaker= minat.id_pencaker');
                 
                $data['query'] = $this->db->get('minat', $limit, $offset);
                return $data;
        }
        function get_data_pencaker_confirmed($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} WHERE is_confirm=1")->row()->total_rows;
                
                $data['query'] = $this->db->get_where($this->table, array('is_confirm'=>1), $limit, $offset);
                return $data;
        }
        function get_data_pencaker_kec_toconfirm($limit, $offset = 0 , $id_kecamatan){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} JOIN admin ON admin.id_kecamatan = pencaker.id_kecamatan WHERE admin.id_kecamatan = pencaker.id_kecamatan and is_confirm=1")->row()->total_rows;
                
                $this->db->select('p.*,pt.nama_pendidikan,pl.pengalaman');

                $this->db->join('kecamatan k','k.id_kecamatan= p.id_kecamatan');
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan= p.id_pendidikan');
                $this->db->join('pengalaman pl','pl.id_pengalaman= p.id_pengalaman');
                $this->db->where('p.id_kecamatan',$id_kecamatan);
                $this->db->order_by('p.tanggal','desc');
                 
                $data['query'] = $this->db->get_where("pencaker p", array('is_confirm'=>1), $limit, $offset);
                // print_r($data);
                return $data;
        }
        function get_data_pencaker_yes_rekrut($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} WHERE is_confirm=1 and is_ambil=1")->row()->total_rows;
                $this->db->select("pencaker.*,pt.nama_pendidikan,pl.pengalaman");
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=pencaker.id_pendidikan');
                $this->db->join('pengalaman pl','pl.id_pengalaman=pencaker.id_pengalaman');
                $data['query'] = $this->db->get_where($this->table, array('is_confirm'=>1, 'is_ambil'=>1), $limit, $offset);
                return $data;
        }
        function get_data_pencaker_no_rekrut($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} WHERE is_confirm=1 and is_ambil=0")->row()->total_rows;
                
        $this->db->select("pencaker.*,pt.nama_pendidikan,pl.pengalaman");
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=pencaker.id_pendidikan');
                $this->db->join('pengalaman pl','pl.id_pengalaman=pencaker.id_pengalaman');
                $this->db->order_by('pencaker.nama_lengkap','asc');
                        $data['query'] = $this->db->get($this->table, $limit, $offset);
                return $data;
        }
        function get_data_pencaker(){
                $q=$this->db->get($this->table);
                $data=$q->result();
                return $data;
        }
        function get_data_pencaker_paging($limit, $offset = 0){
                $kec = $this->session->userdata('id_kecamatan');
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} WHERE id_kecamatan = $kec and is_confirm=1")->row()->total_rows;
                $data['query'] = $this->db->get_where($this->table, array('id_kecamatan'=>$kec, 'is_confirm'=>1), $limit, $offset);
                return $data;
        }

        function get_data_pencaker_paging_by_tgl($limit, $offset = 0, $awal, $akhir){
                $kec = $this->session->userdata('id_kecamatan');
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} WHERE id_kecamatan = $kec and is_confirm=1")->row()->total_rows;
                $data['query'] = $this->db->get_where($this->table, array('id_kecamatan'=>$kec, 'is_confirm'=>1 , 'tgl_conf >='=>$awal, 'tgl_conf <='=>$akhir  ), $limit, $offset);
                return $data;
        }

        function get_data_pencaker_by_id($data){
            $this->db->select('p.*,k.nama_kecamatan,ps.nama_posisi,pt.nama_pendidikan,b.bidang_keahlian, g.gaji,pl.pengalaman');
            
                $this->db->join('kecamatan k','k.id_kecamatan=p.id_kecamatan');
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=p.id_pendidikan');
                $this->db->join('posisi ps','ps.id_posisi=p.id_posisi');
                $this->db->join('bidang_keahlian b','b.id_bidang=p.id_bidang');
                $this->db->join('gaji g','g.id_gaji=p.id_gaji');
                $this->db->join('pengalaman pl','pl.id_pengalaman=p.id_pengalaman');

                $this->db->where($data);
                $query = $this->db->get('pencaker p');
                $data=$query->first_row();
                return $data;
        }

        function lihat_pass(){
        $username=$this->input->post('username');
        $email=$this->input->post('email');
        $query=$this->db->query("select * from pencaker where username='$username' and email='$email'");
             if ($query->num_rows() > 0) {
                foreach ($query->result() as $data) {
                  $this->session->set_userdata('PASSWORD_DAPAT', $data->username);
                  $new_pass=md5($data->username);
                  $this->db->query("update pencaker set password='$new_pass' where username='$username' and email='$email'");
                  redirect('pencaker/login_form');     
                }
            } else {
                echo "<script>alert('Data Tidak Di Temukan');</script>";
                redirect('pencaker/oh_saya_lupa?error=','refresh');
            }           
        }

        function edit_pencaker($data){
                $this->db->where('id_pencaker',$data['id_pencaker']);
                $q=$this->db->update($this->table,$data);
                return $q;
        }
        function edit_lamar($data){
                $this->db->where('id_lamar',$data['id_lamar']);
                $q=$this->db->update('lamar',$data);
                return $q;
        }
        function edit_minat($data){
                $this->db->where('id_minat',$data['id_minat']);
                $q=$this->db->update('minat',$data);
                return $q;
        }
        
        function delete_pencaker($data)
        {
                $q=$this->db->delete($this->table,$data);
                return $q;
        }
        function get_data_kecamatan(){
                $q=$this->db->get('kecamatan');
                $data=$q->result();
                return $data;
        }
        public function data_pencaker($username)
        {
           $this->db->select('*');
           $this->db->where('username', $username);
           $query = $this->db->get('pencaker');
           
           return $query->row();
        }
        function get_data_posisi(){
                $q=$this->db->get('posisi');
                $data=$q->result();
                return $data;
        }
        function ambil_username($username, $password)
        {
                $this->db->select('*');
                $this->db->from('pencaker');
                $this->db->where('username',$username);
                $this->db->where('password',$password);
                $query= $this->db->get();

                return $query->row_array();
        }
        function get_data_lowongan_by_id_pencaker(){
                        $this->db->select('lowongan.id_lowongan, lowongan.nama_lowongan, lowongan.deadline, posisi.nama_posisi, industri.id_industri, industri.nama_perusahaan');
                        $this->db->join('posisi','posisi.id_posisi= lowongan.id_posisi');
                        $this->db->join('industri','industri.id_industri= lowongan.id_industri');
                        $this->db->where('is_deadline',0);
                $query = $this->db->get('lowongan');
                return $query->result();
                }
                
        function get_data_industri_by_id_pencaker($id_pencaker){
                        $this->db->select('*');
                $this->db->from('industri');
                $this->db->where('is_confirm',1);
                $query= $this->db->get('');
                return $query->result();
                }
        function get_data_industri_by_id_industri($data){
                $this->db->where($data);
                $q=$this->db->get('industri');
                $data=$q->first_row();
                return $data;
                                }
                                
        public function get_data_lowongan_by_id_lowongan($data)
                {
                $this->db->select('l.*,k.nama_kecamatan,ps.nama_posisi,pt.nama_pendidikan,b.bidang_keahlian, g.gaji,pl.pengalaman,i.*');
                $this->db->join('kecamatan k','k.id_kecamatan=l.id_kecamatan');
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=l.id_pendidikan');
                $this->db->join('posisi ps','ps.id_posisi=l.id_posisi');
                $this->db->join('bidang_keahlian b','b.id_bidang=l.id_bidang');
                $this->db->join('gaji g','g.id_gaji=l.id_gaji');
                $this->db->join('pengalaman pl','pl.id_pengalaman=l.id_pengalaman');
                $this->db->join('industri i','i.id_industri = l.id_industri');

                $this->db->where($data);
                $query = $this->db->get('lowongan l');
                   $data = $query->row();
                   return $data ;
                }
                
        function get_data_lamar_confirm($id_prncaker)
          {
            $this->db->select('industri.nama_perusahaan, lamar.is_read, industri.id_industri, lamar.id_lamar');
            $this->db->join('industri','industri.id_industri = lamar.id_industri');
            $this->db->where('lamar.is_confirm',1);
            $this->db->where('lamar.id_pencaker',$id_pencaker);
            $query= $this->db->get('lamar');
            $data=$query->result();
            return $data;
          }

        function edit_minat_read($id_industri, $param, $isi){
                $q= $this->db->update('minat',$isi);
                $this->db->where('id_industri',$id_industri);
                $this->db->where('id_pencaker',$param);
                
                return $q;   
                
        }

        function get_data_minat_confirm($id_pencaker)
          {
            $this->db->select('industri.nama_perusahaan');
            $this->db->join('industri','industri.id_industri = minat.id_industri');
            $this->db->where('minat.is_confirm',1);
            $this->db->where('minat.id_pencaker',$id_pencaker);
            $query= $this->db->get('minat');
            $data=$query->result();
            return $data;
          }


        function get_data_lowongan_by_id_industri($data)
        {
        $this->db->select('lowongan.id_lowongan, lowongan.nama_lowongan, lowongan.isi, posisi.nama_posisi, kecamatan.nama_kecamatan, pencaker.nama_lengkap, lowongan.tanggal, lowongan.deadline');

        $this->db->join('posisi','posisi.id_posisi= lowongan.id_posisi','inner');
        $this->db->join('kecamatan','kecamatan.id_kecamatan= lowongan.id_kecamatan','inner');
        $this->db->join('pencaker','pencaker.id_pencaker= lowongan.id_pencaker','inner');

          $this->db->where($data);
            $q=$this->db->get('lowongan');
            $data=$q->first_row();
            return $data;
        }

        public function data_pencaker_id($id)
        {
           $this->db->select('*');
           $this->db->where('id_pencaker', $id);
           $query = $this->db->get('pencaker');
           
           return $query->row();
        }

}?>