<section id="contact-page" class="container">
        
		<div class="row">
            <div class="col-sm-8">
            <font style="color:black;">
            		<?php
            		date_default_timezone_set("Asia/Jakarta");
                    /* script menentukan hari */  
						 $array_hr= array(1=>"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
						 $hr = $array_hr[date('N')];
					/* script menentukan tanggal */    
						$tgl= date('j');
					/* script menentukan bulan */ 
					  	$array_bln = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");
					  	$bln = $array_bln[date('n')];
					/* script menentukan tahun */ 
						$thn = date('Y');
					/* script perintah keluaran*/ 
					 	echo "<strong>".$hr . ", " . $tgl . " " . $bln . " " . $thn . "</strong>";
					 ?><div id="time"></div></font>
			<hr>
				<div class="center gap">
                      <h3 align="center">Data Perusahaan</h3>
                </div>  
                
				 <form class="form-horizontal" role="form" method="post" <?php echo @$error; ?><?php echo form_open_multipart('industri/pro_add_industri') ;?>

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Username</label>
					<div class="col-sm-8">
					<input type="text" name="username" required='required' class="form-control" maxlength="25" id="inputEmail3" placeholder="Username">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Password</label>
					<div class="col-sm-8">
					<input type="password" required='required' name="pass" class="form-control" maxlength="25" id="inputEmail3" placeholder="Password">
					</div>
				</div>
			<hr>
				<div class="center gap">
                      <h3 align="center">Data Pribadi Perusahaan</h3>
                </div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Nama&nbsp;Perusahaan</label>
					<div class="col-sm-8">
					<input type="text" required='required' name="nama" class="form-control" id="inputEmail3" placeholder="Nama Perusahaan">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Sub Kategori</label>
					<div class="col-sm-8">
					<select required='required' name="sub_kateg" class="form-control">
						<?php foreach ($sub_kategori as $row): ?>
                                            <option value="<?php echo $row->id_subkategori ;?>">&nbsp;<?php echo $row->nama_sub; ?></option>
                                         <?php endforeach;?>
					</select>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Nama&nbsp;Pimpinan</label>
					<div class="col-sm-8">
					<input type="text" required='required' name="pimpinan" class="form-control" id="inputEmail3" placeholder="Nama Pimpinan">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Tahun Berdiri</label>
					<div class="col-sm-8">
					<input type="text" required='required' name="thn" class="form-control" maxlength="4" id="inputEmail3" placeholder="Tahun Berdiri">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-8">
					<input type="email" required='required' name="email" class="form-control" id="inputEmail3" placeholder="Email">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Telepon</label>
					<div class="col-sm-8">
					<input type="text" required='required' name="telp" class="form-control" maxlength="12" id="inputEmail3" placeholder="Telepon">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
					<div class="col-sm-8">
					<input type="text" required='required' name="alamat" class="form-control" id="inputEmail3" placeholder="Alamat">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Kecamatan</label>
					<div class="col-sm-8">
					<select required='required' name="kecamatan" class="form-control">
						<?php foreach ($kecamatan as $row): ?>
                                            <option value="<?php echo $row->id_kecamatan ;?>">&nbsp;<?php echo $row->nama_kecamatan; ?></option>
                                         <?php endforeach;?>
					</select>
					</select>
					</div>
				</div>
				<hr>
				<div class="center gap">
                      <h3 align="center">Unggah Berkas</h3>
                </div> 
                <font color="red">*) Ukuran logo Max.200x200 pixels dan SIUP,TDP,KTP,IMB,Izin Gangguan Max.700x900 pixels</font><br>
				&nbsp;<div class="form-group">
					<label for="" class="col-sm-2 control-label">Logo&nbsp;Perusahaan</label>
					<label for="" class="col-sm-2 control-label"><input type="file" name="foto_industri"  id="inputEmail3"></label>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">SIUP</label>
					<label for="inputEmail3" class="col-sm-2 control-label"><input type="file" name="siup"  id="inputEmail3"></label>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">TDP</label>
					<label for="inputEmail3" class="col-sm-2 control-label"><input type="file" name="tdp"  id="inputEmail3"></label>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">KTP</label>
					<label for="inputEmail3" class="col-sm-2 control-label"><input type="file" name="ktp"  id="inputEmail3"></label>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">IMB</label>
					<label for="inputEmail3" class="col-sm-2 control-label"><input type="file" name="imb"  id="inputEmail3"></label>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Izin Gangguan</label>
					<label for="inputEmail3" class="col-sm-2 control-label"><input type="file" name="ig"  id="inputEmail3"></label>
				</div>

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Tanggal Siup Berakhir</label>
					<div class="col-sm-8">
					<input type="date" name="tgl_siup" class="form-control" required='required'>
					</div>
				</div>

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
					<div class="col-sm-8">
					<textarea name="ket" class="form-control"></textarea>
					</div>
				</div>
				<hr><div class="center gap" align="center">
                      <button type="submit" class="btn btn-theme btn-lg">Daftar</button>
                </div> 
				</form>
            </div>
        </div>
    </section>