    <section id="featured">
    <!-- start slider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <font style="color:black;">
            		<?php
            		date_default_timezone_set("Asia/Jakarta");
                    /* script menentukan hari */  
						 $array_hr= array(1=>"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
						 $hr = $array_hr[date('N')];
					/* script menentukan tanggal */    
						$tgl= date('j');
					/* script menentukan bulan */ 
					  	$array_bln = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");
					  	$bln = $array_bln[date('n')];
					/* script menentukan tahun */ 
						$thn = date('Y');
					/* script perintah keluaran*/ 
					 	echo "<strong>".$hr . ", " . $tgl . " " . $bln . " " . $thn . "</strong>";
					 ?><div id="time"></div></font>
    <!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
              <li>
                <img src="<?php echo base_url();?>styles/img/slides/1.jpg" alt="" />
              </li>
              <li>
                <img src="<?php echo base_url();?>styles/img/slides/2.jpg" alt="" />
              </li>
              <li>
                <img src="<?php echo base_url();?>styles/img/slides/3.jpg" alt="" />
              </li>
            </ul>
        </div>
    <!-- end slider -->
            </div>
        </div>
    </div>  
    </section>

    <section class="callaction">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="big-cta">
                    <div class="cta-text">
                        <h2><span><marquee direction="left" scrollamount="8" align="center" behavior="alternate">Bursa Kerja Online</marquee></span></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>

    <section id="services" class="emerald2">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                          <a href="<?php echo base_url();?>home/pencaker_confirm"><img class="icon-dribbble icon-md" height="100" width="100" src="<?php echo base_url();?>styles/images/ico/pencaker.png"/></a>
                        </div>
                        <div class="media-body">
                            <a href="<?php echo base_url();?>home/pencaker_confirm" style="text-decoration:none"><h3 class="media-heading">Pencaker</h3></a>
                            <?php
                                $sql1="select count(*) as total1 from pencaker where is_confirm = 1" ; 
                                $result1= mysql_query($sql1); 
                                $result1= mysql_fetch_assoc($result1);
                                $total1= $result1['total1'];
                            ?>
                            <p>jumlah pencaker : <?php echo $total1;?></p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <a href="<?php echo base_url();?>home/industri"><img class="icon-dribbble icon-md" height="100" width="100" src="<?php echo base_url();?>styles/images/ico/industri.png"/></a>
                        </div>
                        <div class="media-body">
                            <a href="<?php echo base_url();?>home/industri" style="text-decoration:none"><h3 class="media-heading">Industri</h3></a>
                            <?php
                                $sql2="select count(*) as total2 from industri where is_confirm = 1" ; 
                                $result2= mysql_query($sql2); 
                                $result2= mysql_fetch_assoc($result2);
                                $total2= $result2['total2'];
                            ?>
                            <p>jumlah industri : <?php echo $total2;?></p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <a href="<?php echo base_url();?>home/bursa_kerja"><img class="icon-dribbble icon-md" height="100" width="100" src="<?php echo base_url();?>styles/images/ico/lowongan.png"/></a>
                        </div>
                        <div class="media-body">
                            <a href="<?php echo base_url();?>home/bursa_kerja" style="text-decoration:none"><h3 class="media-heading">Lowongan</h3></a>
                            <?php
                                $sql3="select count(*) as total3 from lowongan where is_deadline = 0" ; 
                                $result3= mysql_query($sql3); 
                                $result3= mysql_fetch_assoc($result3);
                                $total3= $result3['total3'];
                            ?>
                            <p>jumlah lowongan : <?php echo $total3;?></p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
            </div>
        </div>
    </section>

    <section id="content">
    <div class="container">
    <div class="row">
            <div class="col-lg-12">
                <div class="solidline">
                </div>
            </div>
        </div>
        <!-- end divider -->
        <!-- Portfolio Projects -->
        <div class="row">
            <div class="col-lg-12">
                <h4 class="heading">Recent Works</h4>
                <div class="row">
                    <section id="projects">
                    <ul id="thumbs" class="portfolio">
                        <!-- Item Project and Filter Name -->
                        <li class="col-lg-3 design" data-id="id-0" data-type="web">
                        <div class="item-thumbs">
                        <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                        <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="" href="<?php echo base_url();?>styles/img/works/4.jpg">
                        <span class="overlay-img"></span>
                        <span class="overlay-img-thumb font-icon-plus"></span>
                        </a>
                        <!-- Thumb Image and Description -->
                        <img src="<?php echo base_url();?>styles/img/works/4.jpg" alt="Love your job">
                        </div>
                        </li>
                        <!-- End Item Project -->
                        <!-- Item Project and Filter Name -->
                        <li class="item-thumbs col-lg-3 design" data-id="id-1" data-type="icon">
                        <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                        <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="" href="<?php echo base_url();?>styles/img/works/5.jpg">
                        <span class="overlay-img"></span>
                        <span class="overlay-img-thumb font-icon-plus"></span>
                        </a>
                        <!-- Thumb Image and Description -->
                        <img src="<?php echo base_url();?>styles/img/works/5.jpg" alt="Work Hard Dream Big">
                        </li>
                        <!-- End Item Project -->
                        <!-- Item Project and Filter Name -->
                        <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="illustrator">
                        <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                        <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="" href="<?php echo base_url();?>styles/img/works/9.jpg">
                        <span class="overlay-img"></span>
                        <span class="overlay-img-thumb font-icon-plus"></span>
                        </a>
                        <!-- Thumb Image and Description -->
                        <img src="<?php echo base_url();?>styles/img/works/9.jpg" alt="Time is Money">
                        </li>
                        <!-- End Item Project -->
                        <!-- Item Project and Filter Name -->
                        <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="illustrator">
                        <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                        <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="" href="<?php echo base_url();?>styles/img/works/10.jpg">
                        <span class="overlay-img"></span>
                        <span class="overlay-img-thumb font-icon-plus"></span>
                        </a>
                        <!-- Thumb Image and Description -->
                        <img src="<?php echo base_url();?>styles/img/works/10.jpg" alt="Live to Work">
                        </li>
                        <!-- End Item Project -->
                    </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
    </section>