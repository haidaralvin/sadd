        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Selamat Datang</h1>
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="col-sm-8">
                                <p>Selamat datang <?php echo $this->session->userdata('nama_admin');?>. Anda masuk sebagai Admin Disnaker.</p>
                                <button type="button" onclick="window.location.href='<?php echo base_url();?>link/logout'" class="btn btn-success">Logout</button>
                            </div>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
