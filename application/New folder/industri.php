<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Industri extends CI_Controller {

  function __construct()
  {

    parent:: __construct();
    $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
    $this->output->set_header("Pragma: no-cache");
    
  }


  public function index()
  {
    $session = $this->session->userdata('isLogin');

    if($session == FALSE)
      {
        redirect('industri/login_form');
      }else
      {
      
        redirect('industri/see_industri');
      }

  }

  public function login_form()
    {
  
    
    $this->form_validation->set_rules('username', 'username', 'required');
    $this->form_validation->set_rules('password', 'password', 'required');
    $this->form_validation->set_error_delimiters('<span class="error">', '</span>');

      if($this->form_validation->run()==FALSE)
      {
        $vals = array(
                  
                  'img_path'   => './captcha/',
                  'img_url'    => base_url().'captcha/',
                  'img_width'  => '300',
                  'img_height' => 80,
                  'border' => 50,
                  'expiration' => 300
              );
  $cap = create_captcha($vals);
  $data['image'] = $cap['image'];
  $this->session->set_userdata('mycaptcha', $cap['word']);

        $this->load->view('header');
        $this->load->view('industri/login_industri',$data);
        $this->load->view('footer');
        
      }else{

      if ($this->input->post('captcha') == $this->session->userdata('mycaptcha'))
      {

       $username = $this->input->post('username');
       $password = md5($this->input->post('password'));
       $cek = $this->m_industri->ambil_username($username, $password);
        
        if($cek)
        {
          
          $this->session->set_userdata('isLogin', TRUE);
          $this->session->set_userdata('username',$username);
          $this->session->set_userdata('id_industri',$cek['id_industri']);
          $this->session->set_userdata('nama_perusahaan',$cek['nama_perusahaan']);
          $this->session->set_userdata('alamat',$cek['alamat']);
          $this->session->set_userdata('tahun_berdiri',$cek['tahun_berdiri']);
          $this->session->set_userdata('telepon',$cek['telepon']);
          $this->session->set_userdata('email',$cek['email']);
          $this->session->set_userdata('id_subkategori',$cek['id_subkategori']);
          $this->session->set_userdata('nama_pemimpin',$cek['nama_pemimpin']);
          $this->session->set_userdata('keterangan',$cek['keterangan']);
          $this->session->set_userdata('foto_industri',$cek['foto_industri']);
          $this->session->set_userdata('id_kecamatan',$cek['id_kecamatan']);
          $this->session->set_userdata('is_confirm',$cek['is_confirm']);
          

          if($this->session->userdata('is_confirm') == '0'){

          
          echo " <script>
                alert('Maaf Perusahaan Belum Di Konfirmasi');
                history.go(-1);
              </script>";

          
            }else{
              redirect('industri/see_industri');
            }
       
        }
        else
        {
         echo " <script>
                alert('Username Password Salah');
                history.go(-1);
              </script>";        
        }
      }
      else{
        echo " <script>
                alert('Captcha Salah');
                history.go(-1);
              </script>"; 
      }  
    }
  }

 function lihat_pass(){
    $this->m_industri->lihat_pass();    
  }

  function oh_saya_lupa(){
    $this->session->sess_destroy();
    $this->load->view('header');
    $this->load->view('industri/lupa_password');
    $this->load->view('footer');
  }

  function result_pass(){
    $this->session->sess_destroy();
    $this->load->view('industri/login_form');
  }

  public function reset()
    {
        $user = $this->session->userdata('username');
        $user = $this->session->userdata('email');
        $user = $this->session->userdata('id_industri');
        $data['industri'] = $this->m_industri->data_industri($user);

        $this->load->view('industri/header');
        $this->load->view('industri/reset_password',$data);
        $this->load->view('industri/footer');
    }

    public function pro_reset()
    {
      $email= $this->input->post('email');

      $this->load->helper('string');
      
      $data = array(
                  'password' => md5($this->input->post('password'))
                  );

      $this->db->where('email', $email);
      $this->db->update('industri', $data);

      $user = $this->session->userdata('username');
      $user = $this->session->userdata('id_industri');
      $id_kec = $this->session->userdata('id_kecamatan');
      $id_subkategori=$this->session->userdata('id_subkategori');
      $data['industri'] = $this->m_industri->data_industri($user);
      $data['kecamatan'] = $this->m_kecamatan->ambil_kecamatan($id_kec);
      $data['sub_kategori']= $this->m_sub_kategori->get_nmkateg_by_idsub($id_subkategori);
        
       $this->load->view('industri/header');
       $this->load->view('industri/see_industri',$data);
       $this->load->view('footer');
      echo "<script>alert('Password Berhasil Di Reset');</script>";
    }

   function reg_industri()
    { 
      $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      //$data['kategori']=$this->m_kategori->get_data_kategori();
      $data['sub_kategori']=$this->m_sub_kategori->get_data_sub_kategori();
      $this->load->view('header');
      $this->load->view('industri/register_industri', $data);
      $this->load->view('footer');
    }

  function upload_logo(){

    $config['upload_path'] = './upload/foto_industri';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '0';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = FALSE;
        $config['encrypt_name'] = FALSE;

        $this->upload->initialize($config);
        $field_name = 'foto_industri';

        if (!$this->upload->do_upload($field_name)) {
            $data['error'] = $this->upload->display_errors();
            $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      $data['sub_kategori']=$this->m_sub_kategori->get_data_sub_kategori();
            $this->load->view('header');
      $this->load->view('industri/register_industri', $data);
      $this->load->view('footer');
    
         }else{
          $image_name = $this->upload->data();
          $b=$image_name[file_name];
         }

  }

   function upload_siup(){

        $config1['upload_path'] = './upload/scan_ijin_usaha';
        $config1['allowed_types'] = 'gif|jpg|jpeg|png';
        $config1['max_size'] = '0';
        $config1['overwrite'] = FALSE;
        $config1['remove_spaces'] = FALSE;
        $config1['encrypt_name'] = FALSE;

        $this->upload->initialize($config1);
        $field_name1 = 'siup';

        if (!$this->upload->do_upload($field_name1)) 
        {
          $data['error'] = $this->upload->display_errors();
          $data['kecamatan']=$this->m_industri->get_data_kecamatan();
          $data['sub_kategori']=$this->m_sub_kategori->get_data_sub_kategori();
          $this->load->view('header');
          $this->load->view('industri/register_industri', $data);
          $this->load->view('footer');
  
         }else{

          $image_name1 = $this->upload->data();
          $a=$image_name1[file_name];
         
         }

  }

  function upload_tdp(){

        $config1['upload_path'] = './upload/tdp';
        $config1['allowed_types'] = 'gif|jpg|jpeg|png';
        $config1['max_size'] = '0';
        $config1['overwrite'] = FALSE;
        $config1['remove_spaces'] = FALSE;
        $config1['encrypt_name'] = FALSE;

        $this->upload->initialize($config1);
        $field_name1 = 'tdp';

        if (!$this->upload->do_upload($field_name1)) 
        {
          $data['error'] = $this->upload->display_errors();
          $data['kecamatan']=$this->m_industri->get_data_kecamatan();
          $data['sub_kategori']=$this->m_sub_kategori->get_data_sub_kategori();
          $this->load->view('header');
          $this->load->view('industri/register_industri', $data);
          $this->load->view('footer');
  
         }else{

          $image_name1 = $this->upload->data();
          $a=$image_name1[file_name];
         
         }

  }

  function upload_ktp(){

        $config1['upload_path'] = './upload/ktp_industri';
        $config1['allowed_types'] = 'gif|jpg|jpeg|png';
        $config1['max_size'] = '0';
        $config1['overwrite'] = FALSE;
        $config1['remove_spaces'] = FALSE;
        $config1['encrypt_name'] = FALSE;

        $this->upload->initialize($config1);
        $field_name1 = 'ktp';


        if (!$this->upload->do_upload($field_name1)) 
        {
          $data['error'] = $this->upload->display_errors();
          $data['kecamatan']=$this->m_industri->get_data_kecamatan();
          $data['sub_kategori']=$this->m_sub_kategori->get_data_sub_kategori();
          $this->load->view('header');
          $this->load->view('industri/register_industri', $data);
          $this->load->view('footer');

         }else{

          $image_name2 = $this->upload->data();
          $a=$image_name2[file_name];
          
         }

  }
  function upload_imb(){

        $config1['upload_path'] = './upload/imb';
        $config1['allowed_types'] = 'gif|jpg|jpeg|png';
        $config1['max_size'] = '0';
        $config1['overwrite'] = FALSE;
        $config1['remove_spaces'] = FALSE;
        $config1['encrypt_name'] = FALSE;

        $this->upload->initialize($config1);
        $field_name1 = 'imb';


        if (!$this->upload->do_upload($field_name1)) 
        {
          $data['error'] = $this->upload->display_errors();
          $data['kecamatan']=$this->m_industri->get_data_kecamatan();
          $data['sub_kategori']=$this->m_sub_kategori->get_data_sub_kategori();
          $this->load->view('header');
          $this->load->view('industri/register_industri', $data);
          $this->load->view('footer');
    
         }else{

          $image_name1 = $this->upload->data();
          $a=$image_name1[file_name];
          
         }

  }
  function upload_ig(){

        $config1['upload_path'] = './upload/ijin_gangguan';
        $config1['allowed_types'] = 'gif|jpg|jpeg|png';
        $config1['max_size'] = '0';
        $config1['overwrite'] = FALSE;
        $config1['remove_spaces'] = FALSE;
        $config1['encrypt_name'] = FALSE;

        $this->upload->initialize($config1);
        $field_name1 = 'ig';


        if (!$this->upload->do_upload($field_name1)) 
        {
          $data['error'] = $this->upload->display_errors();
          $data['kecamatan']=$this->m_industri->get_data_kecamatan();
          $data['sub_kategori']=$this->m_sub_kategori->get_data_sub_kategori();
          $this->load->view('header');
          $this->load->view('industri/register_industri', $data);
          $this->load->view('footer');
        
         }else{

          $image_name1 = $this->upload->data();
          $a=$image_name1[file_name];
          
         }

  }

  function pro_add_industri()
  {

            $data = array(
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('pass')),
                'nama_perusahaan' => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'id_kecamatan' => $this->input->post('kecamatan'),
                'tahun_berdiri' => $this->input->post('thn'),
                'telepon' => $this->input->post('telp'),
                'email' => $this->input->post('email'),
                'id_subkategori' => $this->input->post('sub_kateg'),
                'nama_pemimpin' => $this->input->post('pimpinan'),
                'keterangan' => $_POST['ket'],
                'foto_industri' => $_FILES['foto_industri']['name'],
                'scan_ijin_usaha' => $_FILES['siup']['name'],
                'tdp' => $_FILES['tdp']['name'],
                'ktp' => $_FILES['ktp']['name'],
                'imb' => $_FILES['imb']['name'],
                'ig' => $_FILES['ig']['name'],
                'is_confirm' => '0',
                'tgl_conf' => '0000-00-00',
                'tgl_siup_exp' => $this->input->post('tgl_siup')


            );

            if(empty($_FILES['foto_industri']['name'])){
              //echo "gagal";
              echo " <script>
                alert('Maaf Masukkan Logo Perusahaan');
                history.go(-1);
              </script>";

            }elseif(empty($_FILES['siup']['name']))

            {
              echo " <script>
                alert('Maaf Masukkan Surat Ijin Usaha Perusahaan');
                history.go(-1);
              </script>";
            }
            elseif (empty($_FILES['tdp']['name'])) {
              echo " <script>
                alert('Maaf Masukkan Tanda Daftar Perusahaan');
                history.go(-1);
              </script>";
            }
            elseif(empty($_FILES['ktp']['name']))

            {
              echo " <script>
                alert('Maaf Masukkan KTP Penanggung Jawab');
                history.go(-1);
              </script>";
            }
            elseif(empty($_FILES['imb']['name']))

            {
              echo " <script>
                alert('Maaf Masukkan Izin Mendirikan Bangunan');
                history.go(-1);
              </script>";
            }
            elseif(empty($_FILES['ig']['name']))

            {
              echo " <script>
                alert('Maaf Masukkan Izin Gangguan');
                history.go(-1);
              </script>";
            }
            else{
              $cek_username=mysql_num_rows(mysql_query("SELECT username FROM industri WHERE username='$_POST[username]'"));
              $cek_email=mysql_num_rows(mysql_query("SELECT email FROM industri WHERE email='$_POST[email]'"));
              if ($cek_username > 0) {
                echo "<script>alert('Username sudah ada yang pakai. Ulangi lagi');history.go(-1);</script>";
              }
              elseif ($cek_email > 0) {
                echo "<script>alert('Email sudah ada yang pakai. Ulangi lagi');history.go(-1);</script>";
              }
              else{
                $this->upload_logo();
                $this->upload_siup();
                $this->upload_tdp();
                $this->upload_ktp();
                $this->upload_imb();
                $this->upload_ig();
                
                $res=$this->m_industri->add_industri($data);
                if($res){
                        redirect('industri/login_form');
                
                }else{
                        echo "Insert Failed<br>";
                        echo '<a href="'.site_url('industri/reg_industri').'">Back</a>';
                }

             }
            }
       }

     
    public function see_industri()
    {     
      if($this->session->userdata('isLogin') == FALSE)
    {
      redirect('industri/login_form');
    }else
    { 
      
      $user = $this->session->userdata('username');
      $user = $this->session->userdata('id_industri');
      $id_kec = $this->session->userdata('id_kecamatan');
      $id_subkategori=$this->session->userdata('id_subkategori');
      $data['industri'] = $this->m_industri->data_industri($user);
      $data['kecamatan'] = $this->m_kecamatan->ambil_kecamatan($id_kec);
      $data['sub_kategori']= $this->m_sub_kategori->get_nmkateg_by_idsub($id_subkategori);
        
       $this->load->view('industri/header');
       $this->load->view('industri/see_industri',$data);
       $this->load->view('industri/footer');

    }  
        
    } 

   public function logout()
  {
   $this->session->sess_destroy();
   
   redirect('industri/login_form');
  }

   public function add_lowongan()
    {       
     
        $data['posisi']=$this->m_lowongan->get_data_posisi();
        $data['kecamatan']=$this->m_lowongan->get_data_kecamatan();
        $data['industri']=$this->m_lowongan->get_data_industri();

        $data['pendidikan_terakhir'] = $this->db->query("SELECT * from pendidikan_terakhir")->result();
        $data['bidang_keahlian'] = $this->db->query("SELECT * from bidang_keahlian")->result();
        $data['pengalaman'] = $this->db->query("SELECT * from pengalaman")->result();
        $data['gaji'] = $this->db->query("SELECT * from gaji")->result();
        $data['usia'] = $this->db->query("SELECT * from usia")->result();

        $this->load->view('industri/header');
        $this->load->view('industri/add_lowongan', $data);
        $this->load->view('industri/footer');
    } 

     public function pro_add_lowongan()
    {
/*____________________
   code start
**********************/ 
// 1. load model profile matching supaya method2 nya bisa dipanggil. ini sama dengan autoload model yang ada di config/autoload.php , cuma karena jarang di pakai, jadi tidak dimasukan di autoload.

      $this->load->model("m_profile_matching"); 

// 2. membuat variable $data untuk menampung data dari form add_lowongan yang dikirim melalui $_POST
        $data = array(
            'nama_lowongan' => $_POST['nama_lowongan'],
            'isi' => $_POST['isi'],
            'id_posisi' => $_POST['posisi'],
            'id_kecamatan' => $_POST['kecamatan'],
            'id_industri' => $_POST['industri'],
            'deadline' => $_POST['deadline'],
            'id_pendidikan' => $_POST['pend_terakhir'],
            'id_bidang' => $_POST['bid_keahlian'],
            'id_gaji' => $_POST['gaji'],
            'id_pengalaman' => $_POST['pengalaman'],
            'id_usia' => $_POST['usia']
        );                  
// 3. tambahkan lowongan baru ke table lowongan di database/
        $res=$this->m_lowongan->add_lowongan($data);

        if($res){
// 4. jika data lowongan sudah masuk ke database, maka kirimkan email ke para pencaker yang sudah dihitung dengan profile matching
// Lakukan pengiriman Email ke pencaker, bahwa ada lowongan baru.

// membuat variable untuk menampung data lowongan sesuai dengan  id lowongan yang dibuat berupa variable $res
              $detail_lowongan=$this->m_industri->get_data_lowongan_by_id_pencaker(array('id_lowongan'=>$res));
// membuat variable industri sesuai dengan id_industri yang didapatkan
              $data_industri=$this->m_industri->get_data_industri_by_id(array('id_industri'=>$_POST['industri']));

// Mengambil data pencaker sesuai perhitungan m_profile_matching
              $_GET['id_lowongan']=$detail_lowongan->id_lowongan;
              $data_pencaker=$this->m_profile_matching->rekomendasi_pencaker(false);

    // membuat variable receiver ini untuk mengisi alamat email penerima nya
        //  kenapa di masukan array ? karena nanti bakalan ada 10 email  penerima
              $receiver=array();

// kita looping data pencaker untuk mendapatk email masing2 pencaker
              foreach ($data_pencaker as $no=>$row_pencaker) {
                if($no<10){
// jika no < 10 maka $receiver kita isi dengan email ini , artinya kita disini mengirim email hanya ke 10 pencakert tertinggi saja.
                  $receiver[]=$row_pencaker->email;
                }else{
                  break;
                }
              }
                                    
 // membuat varialble $subject untuk mengisi tulisan subjek email
              $subject="Lowongan Kerja Baru - ".$_POST['nama_lowongan'];
    // membuat variable message, ini adalah isi pesan dari email. format string HTML.
          $message='  <div style="width:800px;margin:10px auto 0px; padding:10px;background:#f1f1f1; border:solid 1px #ddd; font-size:14px;color:#555;font-family: arial;"> 
<p style="font-size:14px;color:#555;font-family: arial;">
    Perusahaan <b>'.$data_industri->nama_perusahaan.'</b> membuka lowongan baru <b>'.$detail_lowongan->nama_lowongan.'</b>. Dengan kriteria sebagai berikut :
   </p>

<table style="font-size:14px;color:#555; padding:10px;" width="100%" bgcolor="#FFFFFF" border="0" cellpadding="5" cellspacing="0">
<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Kecamatan
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$detail_lowongan->nama_kecamatan.'
  </td>
</tr>
<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Usia
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  '.$detail_lowongan->usia.'
  </td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
  Posisi
  </td>

  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$detail_lowongan->nama_posisi.'
  </td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
Gaji
</td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$detail_lowongan->gaji.'
</td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
Pengalaman
</td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$detail_lowongan->pengalaman.'
</td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
Bidang Keahlian
</td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$detail_lowongan->bidang_keahlian.'
</td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
Pendidikan</td>
  <td colspan="2" style="padding:5px; font-size:14px;color:#555; font-family: arial;">
'.$detail_lowongan->nama_pendidikan.'
</td>
</tr>

<tr>
  <td colspan="2" style="padding:5px; font-size:20px;color:#f00; font-family: arial;">
  Deadline
  </td>
    <td colspan="2" style="padding:5px; font-size:20px;color:#f00; font-family: arial;">
  '.$detail_lowongan->deadline.'
  </td>
</tr>

</table>

<p style="font-size:14px;color:#555;font-family: arial;">
Keterangan :
'.$detail_lowongan->isi.'
</p>


<center><div style="background:white;padding-bottom:20px;padding-top:20px">
<a href="http://localhost/bursa_kerja_tes" style="background:#00c0ef;clear:both;color:#ffffff!important;display:block;font-family:arial,sans-serif;font-size:14px;padding:0px 10px;line-height:1.5;margin:0 auto;padding:10px 0;text-align:center;text-decoration:none!important;width:50%" target="_blank">Lihat Lowongan</a>
</div>
</center>
<p style="font-size:14px;color:#555;font-family: arial;">
Terima kasih atas perhatian dan kepercayaan Anda.
</p>

</div>';
// di sini kita eksekusi pengiriman email ke pencaker (sesuai data diatas)
// method sendMail berada di helper, penjelasan nya ada di Helpers/global_helper.php
              sendMail($receiver,$subject,$message);
                redirect('industri/add_lowongan');
        }else{
                echo "Insert Failed<br>";
                echo '<a href="'.site_url('industri/add_lowongan').'">Back</a>';
        }
        
    }    
    

    public function see_lowongan()
    {
      $id_industri=$this->session->userdata('id_industri');
 
      $data['lowongan'] = $this->m_industri->get_data_lowongan_by_id_industri($id_industri);
           
      $this->load->view('industri/header');
      $this->load->vars($data);
      $this->load->view('industri/see_lowongan', $data);
      $this->load->view('industri/footer');
    
    }

  public function see_pencaker()
    {
/*____________________
   code start
**********************/ 

// 1. load model profile matching supaya method2 nya bisa dipanggil. ini sama dengan autoload model yang ada di config/autoload.php , cuma karena jarang di pakai, jadi tidak dimasukan di autoload.
      $this->load->model("m_profile_matching"); 
// 2. memembuat variable $id_industri yang ambil dari session, sesuai yang login
      $id_industri=$this->session->userdata('id_industri');
// 3. membuat variable lowongan yang diambil dari table lowongan sesuai dengan id_industri nya 
//    untuk ditampilan di select option
      $data['lowongan'] = $this->m_industri->get_data_lowongan_by_id_industri($id_industri);
      if(!isset($_GET['id_lowongan'])) {
// 4. cek dlu, jika belum memilih id_lowongan atau tidak ada id_lowongan makan di gunakan id lowongan yang pertama dari data lowongan
        $_GET['id_lowongan']=$data['lowongan'][0]->id_lowongan;
      }
// 5. ambli data pencaker dari model, sesuai dengan perhitungan profile matching
      $data['pencaker']=$this->m_profile_matching->rekomendasi_pencaker();

// 6. ambil data kecamatan, posisi dan pendidikan untuk ditampilan di select option
      $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      $data['posisi']=$this->m_industri->get_data_posisi();
      $data['pedidikan']=$this->m_industri->get_data_pendidikan();

      $this->load->view('industri/header');
        $this->load->view('industri/see_pencaker', $data);
        $this->load->view('industri/footer');
    }
  
   public function detail_pencaker()
    {
        $id_pencaker=$_GET['id_pencaker'];
        $data['pencaker']=$this->m_pencaker->get_detail_pencaker($id_pencaker);
        $this->load->view('industri/header');
        $this->load->view('industri/detail_pencaker', $data);
        $this->load->view('industri/footer');
    }

  public function delete_lowongan()
    {
        $data['id_lowongan']=$_GET['id_lowongan'];
        $res=$this->m_lowongan->delete_lowongan($data);
        if($res){
                redirect('industri/see_lowongan');
        }else{
                echo "Delete Failed <br>";
                echo '<a href="'.site_url('industri/see_lowongan').'">Back</a>';
        }
      }

  public function minat_pencaker()
    {       
        $param=array('id_pencaker'=>$_GET['id_pencaker']);
        $data['pencaker']=$this->m_industri->get_data_pencaker_by_id_pencaker($param);
        $id_industri=$this->session->userdata('id_industri');
        $data['industri']= $this->m_industri->get_data_pencaker_by_id_industri($id_industri);
        $this->load->view('industri/header');
        $this->load->view('industri/add_minat', $data);
        $this->load->view('industri/footer');
    } 
  
   public function pro_add_minat()
    {
 /*____________________
   code start
**********************/ 
// 1. buat variable sebagai parameter untuk menambahkan data ke table minat dan mengedit pencaker sesuai data yang didapatkan
        $data1 = array(
            'id_pencaker' => $_POST['pencaker'],
            'id_industri' => $_POST['industri']               
      );

      $data=array(
            'id_pencaker'=>$_POST['pencaker'],
            'is_ambil' => '1'
        );                  

// 2. buat variable $pencaker yang diambil dari table pencaker sesuai dengan id yang didapatkan.
// variable ini digunakan untuk mengecek apakah pencaker tersebut sudah diminati perusahaan lain apa belum 
$pencaker=$this->m_pencaker->get_detail_pencaker($_POST['pencaker']);
 
  if($pencaker->is_ambil==1){
// 3. jika is_ambil dari pencaker = 1, berarti pencaker sudah ada yang meminati. sehingga perusahaan lain tidak bisa meminati. 
    echo "<script>alert(' Anda tidak bisa meminati. ".$pencaker->nama_lengkap.". Karena sudah diminati perusahaan lain.');
window.location='".site_url('industri/see_pencaker')."';
    </script>";
   }else{
// 4. jika belum ada yang minat maka tambahka ke table minat
        $res=$this->m_lowongan->add_minat($data1);
// 5. edit pencakernya menjadi sudah diminati dengan memberi parameter is_ambli =1 di dalam variable $data
        $res=$this->m_pencaker->edit_pencaker($data);
        if($res){
           echo "<script>alert(' Anda berhasil meminati. ".$pencaker->nama_lengkap.".');
window.location='".site_url('industri/see_pencaker')."';
    </script>";
        }else{
                echo "Insert Failed<br>";
                echo '<a href="'.site_url('industri/minat_pencaker').'">Back</a>';
        }
        
    }  
  }
      
    // public function coba()
    // {  
    //     $id_industri=$this->session->userdata('id_industri');
    //     $data['pencaker']= $this->m_industri->get_data_pencaker_by_id_industri($id_industri);
    //     $data['kecamatan']=$this->m_industri->get_data_kecamatan();
    //     $data['posisi']=$this->m_industri->get_data_posisi();     
    //     $this->load->view('industri/header');
    //     $this->load->view('industri/proses',$data);
    //     $this->load->view('footer');
    // } 


    function search_pencaker()
    {

      if (isset($_POST['id_kecamatan']) && empty($_POST['pendidikan_terakhir']) && empty($_POST['id_posisi'])) {
      $id_kecamatan = $_POST['id_kecamatan'];
      $data['pencaker']= $this->m_industri->get_data_pencaker_by_kecamatan($id_kecamatan);
      $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      $data['posisi']=$this->m_industri->get_data_posisi();
      $this->load->view('industri/header');
      $this->load->view('industri/see_pencaker', $data);
      $this->load->view('industri/footer');
      }


      elseif(isset($_POST['pendidikan_terakhir']) && empty($_POST['id_kecamatan']) && empty($_POST['id_posisi']))
      {
      $pendidikan_terakhir = $_POST['pendidikan_terakhir'];
      $data['pencaker']= $this->m_industri->get_data_pencaker_by_pendidikan($pendidikan_terakhir);
      $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      $data['posisi']=$this->m_industri->get_data_posisi();
      $this->load->view('industri/header');
        $this->load->view('industri/see_pencaker', $data);
        $this->load->view('industri/footer');
      }


      elseif (isset($_POST['id_posisi']) && empty($_POST['id_kecamatan']) && empty($_POST['pendidikan_terakhir'])) {
      $id_posisi = $_POST['id_posisi'];
      $data['pencaker']= $this->m_industri->get_data_pencaker_by_posisi($id_posisi);
      $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      $data['posisi']=$this->m_industri->get_data_posisi();
      $this->load->view('industri/header');
        $this->load->view('industri/see_pencaker', $data);
        $this->load->view('industri/footer');
        
      }

      elseif (empty($_POST['id_posisi']) && isset($_POST['id_kecamatan']) && isset($_POST['pendidikan_terakhir'])) {
      $pendidikan_terakhir = $_POST['pendidikan_terakhir'];
      $id_kecamatan = $_POST['id_kecamatan'];
      $data['pencaker']= $this->m_industri->get_data_pencaker_by_pendidikan_kecamatan($pendidikan_terakhir, $id_kecamatan);
      $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      $data['posisi']=$this->m_industri->get_data_posisi();
      $this->load->view('industri/header');
        $this->load->view('industri/see_pencaker', $data);
        $this->load->view('industri/footer');
           
      }

      elseif (isset($_POST['id_posisi']) && isset($_POST['id_kecamatan']) && empty($_POST['pendidikan_terakhir'])) {
      $id_posisi = $_POST['id_posisi'];
      $id_kecamatan = $_POST['id_kecamatan'];
      $data['pencaker']= $this->m_industri->get_data_pencaker_by_posisi_kecamatan($id_posisi, $id_kecamatan);
      $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      $data['posisi']=$this->m_industri->get_data_posisi();
      $this->load->view('industri/header');
      $this->load->view('industri/see_pencaker', $data);
      $this->load->view('industri/footer');
            
      }

      elseif (isset($_POST['id_posisi']) && empty($_POST['id_kecamatan']) && isset($_POST['pendidikan_terakhir'])) {
      $id_posisi = $_POST['id_posisi'];
      $pendidikan_terakhir = $_POST['pendidikan_terakhir'];
      $data['pencaker']= $this->m_industri->get_data_pencaker_by_posisi_pendidikan($id_posisi, $pendidikan_terakhir);
      $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      $data['posisi']=$this->m_industri->get_data_posisi();
      $this->load->view('industri/header');
      $this->load->view('industri/see_pencaker', $data);
      $this->load->view('industri/footer');
            
      }

      elseif (isset($_POST['id_posisi']) && isset($_POST['id_kecamatan']) && isset($_POST['pendidikan_terakhir'])) {
      $id_posisi = $_POST['id_posisi'];
      $pendidikan_terakhir = $_POST['pendidikan_terakhir'];
      $id_kecamatan = $_POST['id_kecamatan'];
      $data['pencaker']= $this->m_industri->get_data_pencaker_by_posisi_pendidikan_kecamatan($id_posisi, $pendidikan_terakhir, $id_kecamatan);
      $data['kecamatan']=$this->m_industri->get_data_kecamatan();
      $data['posisi']=$this->m_industri->get_data_posisi();
      $this->load->view('industri/header');
      $this->load->view('industri/see_pencaker', $data);
      $this->load->view('industri/footer');
            
      }

      else{
         redirect('industri/see_pencaker');
      }
    }

    public function detail_lowongan()
    {
        $id_industri=$this->session->userdata('id_industri');   
        $param=array('id_lowongan'=>$_GET['id_lowongan']);
        $data['lowongan']=$this->m_industri->get_data_lowongan_by_id_pencaker($param,$id_industri);
        $this->load->view('industri/header');
        $this->load->view('industri/detail_lowongan', $data);
        $this->load->view('industri/footer');
    }


    function see_notifikasi()
       {
/*____________________
   code start
**********************/ 
// membuat variable $id_industri di beri value id_industri dari session userdata (id_industri yang login saat ini)
          $id_industri=$this->session->userdata('id_industri');

// mengambil data dari table minat yang sudah di konfirmasi  disnaker, dengan parameter id_industri yang login saat ini.
          $data['minat']= $this->m_industri->get_data_minat_confirm($id_industri);
// mengambil jumlah notifikasi minat yang belum dibaca, digunakan untuk menampilkan ada notifikasi baru yang  ada background merahnya di tampilan
          $data['jumlah_minat']= $this->m_industri->get_jumlah_minat_confirm_unread($id_industri);
          
// mengambil data dari table lamar yang sudah di konfirmasi disnaker, dengan parameter id_industri yang login saat ini.
          $data['lamar']= $this->m_industri->get_data_lamar_confirm($id_industri);
// mengambil jumlah notifikasi lamar yang belum dibaca, digunakan untuk menampilkan ada notifikasi baru yang  ada background merahnya di tampilan
          $data['jumlah_lamar']= $this->m_industri->get_jumlah_lamar_confirm_unread($id_industri);

          $this->load->view('industri/header');
          $this->load->view('industri/see_notifikasi', $data);
          $this->load->view('industri/footer');

       }

    public function detail_pencaker_minat()
    {

/*____________________
   code start
**********************/ 
//1. membuat variable $isi dan $param yang digunakan sebagai parameter saat memanggil method  edit_minat_read
 
        $isi=array(
            'is_read'=> 1
        );

        $param=array(
          'id_minat'=>$_GET['id_minat']
        );
// 2. edit is_read ditable minat menjadi 1. 
$data['minat']=$this->m_industri->edit_minat_read($param, $isi);
    
// 3. ambil data pencaker sesuai dengan id_minat yang didapatkan , yang nanti akan digunakan sebagai info di detail notifikasi
        $data['pencaker']=$this->m_industri->get_data_det_pencaker_notif($param);
        $this->load->view('industri/header');
        $this->load->view('industri/see_detail_notifikasi_minat', $data);
        $this->load->view('industri/footer');
    }

    public function detail_pencaker_lamar()
    {
/*____________________
   code start
**********************/ 
//1. membuat variable $isi dan $param yang digunakan sebagai parameter saat memanggil method  edit_lamar_read
        $isi=array(
            'is_read'=> 1
        );

        $param=array('id_lamar'=>$_GET['id_lamar']);

// 2. edit is_read ditable minat menjadi 1. 
        $data['lamar']=$this->m_industri->edit_lamar_read($param, $isi);

// 3. ambil data pencaker sesuai dengan id_minat yang didapatkan , yang nanti akan digunakan sebagai info di detail notifikasi
        $data['pencaker']=$this->m_industri->get_data_det_pencaker_notif_lamar($param);
        $data['lowongan']=$this->m_industri->get_data_lowongan_by_id_pencaker(array('id_lowongan'=>$data['pencaker']->id_lowongan));

        $this->load->view('industri/header');
        $this->load->view('industri/see_detail_notifikasi_lamar', $data);
        $this->load->view('industri/footer');
    }

    public function saran()
    {

      $id_perusahaan = $this->session->userdata('id_industri');
      $kriteria = array("nama_kecamatan","bidang_keahlian","pendidikan_terakhir","pengalaman","gaji"); //tambahkan kriteria aku ambil yg kecamatan aja.

      $id_pos = $this->uri->segment(3); //ambil idposisi yang dibuka sama perusahaane kalau buka satu lowongan akan ada satu menu gtu.
      

      $industri = $this->db->query("SELECT i.nama_perusahaan,k.nama_kecamatan, l.bidang_keahlian,l.pendidikan_terakhir, l.pengalaman,l.gaji, p.nama_posisi
                      FROM lowongan l
                      INNER JOIN kecamatan k ON l.id_kecamatan = k.id_kecamatan
                      INNER JOIN industri i ON l.id_industri = i.id_industri
                      INNER JOIN posisi p ON l.id_posisi = p.id_posisi
                      WHERE i.id_industri = $id_perusahaan AND p.id_posisi = $id_pos")->row();

                      //echo "<pre>";print_r($industri);// die();
      

      $pencari = $this->db->query("SELECT p.id_pencaker,p.nama_lengkap, k.nama_kecamatan,p.bidang_keahlian, p.pendidikan_terakhir,p.pengalaman, p.gaji,s.nama_posisi, s.id_posisi
                  FROM pencaker p
                  INNER JOIN posisi s ON p.id_posisi = s.id_posisi
                  INNER JOIN kecamatan k ON k.id_kecamatan = p.id_kecamatan
                  WHERE s.id_posisi = $id_pos")->result();

                  //echo "<pre>";print_r($pencari);

      //Delete Nilai digunakan jika ada perubahan data
      $this->db->delete('hasil_perhitungan', array('id_pencaker' => $id_perusahaan)); 

      foreach ($pencari as $date) {
              $benar1 = array();
              $salah1 = array();

              for ($y=0; $y < count($kriteria) ; $y++) { 
                  
                  if ($date->$kriteria[$y] == $industri->$kriteria[$y]) {
                      $benar1[$kriteria[$y]] = 1;
                  }else{
                      $salah1[$kriteria[$y]] = 0;
                  }
              }

              $mirip1   = count($benar1);
              $beda1   = count($salah1);


          //PROSES RUMUS SIMILARITY MEASURE
              $proses1 = round($mirip1/($mirip1+$beda1),2);
              //echo "<h3>".$date->id_pencaker.". HASIL PROSES = ".$mirip1."/(".$mirip1." + ".$beda1.") = ".$proses1."</h3>";

          //Insert Nilai
              $hasil1 = array(
                  'id_pencaker' => $id_perusahaan ,
                  'id_lowongan' => $date->id_pencaker ,
                  'hasil' => $proses1
              );
              $this->db->insert('hasil_perhitungan', $hasil1); 
          }

      $data_pelamar = $this->db->query("SELECT p.id_pencaker, p.nama_lengkap, k.nama_kecamatan
                      FROM hasil_perhitungan h
                      INNER JOIN pencaker p ON p.id_pencaker = h.id_lowongan
                      INNER JOIN kecamatan k ON k.id_kecamatan = p.id_kecamatan
                      WHERE h.hasil = (SELECT MAX(hasil) FROM hasil_perhitungan WHERE id_pencaker = $id_perusahaan) GROUP BY p.id_pencaker")->result();
                      //ini buat nampilkan perhitungan yg maaksimal di tabel perhitungan berdasarkan id pencaker. karena ada satu tabel jadi jangan bingung kalau ada persamaan antara id_pencaker dan id_perusahaan.
                      //echo "<pre>";print_r($data_pelamar); die();
            
     
        $b['pelamar']=$data_pelamar;
        $this->load->view('industri/header');
        $this->load->view('industri/saran', $b);
        $this->load->view('industri/footer');
    }

}

?>