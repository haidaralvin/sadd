        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Konfirmasi Lamar</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table table-striped" align="center">
                                <tr class="success">
                                        <th>Lowongan</th>
                                        <th>Pencaker</th>
                                        <th>Industri</th>
                                        <th>Konfirmasi</th>
                                </tr>
                                <?php $no = $offset;
                                // Memastikan jika data tidak kosong
                                if ($data['query']->num_rows() > 0) {
                                foreach ($data['query']->result() as $row) {
                                    ?>
                                <tr>
                                        <td><a target="_blank" href="<?php echo site_url('link/see_detail_lowongan?id_lowongan='.$row->id_lowongan)?>"><?php echo $row->nama_lowongan; ?></a></td>
                                        <td><a target="_blank" href="<?php echo site_url('link/see_detail_pencaker?id_pencaker='.$row->id_pencaker)?>"><?php echo $row->nama_lengkap; ?></a></td>
                                        <td><a target="_blank" href="<?php echo site_url('link/see_detail_industri?id_industri='.$row->id_industri)?>"><?php echo $row->nama_perusahaan; ?></a></td>
                                        <td>
                                            <a href="<?php echo site_url('link/pro_konfirmasi_lamar?id_lamar='.$row->id_lamar)?>" onclick="return confirm('Yakin ingin konfirmasi data ini?')"><span class="glyphicon glyphicon-ok"></span></a>
                                        </td>
                                </tr>
                                <?php } } // jika data masih kosong, tampilkan pesan
                                else {
                                    echo "<tr><td colspan='4' align='center'>Data masih kosong !</td></tr>";
                                }
                                ?>
                                
                                
                            </table>   
                            <div class="btn-toolbar" role="toolbar"><div class="btn-group"><?php echo $this->pagination->create_links(); ?></div></div>  
                              
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                