<section id="contact-page" class="container">

<h3 style="font-weight: normal;font-size: 16px;border: solid 1px #ff0047;padding: 15px;box-shadow: 1px 1px 5px #d6d6d6;background: rgba(255, 255, 255, 0.07);border-left: solid 5px #ff0047;">
Lamaran Anda Untuk Lowongan <b><i><?php echo $lowongan->nama_lowongan; ?></i></b> di perusahaan <b><?php echo $industri->nama_perusahaan; ?></b> telah disampaikan atau sudah diteruskan ke perusahaan. 
Selanjutnya silahkan menunggu untuk dihubungi atau diminati oleh perusahaan tersebut.
</h3>        

<h4>Detail Notifikasi Lamar</h4>

        <div class="industri">
                <div class="col-md-12">

                     <table class="table table-striped" align="center">                                
                                               
                                <tr>
                                    
                                    <td>Tanggal Lamar: <?php echo $lamar->tanggal_lamar; ?></td>
    
                                </tr>                                
                               <tr>
                                    
                                    <td>Nama  Perusahaan: <?php echo $industri->nama_perusahaan; ?></td>
                                </tr>                                

                                <tr>
                                    
                                    <td>Nama  Lowongan: <?php echo $lowongan->nama_lowongan; ?></td>
                                </tr>                                
                                <tr>
                                    
                                    <td>Kecamatan  : <?php echo $lowongan->nama_kecamatan; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Posisi  : <?php echo $lowongan->nama_posisi; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Gaji : <?php echo $lowongan->gaji; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Pengalaman Dibutuhkan: <?php echo $lowongan->pengalaman; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Bidang Keahlian: <?php echo $lowongan->bidang_keahlian; ?></td>
                                </tr>                 
                                <tr>
                                    
                                    <td>Pendidikan: <?php echo $lowongan->nama_pendidikan; ?></td>
                                </tr>                 
                                <tr>
                                    <td>Tanggal: <?php echo $lowongan->tanggal; ?></td>
                                </tr>
                                <tr>
                                    <td>Deadline : <?php echo $lowongan->deadline; ?></td>
                                </tr>
                              
                                <tr>
                                    <td>Isi : <?php echo $lowongan->isi; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url("pencaker/see_notifikasi")?>"><span class="glyphicon glyphicon-home"></span> Back</a>
                                    </td>
                                </tr>
                                    
                            </table>    
                

                             
                        </div>
                    </div>


<!-- <h4>Info Perusahaan</h4>

        <div class="industri">
                <div class="col-md-12">
    <img class="img-rounded" style="max-height: 100px" src="<?php echo base_url();?>upload/foto_industri/<?php echo $industri->foto_industri;?>"/>
                    <table class="table table-striped" align="center">                                
                                               
                                
                                <tr>

                                    <td>Alamat : <?php echo $industri->alamat; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Kecamatan : <?php echo $industri->nama_kecamatan;?> </td>
                                    
                                </tr>
                                
                                <tr>
                                    <td>Pendiri : <?php echo $industri->nama_pemimpin; ?></td>
                                </tr>
                                <tr>
                                    <td>Tahun Berdiri : <?php echo $industri->tahun_berdiri; ?></td>
                                </tr>
                                <tr>
                                    <td>Bidang Usaha : <?php echo $industri->nama_sub;?> </td>
                                </tr>
                                <tr>
                                    <td>Keterangan : <?php echo $industri->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Terdaftar : <?php echo $industri->tanggal; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url("pencaker/see_notifikasi")?>"><span class="glyphicon glyphicon-home"></span>Back</a>
                                    </td>
                                </tr>
                                
                            </table>    
                

                             
                        </div>
                    </div>
-->
                    
    </section> 