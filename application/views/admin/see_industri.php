        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Industri</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table" align="center">
                                
                                <?php $no = $offset;
                                // Memastikan jika data tidak kosong
                                if ($data['query']->num_rows() > 0) {
                                foreach ($data['query']->result() as $row) {?>
                                <tr>
                                    <td rowspan="5"><img width="100" height="100"src="<?php echo base_url();?>upload/foto_industri/<?php echo $row->foto_industri;?>"</td>
                                        
                                </tr>
                                <tr>
                                    <td>Nama : <?php echo $row->nama_perusahaan; ?></td>
                                </tr>
                                <tr>
                                    <td>Alamat : <?php echo $row->alamat; ?></td>
                                </tr>
                                <tr>
                                     <td>Tahun Berdiri : <?php echo $row->tahun_berdiri; ?></td>
                                </tr>
                                            
                                         
                                <tr>
                                    <td>Bidang Usaha : <?php echo $row->nama_sub;?> </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url('link/see_detail_industri?id_industri='.$row->id_industri)?>"><span class="glyphicon glyphicon-zoom-in"></span>Detail</a>
                                    </td>
                                </tr>
                                  
                                <?php } } // jika data masih kosong, tampilkan pesan
                                else {
                                    echo "<tr><td align='center'>Data masih kosong !</td></tr>";
                                }
                                ?>
                                
                                
                            </table>   
                            <div class="btn-toolbar" role="toolbar"><div class="btn-group"><?php echo $this->pagination->create_links(); ?></div></div>  
                             
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>