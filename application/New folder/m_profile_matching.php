<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_profile_matching extends CI_Model { 


// start
        function get_lowongan(){
                
                $this->db->select('industri.*,lowongan.*,posisi.nama_posisi,industri.nama_perusahaan, kecamatan.nama_kecamatan,sub_kategori.nama_sub');
                $this->db->join('posisi','posisi.id_posisi = lowongan.id_posisi');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= lowongan.id_kecamatan');
                $this->db->join('industri','industri.id_industri = lowongan.id_industri');
                $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori'	);

                                
                $query = $this->db->get("lowongan");
                $data=$query->result_array();
      
                return $data;
        }


        function get_data_usia(){
        	$query=$this->db->select("*")->get("usia");
  			$data=$query->result_array();
			// data di repack dengan i sebagai key
            return $data;
        }

        function get_skor_usia($usia_saat_ini,$data_usia){
        	$skor_master_usia=0;
        	foreach ($data_usia as $data) {
        		if($usia_saat_ini<=$data['batas_max']){
        				$skor_master_usia=$data['id_usia'];
        				break;
        		}
        	}
	
			return $skor_master_usia;
        }

        function get_single_pencaker(){
        	$id_pencaker=$this->session->userdata('id_pencaker');
        	$query=$this->db->select("*")->where('id_pencaker',$id_pencaker)->get("pencaker");
  			$data=$query->row_array();
      
            return $data;
        }


// start render dump
        function render_html_table($table_content){
			$str_html='<table border="1" class="table table-bordered">';
				$str_html.=$table_content;
			$str_html.="</table>";
			return $str_html;
    	}    	    


        function render_html_th($data){
        	$str_html="
			<tr>";
			foreach ($data as $key => $value) {
				$str_html.='<th class="content_pm">';
				$str_html.=$key;
				$str_html.="</th>";
			}
			$str_html.="</tr>";
			return $str_html;
        }

        function render_html_td($data){
        	$str_html="
			<tr>";
			foreach ($data as $key => $value) {
				$str_html.='<td class="content_pm">';
				$str_html.=$value;
				$str_html.="</td>";
			}
			$str_html.="</tr>";
			return $str_html;
        }


        function render_html_title($str_value){
			$str_html="
			<h4>";
				$str_html.=$str_value;
			$str_html.="</h4>";
			return $str_html;
    	}    	        
        function render_html_subtitle($str_value){
			$str_html="
			<h5>";
				$str_html.=$str_value;
			$str_html.="</h5>";
			return $str_html;
    	}    	        

        function render_html_table_single($dat_single){
        	$th=$this->render_html_th($dat_single);
        	$td=$this->render_html_td($dat_single);
        	$table=$this->render_html_table($th.$td);
        	return $table;
        }

        function render_html_table_multiple($data_multiple){
        	$td='';
        	$th='';
        	foreach ($data_multiple as $key=>$data_single) {
        		$array_id_lowongan=array(
        				$this->key_id=>"<b>".$key."</b>"
        			);
        		$data_single_repack=array_merge($array_id_lowongan,$data_single);
	        	$td.=$this->render_html_td($data_single_repack);
        	}
	        	$th.=$this->render_html_th($data_single_repack);

        	$table=$this->render_html_table($th.$td);
        	return $table;
        }

        function join_data($source1,$source2,$new_key){
        	$data=array();
        	foreach ($source1 as $key => $value) {
        		$data[$key]=$value;
        		$data[$key][$new_key]=$source2[$key];
        	}
        	return $data;
        }
        function render_html_col_start(){
        	return '<div class="col-md-6">';
        }
        function render_html_col_end(){
        	return '</div>';
        }

        function dump_lowongan(){
        	echo '<div id="dump_pm">';
        	echo $this->render_html_title("Detail Perhitungan Profile Matching");
        	echo $this->render_html_title("#1 Nilai Target");
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Skor Pencaker Personal");
    	    		echo $this->render_html_table_single($this->skor_pencaker_personal);
        echo $this->render_html_col_end();
        echo $this->render_html_col_start();
        		echo $this->render_html_subtitle("Skor Pencaker Profesional");
					echo $this->render_html_table_single($this->skor_pencaker_profesional);
        echo $this->render_html_col_end();

        	echo $this->render_html_title("#2 Nilai Lowongan");
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Skor Lowongan Personal");
    	    		echo $this->render_html_table_multiple($this->lowongan_personal);
        echo $this->render_html_col_end();
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Skor Lowongan Profesional");
    	    		echo $this->render_html_table_multiple($this->lowongan_profesional);
        echo $this->render_html_col_end();

        	echo $this->render_html_title("#3 Perhitungan Gap");
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Gap Personal");
    	    		echo $this->render_html_table_multiple($this->gap_personal);	        	
        echo $this->render_html_col_end();
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Gap profesional");
    	    		echo $this->render_html_table_multiple($this->gap_profesional);
        echo $this->render_html_col_end();
        		
        	echo $this->render_html_title("#4 Pembobotan");
	        	echo $this->render_html_subtitle("Master Pembobotan");
	    		echo $this->render_html_table_single($this->master_bobot_gap);

	    echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Bobot Personal");
    	    		echo $this->render_html_table_multiple($this->bobot_gap_personal);	        	
	    echo $this->render_html_col_end();
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Bobot profesional");
    	    		echo $this->render_html_table_multiple($this->bobot_gap_profesional);
    	echo $this->render_html_col_end();
		
            	echo $this->render_html_title("#5 Perhitungan dan Pengelompokan Core dan Secondary Factor<br/> Rumus : NCF=( ∑NC(i,c) ) / ( ∑IC ), Rumus : NSF=( ∑NS(i,s) ) / ( ∑IS )");
		echo $this->render_html_col_start();
		        	echo $this->render_html_subtitle("NCF & NSF Personal");
					$this->ncf_personal_display=$this->join_data($this->bobot_gap_personal,$this->ncf_personal,'NCF');
					$this->nsf_personal_display=$this->join_data($this->ncf_personal_display,$this->nsf_personal,'NSF');
	    	    		echo $this->render_html_table_multiple($this->nsf_personal_display);	        	
		echo $this->render_html_col_end();
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("NCF & NSF Profesional");
					$this->ncf_profesional_display=$this->join_data($this->bobot_gap_profesional,$this->ncf_profesional,'NCF');
					$this->nsf_profesional_display=$this->join_data($this->ncf_profesional_display,$this->nsf_profesional,'NSF');
	    	    		echo $this->render_html_table_multiple($this->nsf_profesional_display);	        	
		echo $this->render_html_col_end();
            

            	echo $this->render_html_title("#6 Perhitungan Nilai Total<br/> Rumus : N(i,s) = (60)%.NCF(i,s) + (40)%.NSF(i,s)");
		echo $this->render_html_col_start();
		        	echo $this->render_html_subtitle("Total Aspek Personal");
					$this->ni_personal_display=$this->join_data($this->nsf_personal_display,$this->ni_personal,'Ni');
	    	    		echo $this->render_html_table_multiple($this->ni_personal_display);	        	
		echo $this->render_html_col_end();
        echo $this->render_html_col_start();
		        	echo $this->render_html_subtitle("Total Aspek Profesional");
					$this->ni_profesional_display=$this->join_data($this->ncf_profesional_display,$this->ni_profesional,'Ns');
	    	    		echo $this->render_html_table_multiple($this->ni_profesional_display);	        	
		echo $this->render_html_col_end();

            	echo $this->render_html_title("#6 Perhitungan Ranking<br/> Ranking = (40)%.Ni + (60)%.Ns");
		        	echo $this->render_html_subtitle("Ranking");
				// $data_rangking=$this->join_data($this->ni_personal,$this->ni_profesional,'a');
	    	    		echo $this->render_html_table_single($this->rangkings);	        	

        	echo '</div';
        }

        function rekomendasi_lowongan(){

		/*___________________
		Langkah 1 : persiapan data dan skor penilaian
		___________________________*/
		// ambil profil nya pencaker
		   $row_pencaker = $this->get_single_pencaker();
		// ambil semua data lowongan
		    $data_lowongan = $this->get_lowongan();
		// ambil data master skor hanya sub aspek penilaian usia , yang lainya langsung memanfaatkan ID digunakan sebagai skor.
		    $data_usia= $this->get_data_usia();
		    $usia_saat_ini=getOld($row_pencaker['tanggal_lahir']);

		    $data_master_skor_usia=$this->get_skor_usia($usia_saat_ini,$data_usia);
		    // masukan skor usia ke array row pencaker supaya field konsisten.
		    $row_pencaker['id_usia']=$data_master_skor_usia;

		/*___________________
		Langkah 2 : nilai  pencaker
		___________________________*/

		    $skor_pencaker_personal=array(
		        "id_pendidikan"=>$row_pencaker['id_pendidikan'],
		        "id_kecamatan"=>8,
		        "id_bidang"=>8,
		        "id_usia"=>$row_pencaker['id_usia']
		      );

		    $this->skor_pencaker_personal=$skor_pencaker_personal;

		    $skor_pencaker_profesional=array(
		        "id_pengalaman"=>$row_pencaker['id_pengalaman'],
		        "id_posisi"=>8,
		        "id_gaji"=>$row_pencaker['id_gaji']
		      );
		    $this->skor_pencaker_profesional=$skor_pencaker_profesional;


		/*___________________
		Langkah 3 : nilai lowongan
		___________________________*/
		   $lowongan_personal=array();
		   $lowongan_profesional=array();
		   foreach ($data_lowongan as $row_lowongan) {
		        $lowongan_personal[$row_lowongan['id_lowongan']]=array(
		                        "id_pendidikan"=>$row_lowongan['id_pendidikan'],
		                        "id_kecamatan"=>(($row_pencaker['id_kecamatan']==$row_lowongan['id_kecamatan']) ? 8 : 1) ,
		                        "id_bidang"=>(($row_pencaker['id_bidang']==$row_lowongan['id_bidang']) ? 8 : 1) ,
		                        "id_usia"=>$row_lowongan['id_usia']
		        );

			
		        $lowongan_profesional[$row_lowongan['id_lowongan']]=array(
		                      "id_pengalaman"=>$row_lowongan['id_pengalaman'],
		                      "id_posisi"=>(($row_pencaker['id_posisi']==$row_lowongan['id_posisi']) ? 8 : 1),
		                      "id_gaji"=>$row_lowongan['id_gaji']
		                    );

		    }

		    $this->lowongan_personal=$lowongan_personal;
		    $this->lowongan_profesional=$lowongan_profesional;


		/*___________________
		Langkah 4 : perhitungan GAP, Pembobotan GAP dan perhitingan NCF & NSF 
		___________________________*/
		$master_bobot_gap=array(
		    0=>8,    1=>7.5,    -1=>7,    2=>6.5,    -2=>6,    3=>5.5,    -3=>5,    
		    4=>4.5,   -4=>4,    5=>3.5,    -5=>3,    6=>2.5,    -6=>2,    7=>1.5,    -7=>1,
		  );

			$this->master_bobot_gap=$master_bobot_gap;

		    $gap_personal=array();
		    $gap_profesional=array();

		    $bobot_gap_personal=array();
		    $bobot_gap_profesional=array();

		    $ncf_personal=array();
		    $nsf_personal=array();
		    
		    $ncf_profesional=array();
		    $nsf_profesional=array();


		    $ni_personal=array();
		    $ni_profesional=array();
		    foreach ($lowongan_personal as $idl=>$skor_lowongan_personal) {
		    	// penge-gap-an
		        $gap_personal[$idl]=array(
	              "id_pendidikan"=>($skor_lowongan_personal['id_pendidikan']-$skor_pencaker_personal['id_pendidikan']),
	              "id_kecamatan"=>($skor_lowongan_personal['id_kecamatan']-$skor_pencaker_personal['id_kecamatan']),
	              "id_bidang"=>($skor_lowongan_personal['id_bidang']-$skor_pencaker_personal['id_bidang']),
	              "id_usia"=>($skor_lowongan_personal['id_usia']-$skor_pencaker_personal['id_usia'])
		        );

		        // pembobotan
		        $bobot_gap_personal[$idl]=array(
		              "id_pendidikan"=>$master_bobot_gap[$gap_personal[$idl]['id_pendidikan']],
		              "id_kecamatan"=>$master_bobot_gap[$gap_personal[$idl]['id_kecamatan']],
		              "id_bidang"=>$master_bobot_gap[$gap_personal[$idl]['id_bidang']],
		              "id_usia"=>$master_bobot_gap[$gap_personal[$idl]['id_usia']]
		        );

		        // hitung ncf personal (pendidikan,bidang,usia)
		        $bgp=$bobot_gap_personal[$idl];
		        $ncf_personal[$idl]=round(($bgp['id_pendidikan']+$bgp['id_bidang']+$bgp['id_usia'])/3,2);
		        $nsf_personal[$idl]=round(($bgp['id_kecamatan'])/1,2);

		        // perhitungan Nilai Total (Ni)
		        $ni_personal[$idl]=round((60/100*$ncf_personal[$idl])+(40/100*$nsf_personal[$idl]),2);
		    }

		    foreach ($lowongan_profesional as $idl=>$skor_lowongan_profesional) {
		    	// penge-gap-an profesional
		        $gap_profesional[$idl]=array(
	              "id_pengalaman"=>($skor_lowongan_profesional['id_pengalaman']-$skor_pencaker_profesional['id_pengalaman']),
	              "id_posisi"=>($skor_lowongan_profesional['id_posisi']-$skor_pencaker_profesional['id_posisi']),
	              "id_gaji"=>($skor_lowongan_profesional['id_gaji']-$skor_pencaker_profesional['id_gaji'])
		        );
		        // pembobotan
		        $bobot_gap_profesional[$idl]=array(
		              "id_pengalaman"=>$master_bobot_gap[$gap_profesional[$idl]['id_pengalaman']],
		              "id_posisi"=>$master_bobot_gap[$gap_profesional[$idl]['id_posisi']],
		              "id_gaji"=>$master_bobot_gap[$gap_profesional[$idl]['id_gaji']]
		        );

		       // hitung ncf profesional (pendidikan,bidang,usia)
		        $bgp=$bobot_gap_profesional[$idl];
		        $ncf_profesional[$idl]=round(($bgp['id_pengalaman']+$bgp['id_posisi']+$bgp['id_gaji'])/3,2);
		        $nsf_profesional[$idl]=0;

		        // perhitungan Nilai Total (Ni)
		        $ni_profesional[$idl]=round((60/100*$ncf_profesional[$idl])+(40/100*$nsf_profesional[$idl]),2);

		     }
// setting variable buat dump
 		    $this->gap_personal=$gap_personal;
		    $this->gap_profesional=$gap_profesional;

		    $this->bobot_gap_personal=$bobot_gap_personal;
		    $this->bobot_gap_profesional=$bobot_gap_profesional;

		    $this->ncf_personal=$ncf_personal;
		    $this->nsf_personal=$nsf_personal;
		    
		    $this->ncf_profesional=$ncf_profesional;
		    $this->nsf_profesional=$nsf_profesional;


		    $this->ni_personal=$ni_personal;
		    $this->ni_profesional=$ni_profesional;


		/*___________________
		Langkah 5 : Perhitungan Ranking
		___________________________*/
		$rangkings=array();
		    foreach ($data_lowongan as $row_lowongan) {
		        $idl=$row_lowongan['id_lowongan'];      
		        $rangkings[$idl]=round((40/100*$ni_personal[$idl])+(60/100*$ni_profesional[$idl]),2);
		    }

		arsort($rangkings);
		    $this->rangkings=$rangkings;

		// repack hasil rangking ke dalam data lowongan
		$data_lowongan_key_id=array();
		$data_lowongan_urut=array();

		 foreach ($data_lowongan as $row_lowongan) {
		 	$idl=$row_lowongan['id_lowongan'];

	 		$row_lowongan['ranking']=$rangkings[$idl];
	 		$data_lowongan_key_id[$idl]=$row_lowongan;
		 }

		 foreach ($rangkings as $idl=>$rank) {
		 		$data_lowongan_urut[]=(Object) $data_lowongan_key_id[$idl];
		 }

 		$this->key_id="id_lowongan";

		// $this->dump_lowongan();
		 return (Object)  $data_lowongan_urut;

     }



        function get_pencaker($is_ambil=true){

            $this->db->select('*');
            $this->db->from('pencaker');
            $this->db->where('is_confirm',1);
            if($is_ambil){
            	$this->db->where('is_ambil',0);
            }
            if(isset($_GET['pendidikan_terakhir'])) {
	            if($_GET['pendidikan_terakhir']){
					            $this->db->where('id_pendidikan',$_GET['pendidikan_terakhir']);
	            }
	        }
            
            if(isset($_GET['id_kecamatan'])) {
	            if($_GET['id_kecamatan']){
					            $this->db->where('id_kecamatan',$_GET['id_kecamatan']);
	            }
	        }
            $query= $this->db->get();
            return $query->result_array();
        }

        function get_single_lowongan(){
        	$query=$this->db->select("*");
            if(isset($_GET['id_lowongan'])) {
	            if($_GET['id_lowongan']){
					            $this->db->where('id_lowongan',$_GET['id_lowongan']);
	            }
	        }

	        $query=$this->db->get("lowongan");
  			$data=$query->row_array();
      
            return $data;
        }


       function dump_pencaker(){
        	echo '<div id="dump_pm">';
        	echo $this->render_html_title("Detail Perhitungan Profile Matching");
        	echo $this->render_html_title("#1 Nilai Target");
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Skor Lowongan Personal");
    	    		echo $this->render_html_table_single($this->skor_lowongan_personal);
        echo $this->render_html_col_end();
        echo $this->render_html_col_start();
        		echo $this->render_html_subtitle("Skor Lowongan Profesional");
					echo $this->render_html_table_single($this->skor_lowongan_profesional);
        echo $this->render_html_col_end();

        	echo $this->render_html_title("#2 Nilai Pencaker");
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Skor Pencaker Personal");
    	    		echo $this->render_html_table_multiple($this->pencaker_personal);
        echo $this->render_html_col_end();
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Skor Pencaker Profesional");
    	    		echo $this->render_html_table_multiple($this->pencaker_profesional);
        echo $this->render_html_col_end();

        	echo $this->render_html_title("#3 Perhitungan Gap");
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Gap Personal");
    	    		echo $this->render_html_table_multiple($this->gap_personal);	        	
        echo $this->render_html_col_end();
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Gap profesional");
    	    		echo $this->render_html_table_multiple($this->gap_profesional);
        echo $this->render_html_col_end();
        		
        	echo $this->render_html_title("#4 Pembobotan");
	        	echo $this->render_html_subtitle("Master Pembobotan");
	    		echo $this->render_html_table_single($this->master_bobot_gap);

	    echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Bobot Personal");
    	    		echo $this->render_html_table_multiple($this->bobot_gap_personal);	        	
	    echo $this->render_html_col_end();
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("Bobot profesional");
    	    		echo $this->render_html_table_multiple($this->bobot_gap_profesional);
    	echo $this->render_html_col_end();
		
            	echo $this->render_html_title("#5 Perhitungan dan Pengelompokan Core dan Secondary Factor<br/> Rumus : NCF=( ∑NC(i,c) ) / ( ∑IC ), Rumus : NSF=( ∑NS(i,s) ) / ( ∑IS )");
		echo $this->render_html_col_start();
		        	echo $this->render_html_subtitle("NCF & NSF Personal");
					$this->ncf_personal_display=$this->join_data($this->bobot_gap_personal,$this->ncf_personal,'NCF');
					$this->nsf_personal_display=$this->join_data($this->ncf_personal_display,$this->nsf_personal,'NSF');
	    	    		echo $this->render_html_table_multiple($this->nsf_personal_display);	        	
		echo $this->render_html_col_end();
        echo $this->render_html_col_start();
	        	echo $this->render_html_subtitle("NCF & NSF Profesional");
					$this->ncf_profesional_display=$this->join_data($this->bobot_gap_profesional,$this->ncf_profesional,'NCF');
					$this->nsf_profesional_display=$this->join_data($this->ncf_profesional_display,$this->nsf_profesional,'NSF');
	    	    		echo $this->render_html_table_multiple($this->nsf_profesional_display);	        	
		echo $this->render_html_col_end();
            

            	echo $this->render_html_title("#6 Perhitungan Nilai Total<br/> Rumus : N(i,s) = (60)%.NCF(i,s) + (40)%.NSF(i,s)");
		echo $this->render_html_col_start();
		        	echo $this->render_html_subtitle("Total Aspek Personal");
					$this->ni_personal_display=$this->join_data($this->nsf_personal_display,$this->ni_personal,'Ni');
	    	    		echo $this->render_html_table_multiple($this->ni_personal_display);	        	
		echo $this->render_html_col_end();
        echo $this->render_html_col_start();
		        	echo $this->render_html_subtitle("Total Aspek Profesional");
					$this->ni_profesional_display=$this->join_data($this->ncf_profesional_display,$this->ni_profesional,'Ns');
	    	    		echo $this->render_html_table_multiple($this->ni_profesional_display);	        	
		echo $this->render_html_col_end();

            	echo $this->render_html_title("#6 Perhitungan Ranking<br/> Ranking = (40)%.Ni + (60)%.Ns");
		        	echo $this->render_html_subtitle("Ranking");
				// $data_rangking=$this->join_data($this->ni_personal,$this->ni_profesional,'a');
	    	    		echo $this->render_html_table_single($this->rangkings);	        	

        	echo '</div';
        }

        function rekomendasi_pencaker($is_ambil=true){

		/*___________________
		Langkah 1 : persiapan data dan skor penilaian
		___________________________*/
		// ambil profil nya lowongan
		   $row_lowongan = $this->get_single_lowongan();

		// ambil semua data pencaker
		    $data_pencaker = $this->get_pencaker($is_ambil);
		// ambil data master skor hanya sub aspek penilaian usia , yang lainya langsung memanfaatkan ID digunakan sebagai skor.
		    $data_usia= $this->get_data_usia();

		/*___________________
		Langkah 2 : nilai  lowongan
		___________________________*/

		    $skor_lowongan_personal=array(
		        "id_pendidikan"=>$row_lowongan['id_pendidikan'],
		        "id_kecamatan"=>8,
		        "id_bidang"=>8,
		        "id_usia"=>$row_lowongan['id_usia']
		      );
		    $this->skor_lowongan_personal=$skor_lowongan_personal;

		    $skor_lowongan_profesional=array(
		        "id_pengalaman"=>$row_lowongan['id_pengalaman'],
		        "id_posisi"=>8,
		        "id_gaji"=>$row_lowongan['id_gaji']
		      );
		    $this->skor_lowongan_profesional=$skor_lowongan_profesional;

		/*___________________
		Langkah 3 : nilai pencaker
		___________________________*/
		   $pencaker_personal=array();
		   $pencaker_profesional=array();
		   foreach ($data_pencaker as $row_pencaker) {
		   			    $usia_saat_ini=getOld($row_pencaker['tanggal_lahir']);

		    $data_master_skor_usia=$this->get_skor_usia($usia_saat_ini,$data_usia);
		    // masukan skor usia ke array row lowongan supaya field konsisten.
		    $row_pencaker['id_usia']=$data_master_skor_usia;

		        $pencaker_personal[$row_pencaker['id_pencaker']]=array(
		                        "id_pendidikan"=>$row_pencaker['id_pendidikan'],
		                        "id_kecamatan"=>(($row_lowongan['id_kecamatan']==$row_pencaker['id_kecamatan']) ? 8 : 1) ,
		                        "id_bidang"=>(($row_lowongan['id_bidang']==$row_pencaker['id_bidang']) ? 8 : 1) ,
		                        "id_usia"=>$row_pencaker['id_usia']
		        );

		        $pencaker_profesional[$row_pencaker['id_pencaker']]=array(
		                      "id_pengalaman"=>$row_pencaker['id_pengalaman'],
		                      "id_posisi"=>(($row_lowongan['id_posisi']==$row_pencaker['id_posisi']) ? 8 : 1),
		                      "id_gaji"=>$row_pencaker['id_gaji']
		                    );
		    }

		    $this->pencaker_personal=$pencaker_personal;
		    $this->pencaker_profesional=$pencaker_profesional;


		/*___________________
		Langkah 4 : perhitungan GAP, Pembobotan GAP dan perhitingan NCF & NSF 
		___________________________*/
		$master_bobot_gap=array(
		    0=>8,    1=>7.5,    -1=>7,    2=>6.5,    -2=>6,    3=>5.5,    -3=>5,    
		    4=>4.5,   -4=>4,    5=>3.5,    -5=>3,    6=>2.5,    -6=>2,    7=>1.5,    -7=>1,
		  );

			$this->master_bobot_gap=$master_bobot_gap;

		    $gap_personal=array();
		    $gap_profesional=array();

		    $bobot_gap_personal=array();
		    $bobot_gap_profesional=array();

		    $ncf_personal=array();
		    $nsf_personal=array();
		    
		    $ncf_profesional=array();
		    $nsf_profesional=array();


		    $ni_personal=array();
		    $ni_profesional=array();
		    foreach ($pencaker_personal as $idl=>$skor_pencaker_personal) {
		    	// penge-gap-an
		        $gap_personal[$idl]=array(
	              "id_pendidikan"=>($skor_pencaker_personal['id_pendidikan']-$skor_lowongan_personal['id_pendidikan']),
	              "id_kecamatan"=>($skor_pencaker_personal['id_kecamatan']-$skor_lowongan_personal['id_kecamatan']),
	              "id_bidang"=>($skor_pencaker_personal['id_bidang']-$skor_lowongan_personal['id_bidang']),
	              "id_usia"=>($skor_pencaker_personal['id_usia']-$skor_lowongan_personal['id_usia'])
		        );

		        // pembobotan
		        $bobot_gap_personal[$idl]=array(
		              "id_pendidikan"=>$master_bobot_gap[$gap_personal[$idl]['id_pendidikan']],
		              "id_kecamatan"=>$master_bobot_gap[$gap_personal[$idl]['id_kecamatan']],
		              "id_bidang"=>$master_bobot_gap[$gap_personal[$idl]['id_bidang']],
		              "id_usia"=>$master_bobot_gap[$gap_personal[$idl]['id_usia']]
		        );

		        // hitung ncf personal (pendidikan,bidang,usia)
		        $bgp=$bobot_gap_personal[$idl];
		        $ncf_personal[$idl]=round(($bgp['id_pendidikan']+$bgp['id_bidang']+$bgp['id_usia'])/3,2);
		        $nsf_personal[$idl]=round(($bgp['id_kecamatan'])/1,2);

		        // perhitungan Nilai Total (Ni)
		        $ni_personal[$idl]=round((60/100*$ncf_personal[$idl])+(40/100*$nsf_personal[$idl]),2);
		    }

		    foreach ($pencaker_profesional as $idl=>$skor_pencaker_profesional) {
		    	// penge-gap-an profesional
		        $gap_profesional[$idl]=array(
	              "id_pengalaman"=>($skor_pencaker_profesional['id_pengalaman']-$skor_lowongan_profesional['id_pengalaman']),
	              "id_posisi"=>($skor_pencaker_profesional['id_posisi']-$skor_lowongan_profesional['id_posisi']),
	              "id_gaji"=>($skor_pencaker_profesional['id_gaji']-$skor_lowongan_profesional['id_gaji'])
		        );
		        // pembobotan
		        $bobot_gap_profesional[$idl]=array(
		              "id_pengalaman"=>$master_bobot_gap[$gap_profesional[$idl]['id_pengalaman']],
		              "id_posisi"=>$master_bobot_gap[$gap_profesional[$idl]['id_posisi']],
		              "id_gaji"=>$master_bobot_gap[$gap_profesional[$idl]['id_gaji']]
		        );

		       // hitung ncf profesional (pendidikan,bidang,usia)
		        $bgp=$bobot_gap_profesional[$idl];
		        $ncf_profesional[$idl]=round(($bgp['id_pengalaman']+$bgp['id_posisi']+$bgp['id_gaji'])/3,2);
		        $nsf_profesional[$idl]=0;

		        // perhitungan Nilai Total (Ni)
		        $ni_profesional[$idl]=round((60/100*$ncf_profesional[$idl])+(40/100*$nsf_profesional[$idl]),2);

		     }
// setting variable buat dump
 		    $this->gap_personal=$gap_personal;
		    $this->gap_profesional=$gap_profesional;

		    $this->bobot_gap_personal=$bobot_gap_personal;
		    $this->bobot_gap_profesional=$bobot_gap_profesional;

		    $this->ncf_personal=$ncf_personal;
		    $this->nsf_personal=$nsf_personal;
		    
		    $this->ncf_profesional=$ncf_profesional;
		    $this->nsf_profesional=$nsf_profesional;


		    $this->ni_personal=$ni_personal;
		    $this->ni_profesional=$ni_profesional;


		/*___________________
		Langkah 5 : Perhitungan Ranking
		___________________________*/
		$rangkings=array();
		    foreach ($data_pencaker as $row_pencaker) {
		        $idl=$row_pencaker['id_pencaker'];      
		        $rangkings[$idl]=round((40/100*$ni_personal[$idl])+(60/100*$ni_profesional[$idl]),2);
		    }

		arsort($rangkings);
		    $this->rangkings=$rangkings;

		// repack hasil rangking ke dalam data pencaker
		$data_pencaker_key_id=array();
		$data_pencaker_urut=array();

		 foreach ($data_pencaker as $row_pencaker) {
		 	$idl=$row_pencaker['id_pencaker'];

	 		$row_pencaker['ranking']=$rangkings[$idl];
	 		$data_pencaker_key_id[$idl]=$row_pencaker;
		 }

		 foreach ($rangkings as $idl=>$rank) {
		 		$data_pencaker_urut[]=(Object) $data_pencaker_key_id[$idl];
		 }

 		$this->key_id="id_pencaker";

		// $this->dump_pencaker();
		 return (Object)  $data_pencaker_urut;
     }



// end 

}?>
