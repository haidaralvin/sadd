<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_posisi extends CI_Model { 
        private $table = "posisi";

        function get_data_posisi(){
                $q=$this->db->get('posisi');
                $data=$q->result();
                return $data;
        }
        function get_all_posisi($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table}")->row()->total_rows;
                $data['query'] = $this->db->limit($limit, $offset)->get($this->table);
                return $data;
        }
        
        function get_data_posisi_by_id($data){
                $this->db->where($data);
                $q=$this->db->get($this->table);
                
                $data=$q->first_row();
                return $data;
        }
        function add_posisi($data){
                $q=$this->db->insert($this->table,$data);
                return $q;
        }

        function edit_posisi($data){
                $this->db->where('id_posisi',$data['id_posisi']);
                $q=$this->db->update($this->table,$data);
                return $q;
        }
        
        function delete_posisi($data)
        {
                $q=$this->db->delete($this->table,$data);
                return $q;
        }

        function get_data_nmposisi_by_id($id_posisi){
                $this->db->select('*');
                $this->db->where('id_posisi', $id_posisi);
                $query = $this->db->get('posisi');
                $data=$query->result();
                return $data;
        }
        function ambil_posisi($id_pos)
        {
           $this->db->select('*');
           $this->db->where('id_posisi', $id_pos);
           $query = $this->db->get('posisi');
           
           return $query->row();
        }


}?>