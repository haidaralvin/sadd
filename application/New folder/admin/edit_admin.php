        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Admin</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>link/pro_edit_admin">
								<div class="form-group">
                                    
                                    <div class="col-sm-8">
                                        <input type="hidden" class="form-control" name="id_admin" value="<?php echo $admin->id_admin ?>">
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    
                                    <div class="col-sm-8">                                        
                                        <input type="text" required="required" class="form-control" name="username" value="<?php echo $admin->username?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    
                                    <div class="col-sm-8">
                                        
                                        <input type="password" required="required" class="form-control" name="password" value="<?php echo $admin->password?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-8">
                                  <select class="form-control" name="level">
                                    <option <?php if($admin->level == 'disnaker') {?> selected <?php } ?> value="disnaker">Disnaker</option>
                                    <option <?php if($admin->level == 'disperindag') {?> selected <?php } ?>  value="disperindag">Disperindag</option>
                                    <option <?php if($admin->level == 'kecamatan') {?> selected <?php } ?>  value="kecamatan">Kecamatan</option>                           
                                    
                                  </select>
                                  </div>
                                </div>
                                <div class="form-group">
									
									<div class="col-sm-8">
                                        
										<input type="text" required="required" class="form-control" name="nama_admin" value="<?php echo $admin->nama_admin?>">
									</div>
								</div>
								<div class="col-sm-2 col-sm-offset-2">
										<button type="submit" class="btn btn-primary btn-lg">Submit</button>
									</div>
							</form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
				