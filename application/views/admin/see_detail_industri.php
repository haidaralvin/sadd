        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detail Industri</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table" align="center">                                
                                
                                <tr>
                                    <td align="center"><img width="250" height="250"src="<?php echo base_url();?>upload/foto_industri/<?php echo $industri->foto_industri;?>"</td>
                                    
                                </tr>
                                <tr>
                                    
                                    <td>Nama : <?php echo $industri->nama_perusahaan; ?></td>
                                </tr>                                
                                <tr>

                                    <td>Alamat : <?php echo $industri->alamat; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Kecamatan : <?php echo $industri->nama_kecamatan;?> </td>
                                    
                                </tr>
                                
                                <tr>
                                    <td>Pemimpin : <?php echo $industri->nama_pemimpin; ?></td>
                                </tr>
                                <tr>
                                    <td>Tahun Berdiri : <?php echo $industri->tahun_berdiri; ?></td>
                                </tr>
                                <tr>
                                    <td>Bidang Usaha : <?php echo $industri->nama_sub;?> </td>
                                </tr>
                                <tr>
                                    <td>Keterangan : <?php echo $industri->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td>Email : <?php echo $industri->email; ?></td>
                                </tr>
                                <tr>
                                    <td>Telepon : <?php echo $industri->telepon; ?></td>
                                </tr>
                                <tr>
                                    <td align="center"><img width="400" src="<?php echo base_url();?>upload/ktp_industri/<?php echo $industri->ktp;?>"></td>
                                </tr>
                                <tr>
                                    <td align="center"><img width="400" src="<?php echo base_url();?>upload/imb/<?php echo $industri->imb;?>"></td>
                                </tr>
                                <tr>
                                    <td align="center"><img width="400" src="<?php echo base_url();?>upload/scan_ijin_usaha/<?php echo $industri->scan_ijin_usaha;?>"></td>
                                </tr>
                                <tr>
                                    <td align="center"><img width="400" src="<?php echo base_url();?>upload/ijin_gangguan/<?php echo $industri->ig;?>"></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Terdaftar : <?php echo $industri->tanggal; ?></td>
                                </tr>
                                <tr>
                                    <td>
                        <a class="btn btn-primary" href="javascript:history.back()"> <span class="
                                        glyphicon glyphicon-home"></span> Back</a>
                                    </td>
                                </tr>
                                
                            </table>        
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>