    <section id="contact-page" class="container">
        <div class="row">
                <div class="col-md-12">
                    <h3>Reset Password</h3>                
                </div>
            </div>
        <div class="row">
            <div class="col-sm-8">
            <hr>               
                <form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>industri/pro_reset">
                <div class="form-group">
                    <label  class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-8">
                    <input type="text" required='required' name="email" class="form-control" value="<?php echo $this->session->userdata('email');?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-2 control-label">New Password</label>
                    <div class="col-sm-8">
                    <input type="password" required='required' name="password" class="form-control" placeholder="New Password">
                    </div>
                </div>
                    <div class="center gap" align="center">
                      <button type="submit" name="submit" class="btn btn-theme btn-lg">Reset</button>
                </div> 
                </form>
            </div>
        </div>
    </section>