
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detail Pencari Kerja</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
  <table class="table" align="center">                                
                                
                                <tr>
                                    <td  style="text-align: center;" width="10%"><img width="250" height="250"src="<?php echo base_url();?>upload/foto_pencaker/<?php echo $pencaker->foto;?>"></td>
                                    
                                </tr>

                                <tr>                                    
                                    <td>Nama : <?php echo $pencaker->nama_lengkap; ?></td>
                                </tr>                                
                                
                                <tr>

                                    <td>Gender : <?php echo $pencaker->jk; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Agama : <?php echo $pencaker->agama; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Status : <?php echo $pencaker->status; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>TTL : <?php echo $pencaker->tanggal_lahir; ?>, <?php echo $pencaker->tempat_lahir; ?></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Tanggal Terdaftar : <?php echo $pencaker->tanggal; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Kecamatan : <?php echo $pencaker->nama_kecamatan; ?></td>
                                </tr>
                                <tr>
                                    <td>Pendidikan : <?php echo $pencaker->nama_pendidikan; ?></td>
                                </tr>
                                <tr>
                                    <td>Pengalaman : <?php echo $pencaker->pengalaman; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Usia : <?php echo getOld($pencaker->tanggal_lahir); ?> Tahun</td>
                                </tr>
                                <tr>
                                    <td>Bidang Keahlian : <?php echo $pencaker->bidang_keahlian; ?></td>
                                </tr>
                                <tr>
                                    <td>Posisi : <?php echo $pencaker->nama_posisi; ?></td>
                                </tr>
                                <tr>
                                    <td>Gaji : <?php echo $pencaker->gaji; ?></td>
                                </tr>

                                <tr>
                                    <td>Keterangan : <?php echo $pencaker->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td>CV : <a href="<?php echo base_url();?>upload/cv/<?php echo $pencaker->cv;?>"><?php echo $pencaker->cv;?></td>
                                </tr>
                                <tr>
                                    <td>Scan Ijazah : <img width="400" src="<?php echo base_url();?>upload/scanijazah/<?php echo $pencaker->scanijazah;?>"></td>
                                </tr>
                                
                                <tr>
                                    <td>Scan KTP : <img width="400" src="<?php echo base_url();?>upload/scanktp/<?php echo $pencaker->scanktp;?>"></td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <a class="btn btn-primary" href="javascript:history.back()"> <span class="
                                        glyphicon glyphicon-home"></span> Back</a>


                                    </td>
                                </tr>
                                
                            </table>      
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>