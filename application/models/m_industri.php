<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_industri extends CI_Model {  
        private $table = "industri";

        function getDown()
                    {
                        $this->db->select('*');
                        $this->db->where('id_pencaker');
                        $query =  $this->db->get('pencaker');
                        foreach ($query->result() as $row)
                        {
                        $data = $cv;
                        $name = file_get_contents(base_url()."upload/cv".$cv);
                        }   
                        force_download($name,$data);              
                   }

        function get_data_kecamatan(){
                $this->db->where('id_kecamatan != 0');
                $q=$this->db->get('kecamatan');
                $data=$q->result();
                return $data;
        }

        function get_data_pendidikan(){
                $q=$this->db->get('pendidikan_terakhir');
                $data=$q->result();
                return $data;
        }

        function add_industri($data){
            $q=$this->db->insert('industri',$data);
                return $q;

        }

        function get_data_industri(){
                $q=$this->db->get('industri');
                $data=$q->result();
                return $data;
        }

        function get_all_industri($limit, $offset = 0){
                
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table}")->row()->total_rows;
                $this->db->select('sub_kategori.nama_sub, kecamatan.nama_kecamatan,industri.id_industri, industri.username,industri.nama_perusahaan,industri.alamat,industri.id_kecamatan,industri.tahun_berdiri,industri.telepon,industri.email,industri.id_subkategori,industri.nama_pemimpin,industri.keterangan,industri.tanggal,industri.foto_industri,industri.scan_ijin_usaha,industri.is_confirm');
                $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan');
                
                $data['query'] = $this->db->limit($limit, $offset)->get($this->table);
                return $data;
                
        }
        function get_data_industri_disperindag($limit, $offset = 0){
                
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} WHERE is_confirm=1")->row()->total_rows;
                $this->db->select('sub_kategori.nama_sub, kecamatan.nama_kecamatan,industri.id_industri, industri.username,industri.nama_perusahaan,industri.alamat,industri.id_kecamatan,industri.tahun_berdiri,industri.telepon,industri.email,industri.id_subkategori,industri.nama_pemimpin,industri.keterangan,industri.tanggal,industri.foto_industri,industri.scan_ijin_usaha,industri.is_confirm');
                $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan');
                $data['query'] = $this->db->get_where($this->table, array('is_confirm'=>1), $limit, $offset);
                
                return $data;
                
        }

        function get_data_industri_disperindag_by_tgl($limit, $offset = 0, $awal, $akhir){
                
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} WHERE is_confirm=1")->row()->total_rows;
                $this->db->select('sub_kategori.nama_sub, kecamatan.nama_kecamatan,industri.id_industri, industri.username,industri.nama_perusahaan,industri.alamat,industri.id_kecamatan,industri.tahun_berdiri,industri.telepon,industri.email,industri.id_subkategori,industri.nama_pemimpin,industri.keterangan,industri.tanggal,industri.foto_industri,industri.scan_ijin_usaha,industri.is_confirm');
                $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan');
                $data['query'] = $this->db->get_where($this->table, array('is_confirm'=>1, 'tgl_conf >='=>$awal, 'tgl_conf <='=>$akhir), $limit, $offset);
                
                return $data;
                
        }

        function get_data_industri_konfirmasi($limit, $offset = 0){
                
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} WHERE is_confirm=1")->row()->total_rows;
                $this->db->select('sub_kategori.nama_sub, kecamatan.nama_kecamatan,industri.id_industri, industri.username,industri.nama_perusahaan,industri.alamat,industri.id_kecamatan,industri.tahun_berdiri,industri.telepon,industri.email,industri.id_subkategori,industri.nama_pemimpin,industri.keterangan,industri.tanggal,industri.foto_industri,industri.scan_ijin_usaha,industri.is_confirm');
                $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan');
                $this->db->order_by('industri.tanggal','desc');                
                $data['query'] = $this->db->get_where($this->table, array('is_confirm'=>1), $limit, $offset);
                
                return $data;
                
        }

         function get_home_industri(){
                $q=$this->db->get('industri');
                $data=$q->result();
                return $data;
        }

        function get_data_industri_by_id($data){
                $this->db->select('kecamatan.nama_kecamatan,sub_kategori.nama_sub,industri.*');
                $this->db->where($data);
                $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan');
                $this->db->join('sub_kategori','sub_kategori.id_subkategori= industri.id_subkategori');
                $q=$this->db->get('industri');
                
                $data=$q->first_row();
                return $data;
        }

        function get_data_industriadmin_by_id($data){
                $this->db->select('sub_kategori.nama_sub, kecamatan.nama_kecamatan,industri.id_industri, industri.username,industri.nama_perusahaan,industri.alamat,industri.id_kecamatan,industri.tahun_berdiri,industri.telepon,industri.email,industri.id_subkategori,industri.nama_pemimpin,industri.keterangan,industri.tanggal,industri.foto_industri,industri.scan_ijin_usaha,industri.ktp,industri.imb,industri.ig,industri.is_confirm');
                $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan');
                
                $this->db->where($data);
                $q=$this->db->get('industri');
                
                $data=$q->first_row();
                return $data;
        }

         function edit_industri($data){
                $this->db->where('id_industri',$data['id_industri']);
                $q=$this->db->update('industri',$data);
                return $q;
        }

        function delete_industri($data)
        {
                $q=$this->db->delete($this->table,$data);
                return $q;
        }

        function lihat_pass(){
        $username=$this->input->post('username');
        $email=($this->input->post('email'));
        $query=$this->db->query("select * from industri where username='$username' and email='$email'");
             if ($query->num_rows() > 0) {
                foreach ($query->result() as $data) {
                  $this->session->set_userdata('PASSWORD_DAPAT', $data->username);  
                  $new_pass=md5($data->username);
                  $this->db->query("update industri set password='$new_pass' where username='$username' and email='$email'");
                  redirect('industri/login_form');     
                }
            } else {
                echo "<script>alert('Data Tidak Di Temukan');</script>";
                redirect('industri/oh_saya_lupa?error=','refresh');
            }           
        }

        function ambil_username($username, $password)
        {
            $this->db->select('*');
            $this->db->from('industri');
            $this->db->where('username',$username);
            $this->db->where('password',$password);
            $query= $this->db->get();

            return $query->row_array();
        }

       public function data_industri($username)
        {
           $this->db->select('*');
           $this->db->where('username', $username);
           $query = $this->db->get('industri');
           
           return $query->row();
        }

        function get_data_lowongan_by_id_industri($id_industri){
            $this->db->select('lowongan.id_lowongan, lowongan.nama_lowongan, lowongan.deadline, posisi.nama_posisi')
            ->join('posisi','posisi.id_posisi= lowongan.id_posisi')
            ->where('id_industri',$id_industri);
            $this->db->order_by('deadline','desc');
            $query = $this->db->get('lowongan');
            return $query->result();
        }

        function get_data_posisi(){
                $q=$this->db->get('posisi');
                $data=$q->result();
                return $data;
        }


        function get_data_industri_by_id_kec($id_kecamatan){
                $this->db->select('*');
                $this->db->where('id_kecamatan', $id_kecamatan);
                $query = $this->db->get('industri');
                $data=$query->result();
                return $data;
        }

        function get_data_pencaker_by_id_industri($id_industri){
            $this->db->select('*');
            $this->db->from('pencaker');
            $this->db->where('is_confirm',1);
            $this->db->where('is_ambil',0);
            $query= $this->db->get('');
            return $query->result();
        }

        function get_data_pencaker(){
            $this->db->select('*');
            $this->db->from('pencaker');
            $this->db->where('is_confirm',1);
            $this->db->where('is_ambil',0);
            $query= $this->db->get();
            return $query->result();
        }
        
        function get_data_pencaker_by_id_pencaker($data){
                $this->db->select('pencaker.id_pencaker, pencaker.no_ktp, pencaker.nama_lengkap, pencaker.alamat, pencaker.email, pencaker.telepon, pencaker.tanggal_lahir, pencaker.tempat_lahir, pencaker.jk, pencaker.agama, pencaker.id_pendidikan, pencaker.kls,pencaker.thn,pencaker.sd,pencaker.smp,
                    pencaker.sma,pencaker.d_ii,pencaker.akta_ii,pencaker.akta_iii,pencaker.akta_iv,pencaker.akta_v, pencaker.id_pengalaman, pencaker.keterangan, pencaker.status, pencaker.tanggal, pencaker.cv, pencaker.foto, pencaker.scanijazah, pencaker.scanktp, kecamatan.nama_kecamatan,pt.nama_pendidikan pendidikan_terakhir,pl.pengalaman');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= pencaker.id_kecamatan');
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan= pencaker.id_pendidikan');
                $this->db->join('pengalaman pl','pl.id_pengalaman= pencaker.id_pengalaman');
                $this->db->where($data);
                $q=$this->db->get('pencaker');
                $data=$q->first_row();
                return $data;
                }

        function SearchResult($perPage,$uri,$isi,$id_industri)
        {
            $this->db->select('lowongan.nama_lowongan, lowongan.deadline, posisi.nama_posisi');
           $this->db->join('posisi','posisi.id_posisi= lowongan.id_posisi');
           $this->db->from('lowongan');
           $this->db->where('id_industri',$id_industri);
           if(!empty($isi)) {
            $this->db->like('nama_lowongan', $isi);
            $this->db->where('id_industri',$id_industri);
           }
           $this->db->order_by('id_lowongan','asc');
           $getData = $this->db->get('', $perPage, $uri);
         
           if($getData->num_rows() > 0)
            return $getData->result_array();
           else
            return null;
          }

          function get_data_pencaker_by_pendidikan($pendidikan_terakhir)
          {
            $this->db->select('*');
                $this->db->where('pendidikan_terakhir', $pendidikan_terakhir);
                $this->db->where('is_confirm',1);
                $this->db->where('is_ambil',0);
                $query = $this->db->get('pencaker');
                $data=$query->result();
                return $data;
          }


          function get_data_pencaker_by_kecamatan($id_kecamatan)
          {
            $this->db->select('*');
                $this->db->where('id_kecamatan', $id_kecamatan);
                $this->db->where('is_confirm',1);
                $this->db->where('is_ambil',0);
                $query = $this->db->get('pencaker');
                $data=$query->result();
                return $data;
          }

          function get_data_pencaker_by_posisi($id_posisi)
          {
            $this->db->select('*');
                $this->db->where('id_posisi', $id_posisi);
                $this->db->where('is_confirm',1);
                $this->db->where('is_ambil',0);
                $query = $this->db->get('pencaker');
                $data=$query->result();
                return $data;
          }

          function get_data_pencaker_by_pendidikan_kecamatan($pendidikan_terakhir, $id_kecamatan)
          {
            $this->db->select('*');
                $this->db->where('pendidikan_terakhir', $pendidikan_terakhir);
                $this->db->where('id_kecamatan', $id_kecamatan);
                $this->db->where('is_confirm',1);
                $this->db->where('is_ambil',0);
                $query = $this->db->get('pencaker');
                $data=$query->result();
                return $data;
          }

          function get_data_pencaker_by_posisi_kecamatan($id_posisi, $id_kecamatan)
          {
            $this->db->select('*');
                $this->db->where('id_posisi', $id_posisi);
                $this->db->where('id_kecamatan', $id_kecamatan);
                $this->db->where('is_confirm',1);
                $this->db->where('is_ambil',0);
                $query = $this->db->get('pencaker');
                $data=$query->result();
                return $data;
          }

          function get_data_pencaker_by_posisi_pendidikan($id_posisi, $pendidikan_terakhir)
          {
            $this->db->select('*');
                $this->db->where('id_posisi', $id_posisi);
                $this->db->where('pendidikan_terakhir', $pendidikan_terakhir);
                $this->db->where('is_confirm',1);
                $this->db->where('is_ambil',0);
                $query = $this->db->get('pencaker');
                $data=$query->result();
                return $data;
          }

          function get_data_pencaker_by_posisi_pendidikan_kecamatan($id_posisi, $pendidikan_terakhir, $id_kecamatan)
          {
            $this->db->select('*');
                $this->db->where('id_posisi', $id_posisi);
                $this->db->where('pendidikan_terakhir', $pendidikan_terakhir);
                $this->db->where('id_kecamatan', $id_kecamatan);
                $this->db->where('is_confirm',1);
                $this->db->where('is_ambil',0);
                $query = $this->db->get('pencaker');
                $data=$query->result();
                return $data;
          }


          function get_data_minat_confirm($id_industri)
          {
            $this->db->select('minat.*,pencaker.nama_lengkap, pencaker.id_pencaker');
            $this->db->join('pencaker','pencaker.id_pencaker = minat.id_pencaker');
            $this->db->where('minat.is_confirm',1);
            $this->db->where('minat.id_industri',$id_industri);
            $this->db->order_by('minat.tanggal_minat','desc');
            $query= $this->db->get('minat');
            $data=$query->result();
            return $data;
          }

          function get_jumlah_minat_confirm_unread($id_industri)
          {
            $this->db->select('count(minat.id_minat) jumlah');
            $this->db->join('pencaker','pencaker.id_pencaker = minat.id_pencaker');
            $this->db->where('minat.is_confirm',1);
            $this->db->where('minat.is_read_by_industri',0);
            $this->db->where('minat.id_industri',$id_industri);
            $query= $this->db->get('minat');
            $data=$query->row();
            return $data;
          }


        

        function edit_minat_read($param, $isi){
                $this->db->where($param);
                return $this->db->update('minat',$isi);
                
        }

        function edit_lamar_read($param, $isi){
                $this->db->where($param);
                return $this->db->update('lamar',$isi);
                
        }


          function get_data_lamar_confirm($id_industri)
          {
            $this->db->select('lamar.*,pencaker.nama_lengkap, lowongan.nama_lowongan');
            $this->db->join('pencaker','pencaker.id_pencaker = lamar.id_pencaker');
            $this->db->join('lowongan','lowongan.id_lowongan = lamar.id_lowongan');
            $this->db->where('lamar.is_confirm',1);
            $this->db->where('lamar.id_industri',$id_industri);
            $this->db->order_by('lamar.tanggal_lamar','desc');
            $query= $this->db->get('lamar');
            $data=$query->result();
            return $data;
          }

          function get_jumlah_lamar_confirm_unread($id_industri)
          {
            $this->db->select('count(lamar.id_lamar) jumlah');
            $this->db->join('pencaker','pencaker.id_pencaker = lamar.id_pencaker');
            $this->db->join('lowongan','lowongan.id_lowongan = lamar.id_lowongan');
            $this->db->where('lamar.is_confirm',1);
            $this->db->where('lamar.is_read_by_industri',0);
            $this->db->where('lamar.id_industri',$id_industri);
            $query= $this->db->get('lamar');
            $data=$query->row();
            return $data;
          }




          function get_data_det_pencaker_notif($data){
                $this->db->select('p.*,k.nama_kecamatan,ps.nama_posisi,pt.nama_pendidikan,b.bidang_keahlian, g.gaji,pl.pengalaman');
                $this->db->join('kecamatan k','k.id_kecamatan=p.id_kecamatan');
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=p.id_pendidikan');
                $this->db->join('posisi ps','ps.id_posisi=p.id_posisi');
                $this->db->join('bidang_keahlian b','b.id_bidang=p.id_bidang');
                $this->db->join('gaji g','g.id_gaji=p.id_gaji');
                $this->db->join('pengalaman pl','pl.id_pengalaman=p.id_pengalaman');
                $this->db->join('minat m','m.id_pencaker= p.id_pencaker');
          
                $this->db->where($data);
                $query = $this->db->get('pencaker p');

                $data=$query->row();
                return $data;
                }
        function get_data_det_industri_notif($data){
                $this->db->select('sub_kategori.nama_sub, kecamatan.nama_kecamatan,industri.id_industri, industri.username,industri.nama_perusahaan,industri.alamat,industri.id_kecamatan,industri.tahun_berdiri,industri.telepon,industri.email,industri.id_subkategori,industri.nama_pemimpin,industri.keterangan,industri.tanggal,industri.foto_industri,industri.scan_ijin_usaha,industri.is_confirm');
                $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori','inner');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan','inner');
                $this->db->join('minat','minat.id_industri= industri.id_industri');
                $this->db->where($data);
                $q=$this->db->get('industri');
                $data=$q->first_row();
                return $data;
                }

        function get_data_det_pencaker_notif_lamar($data){
                
                $this->db->select('p.*,k.nama_kecamatan,ps.nama_posisi,pt.nama_pendidikan,b.bidang_keahlian, g.gaji,pl.pengalaman,lm.id_lowongan');
                $this->db->join('kecamatan k','k.id_kecamatan=p.id_kecamatan');
                $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=p.id_pendidikan');
                $this->db->join('posisi ps','ps.id_posisi=p.id_posisi');
                $this->db->join('bidang_keahlian b','b.id_bidang=p.id_bidang');
                $this->db->join('gaji g','g.id_gaji=p.id_gaji');
                $this->db->join('pengalaman pl','pl.id_pengalaman=p.id_pengalaman');
                $this->db->join('lamar lm','lm.id_pencaker= p.id_pencaker');
          
                $this->db->where($data);
                $query = $this->db->get('pencaker p');

                $data=$query->row();
                return $data;

                }
        function get_data_det_industri_notif_lamar($data){
                $this->db->select('sub_kategori.nama_sub, kecamatan.nama_kecamatan,industri.id_industri, industri.username,industri.nama_perusahaan,industri.alamat,industri.id_kecamatan,industri.tahun_berdiri,industri.telepon,industri.email,industri.id_subkategori,industri.nama_pemimpin,industri.keterangan,industri.tanggal,industri.foto_industri,industri.scan_ijin_usaha,industri.is_confirm');
                $this->db->join('sub_kategori','industri.id_subkategori= sub_kategori.id_subkategori','inner');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= industri.id_kecamatan','inner');
                $this->db->join('lamar','lamar.id_pencaker= pencaker.id_pencaker');
                $this->db->where($data);
                $q=$this->db->get('industri');
                $data=$q->first_row();
                return $data;
                }





      function get_data_lowongan_by_id_pencaker($data)
      {
      $this->db->select('lowongan.id_lowongan, lowongan.nama_lowongan, lowongan.isi, posisi.nama_posisi, kecamatan.nama_kecamatan, industri.nama_perusahaan, lowongan.tanggal, lowongan.deadline,pt.nama_pendidikan,b.bidang_keahlian,g.gaji,pl.pengalaman,u.usia');
      ;
      $this->db->join('posisi','posisi.id_posisi= lowongan.id_posisi','inner');
      $this->db->join('kecamatan','kecamatan.id_kecamatan= lowongan.id_kecamatan','inner');
      $this->db->join('industri','industri.id_industri= lowongan.id_industri','inner');
      
      $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=lowongan.id_pendidikan');
      
      $this->db->join('bidang_keahlian b','b.id_bidang=lowongan.id_bidang');
      $this->db->join('gaji g','g.id_gaji=lowongan.id_gaji');
      $this->db->join('pengalaman pl','pl.id_pengalaman=lowongan.id_pengalaman');
      $this->db->join('usia u','u.id_usia=lowongan.id_usia');


          $this->db->where($data);
            $q=$this->db->get('lowongan');
            $data=$q->first_row();
            return $data;
     
        
           }
    }
    ?>