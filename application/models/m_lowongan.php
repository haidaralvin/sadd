<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_lowongan extends CI_Model {  
        private $table = "lowongan";

        function get_data_lowongan($limit, $offset = 0){
                
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table}")->row()->total_rows;
                $this->db->select('posisi.nama_posisi,industri.nama_perusahaan, kecamatan.nama_kecamatan,lowongan.id_lowongan,lowongan.nama_lowongan,lowongan.isi,lowongan.tanggal,lowongan.deadline,lowongan.is_deadline');
                $this->db->join('posisi','posisi.id_posisi = lowongan.id_posisi');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= lowongan.id_kecamatan');
                $this->db->join('industri','industri.id_industri = lowongan.id_industri');
                                
                $data['query'] = $this->db->limit($limit, $offset)->get($this->table);
                return $data;
                
        }

        function get_data_lowongan_by_id($data){
                      $this->db->select('lowongan.*, posisi.nama_posisi, kecamatan.nama_kecamatan, industri.nama_perusahaan,pt.nama_pendidikan,b.bidang_keahlian,g.gaji,pl.pengalaman,u.usia');
      ;
      $this->db->join('posisi','posisi.id_posisi= lowongan.id_posisi','inner');
      $this->db->join('kecamatan','kecamatan.id_kecamatan= lowongan.id_kecamatan','inner');
      $this->db->join('industri','industri.id_industri= lowongan.id_industri','inner');
      
      $this->db->join('pendidikan_terakhir pt','pt.id_pendidikan=lowongan.id_pendidikan');
      
      $this->db->join('bidang_keahlian b','b.id_bidang=lowongan.id_bidang');
      $this->db->join('gaji g','g.id_gaji=lowongan.id_gaji');
      $this->db->join('pengalaman pl','pl.id_pengalaman=lowongan.id_pengalaman');
      $this->db->join('usia u','u.id_usia=lowongan.id_usia');


                $this->db->where($data);
                $q=$this->db->get($this->table);
                
                $data=$q->first_row();
                return $data;
        }
        function get_data_lowongan_by_id2($id_posisi){
                $this->db->select('posisi.nama_posisi,industri.nama_perusahaan, kecamatan.nama_kecamatan,lowongan.id_industri,lowongan.id_lowongan,lowongan.nama_lowongan,lowongan.isi,lowongan.tanggal,lowongan.deadline,lowongan.is_deadline');
                $this->db->join('posisi','posisi.id_posisi = lowongan.id_posisi');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= lowongan.id_kecamatan');
                $this->db->join('industri','industri.id_industri = lowongan.id_industri');
                $this->db->where('lowongan.id_posisi',$id_posisi);
                $q=$this->db->get($this->table);
                
                $data=$q->result();
                return $data;
                
        }
        function get_data_lowongan_by_id3($id_kecamatan){
                $this->db->select('posisi.nama_posisi,industri.nama_perusahaan, kecamatan.nama_kecamatan,lowongan.id_industri,lowongan.id_lowongan,lowongan.nama_lowongan,lowongan.isi,lowongan.tanggal,lowongan.deadline,lowongan.is_deadline');
                $this->db->join('posisi','posisi.id_posisi = lowongan.id_posisi');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= lowongan.id_kecamatan');
                $this->db->join('industri','industri.id_industri = lowongan.id_industri');
                
                $this->db->where('lowongan.id_kecamatan',$id_kecamatan);
                $q=$this->db->get($this->table);
                
                $data=$q->result();
                return $data;
                
        }
        function get_data_lowongan_by_id4($id_industri){
                $this->db->select('posisi.nama_posisi,industri.nama_perusahaan, kecamatan.nama_kecamatan,lowongan.id_industri,lowongan.id_lowongan,lowongan.nama_lowongan,lowongan.isi,lowongan.tanggal,lowongan.deadline,lowongan.is_deadline');
                $this->db->join('posisi','posisi.id_posisi = lowongan.id_posisi');
                $this->db->join('kecamatan','kecamatan.id_kecamatan= lowongan.id_kecamatan');
                $this->db->join('industri','industri.id_industri = lowongan.id_industri');
                $this->db->where('lowongan.id_industri',$id_industri);
                $q=$this->db->get($this->table);
                
                $data=$q->result();
                return $data;
                
        }
        
        function delete_lowongan($data)
        {
                $q=$this->db->delete('lowongan',$data);
                return $q;
        }
        
        function get_data_posisi(){
                $q=$this->db->get('posisi');
                $data=$q->result();
                return $data;
        }


        function get_jml_lowowngan($id_posisi){
            $this->db->count_all_results('id_posisi')
            ->where('id_posisi',$id_posisi);
            $query = $this->db->get('lowongan');
            return $query->result();

        }
        function get_data_kecamatan(){
                $q=$this->db->get('kecamatan');
                $data=$q->result();
                return $data;
        }

        function get_data_kecamatan_by_id($id_kecamatan){
                $this->db->select('*');
                $this->db->where('id_kecamatan', $id_kecamatan);
                $query = $this->db->get('kecamatan');
                $data=$query->result();
                return $data;
        }

        function get_data_industri(){
                $q=$this->db->get('industri');
                $data=$q->result();
                return $data;
        }

        function add_lowongan($data){
            $q=$this->db->insert('lowongan',$data);
            if($q){
              return   $this->db->insert_id();
            }else{
                 return $q;                
            }

        }
        function get_data_lowongan_by_id_kategori($id_kategori){
                $this->db->select('*');
                $this->db->where('id_kategori', $id_kategori);
                $query = $this->db->get('lowongan');
                $data=$query->result();
                return $data;
        }

        function add_minat($data1){
                $q=$this->db->insert('minat',$data1);
                return $this->db->insert_id(); // balikan id yang di baru dibuat


        }
        function add_lamar($data1){
                $q=$this->db->insert('lamar',$data1);
                return $this->db->insert_id(); // balikan id yang di baru dibuat
        }
    }
    ?>