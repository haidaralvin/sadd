        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Detail Pencari Kerja</h1>
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table" align="center">                                
                                
                                <tr>
                                    <td align="center"><img width="250" height="250"src="<?php echo base_url();?>upload/foto_pencaker/<?php echo $pencaker->foto;?>"</td>
                                    
                                </tr>
                                <tr>                                   
                                    <td>Nama : <?php echo $pencaker->nama_lengkap; ?></td>
                                </tr>                                
                                <tr>
                                    <td>Gender : <?php echo $pencaker->jk; ?></td>
                                </tr>
                                <tr>
                                    <td>Agama : <?php echo $pencaker->agama; ?></td>
                                </tr>
                                <tr>
                                    <td>Status : <?php echo $pencaker->status; ?></td>
                                </tr>
                                <tr>
                                    <td>Alamat : <?php echo $pencaker->alamat; ?></td>
                                </tr>
                                <tr>
                                    <td>TTL : <?php echo $pencaker->tanggal_lahir; ?>, <?php echo $pencaker->tempat_lahir; ?></td>
                                </tr>
                                <tr>
                                    <td>Pendidikan : <?php echo $pencaker->pendidikan_terakhir; ?></td>
                                </tr>
                                <tr>
                                    <td>Pengalaman : <?php echo $pencaker->pengalaman; ?></td>
                                </tr>
                                <tr>
                                    <td>Keterangan : <?php echo $pencaker->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td>Email : <?php echo $pencaker->email; ?></td>
                                </tr>
                                <tr>
                                    <td>Telepon : <?php echo $pencaker->telepon; ?></td>
                                </tr>
                                <tr>
                                    <td align="center"><img width="400" src="<?php echo base_url();?>upload/scanijazah/<?php echo $pencaker->scanijazah;?>"</td>
                                </tr>
                                <tr>
                                    <td align="center"><img width="400" height="200"src="<?php echo base_url();?>upload/scanktp/<?php echo $pencaker->scanktp;?>"</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Terdaftar : <?php echo $pencaker->tanggal; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url("link/laporan_kecamatan")?>"><span class="glyphicon glyphicon-home"></span></a>
                                    </td>
                                </tr>
                                
                            </table>        
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>