<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ckeditor extends CI_Controller {
 
	public $data 	= 	array();
 
	function __construct()
	{
		parent:: __construct();
		
		$this->data['ckeditor'] = array(
 
			//ID of the textarea that will be replaced
			'id' 	=> 	'content',
			'path'	=>	'js/ckeditor',
 
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Full", 	//Using the Full toolbar
				'width' 	=> 	"550px",	//Setting a custom width
				'height' 	=> 	'100px',	//Setting a custom height
 
			),
 
			//Replacing styles from the "Styles tool"
			'styles' => array(
 
				//Creating a new style named "style 1"
				'style 1' => array (
					'name' 		=> 	'Blue Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 	=> 	'Blue',
						'font-weight' 	=> 	'bold'
					)
				),
 
				//Creating a new style named "style 2"
				'style 2' => array (
					'name' 	=> 	'Red Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 		=> 	'Red',
						'font-weight' 		=> 	'bold',
						'text-decoration'	=> 	'underline'
					)
				)				
			)
		);
 
		$this->data['ckeditor_2'] = array(
 
			'id' 	=> 	'content_2',
			'path'	=>	'js/ckeditor',
 
			//Optionnal values
			'config' => array(
				'width' 	=> 	"550px",	//Setting a custom width
				'height' 	=> 	'100px',	//Setting a custom height
				'toolbar' 	=> 	array(	//Setting a custom toolbar
					array('Bold', 'Italic'),
					array('Underline', 'Strike', 'FontSize'),
					array('Smiley'),
					'/'
				)
			),
 
			'styles' => array(
 
				//Creating a new style named "style 1"
				'style 3' => array (
					'name' 		=> 	'Green Title',
					'element' 	=> 	'h3',
					'styles' => array(
						'color' 	=> 	'Green',
						'font-weight' 	=> 	'bold'
					)
				)
 
			)
		);		
 
 
	}
 
	public function index() {
		
		$data=$this->data;
		$data['kecamatan']=$this->m_industri->get_data_kecamatan();
		//$data['kategori']=$this->m_kategori->get_data_kategori();
		$data['sub_kategori']=$this->m_sub_kategori->get_data_sub_kategori();
		$this->load->view('header');
		$this->load->view('industri/register_industri', $data);
		$this->load->view('footer');
 
 
	}
	public function index2() {
		
		$data=$this->data;
		$data['kecamatan']=$this->m_kecamatan->get_data_kecamatan();
		//$data['kategori']=$this->m_kategori->get_data_kategori();
		$data['posisi']=$this->m_posisi->get_data_posisi();
		$this->load->view('header');
		$this->load->view('pencaker/reg_pencaker', $data);
		$this->load->view('footer');
 
	}
}
?>