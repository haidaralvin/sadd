<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_sub_kategori extends CI_Model {
        private $table = "sub_kategori";

        function get_data_sub_kategori(){
                $q=$this->db->get('sub_kategori');
                $data=$q->result();
                return $data;
        }
        function get_data_subkategori($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} JOIN kategori ON kategori.id_kategori = sub_kategori.id_kategori")->row()->total_rows;
                $this->db->select('sub_kategori.id_subkategori,sub_kategori.nama_sub, kategori.nama_kategori');
                $this->db->join('kategori','kategori.id_kategori= sub_kategori.id_kategori');
                $data['query'] = $this->db->limit($limit, $offset)->get($this->table);
                return $data;
        }
        function get_data_sub_kategori_by_id($data){
                $this->db->where($data);
                $q=$this->db->get($this->table);
                
                $data=$q->first_row();
                return $data;
        }
        function get_all_sub_kategori(){
                $q=$this->db->get($this->table);
                $data=$q->result();
                return $data;
        }
        function add_sub_kategori($data){
                $q=$this->db->insert($this->table,$data);
                return $q;
        }

        function edit_sub_kategori($data){
                $this->db->where('id_subkategori',$data['id_subkategori']);
                $q=$this->db->update($this->table,$data);
                return $q;
        }
        
        function delete_sub_kategori($data)
        {
                $q=$this->db->delete($this->table,$data);
                return $q;
        }

        function get_data_nmsub($id_subkategori){
                $this->db->select('*');
                $this->db->where('id_kategori', $id_kategori);
                $query = $this->db->get('sub_kategori');
                $data=$query->result();
                return $data;
        }


        function get_nmkateg_by_idsub($id_subkategori){
                        $this->db->select('sub_kategori.nama_sub, kategori.nama_kategori')
                        ->join('kategori','kategori.id_kategori= sub_kategori.id_kategori')
                        ->where('id_subkategori',$id_subkategori);
                $query = $this->db->get('sub_kategori');
                return $query->row();
                }
}?>