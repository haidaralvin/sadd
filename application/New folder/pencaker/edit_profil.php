<section id="contact-page" class="container">
        
		<div class="row">
            <div class="col-sm-8">
            <hr>
            	<form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>pencaker/pro_edit_profil">
			<hr>
				<div class="center gap">
                      <h3 align="center">Data Pribadi</h3>
                </div> 
				<div class="form-group">
					<label  class="col-sm-2 control-label">NIK</label>
					<div class="col-sm-8">
					<input type="text" name="no_ktp" class="form-control"  value="<?php echo $pencaker->no_ktp ?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Nama Lengkap</label>
					<div class="col-sm-8">
					<input type="text" name="nama_lengkap" class="form-control"  value="<?php echo $this->session->userdata('nama_lengkap');?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Tanggal Lahir</label>
					<div class="col-sm-8">
					<input type="date" name="tanggal_lahir" class="form-control"  value="<?php echo $this->session->userdata('tanggal_lahir');?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Tempat Lahir</label>
					<div class="col-sm-8">
					<input type="text" name="tempat_lahir" class="form-control"  value="<?php echo $this->session->userdata('tempat_lahir');?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Jenis Kelamin</label>
					<div class="col-sm-8">
					<input type="text" name="jk" class="form-control"  value="<?php echo $this->session->userdata('jk');?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Status</label>
					<div class="col-sm-8">
					<select name="status" class="form-control">
						<option>Menikah</option>
						<option>Belum Menikah</option>
					</select>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Agama</label>
					<div class="col-sm-8">
					<select name="agama" class="form-control">
						<option>Islam</option>
						<option>Kristen</option>
						<option>Katolik</option>
						<option>Hindu</option>
						<option>Budha</option>
					</select>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Telepon</label>
					<div class="col-sm-8">
					<input type="text" name="telepon" class="form-control" value="<?php echo $this->session->userdata('telepon');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Alamat</label>
					<div class="col-sm-8">
					<input type="text" name="alamat" class="form-control" value="<?php echo $this->session->userdata('alamat');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Kecamatan</label>
					<div class="col-sm-8">
					<select name="id_kecamatan" class="form-control">
						<?php foreach ($kecamatan as $row): ?>
                                            <option value="<?php echo $row->id_kecamatan ;?>">&nbsp;<?php echo $row->nama_kecamatan; ?></option>
                                         <?php endforeach;?>
					</select>

					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Pend.Terakhir</label>
					<div class="col-sm-8">
					<select name="pendidikan_terakhir" class="form-control" >
						<option>SD/MI</option>
						<option>SMP/MTS</option>
						<option>SMA/SMK/MA</option>
						<option>D1</option>
						<option>D2</option>
						<option>D3</option>
						<option>D4</option>
						<option>S1</option>
						<option>S2</option>
						<option>S3</option>
					</select>
					</div>
				</div>
				<hr>
				<div class="center gap">
                      <h3 align="center">Pendidikan Formal/Non Formal</h3>
                </div>
                <div class="form-group">
					<label  class="col-sm-2 control-label">Tidak Tamat SD/Sampai Kelas</label>
					<div class="col-sm-2">
					&nbsp;<input type="text" name="kls" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('kls');?>">
					</div>
					<div class="col-sm-2">
					&nbsp;<input type="text" name="thn" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('thn');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">SD/Sederajat</label>
					<div class="col-sm-2">
					<input type="text" name="sd" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('sd');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">SMTP/Sederajat</label>
					<div class="col-sm-2">
					<input type="text" name="smp" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('smp');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">SMTA/DI/
					AKTA&nbsp;I</label>
					<div class="col-sm-2">
					<input type="text" name="sma" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('sma');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">SM/DII/DIII</label>
					<div class="col-sm-2">
					<input type="text" name="d_ii" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('d_ii');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">AKTA II</label>
					<div class="col-sm-2">
					<input type="text" name="akta_ii" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('akta_ii');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">AKTA III</label>
					<div class="col-sm-2">
					<input type="text" name="akta_iii" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('akta_iii');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">S/PASCA/SI/
					AKTA IV/D IV</label>
					<div class="col-sm-2">
					<input type="text" name="akta_iv" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('akta_iv');?>">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">DOKTOR/SII/
					AKTA V</label>
					<div class="col-sm-2">
					<input type="text" name="akta_v" class="form-control" placeholder="Tahun" value="<?php echo $this->session->userdata('akta_v');?>">
					</div>
				</div>
                </hr>
				<hr>
				<div class="center gap">
                      <h3 align="center">Unggah Berkas</h3>
                </div> 
                <font color="red">*) Ukuran File Max.200Kb dan foto Max.200x200 pixels</font><br>
                &nbsp;<div class="form-group">
					<label  class="col-sm-2 control-label">CV </label>
					<label  class="col-sm-2 control-label"><input type="file" name="cv"></label>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Foto</label>
					<label  class="col-sm-2 control-label"><input type="file" name="foto"></label>					
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Ijazah Terakhir</label>
					<label  class="col-sm-2 control-label"><input type="file" name="scanijazah"></label>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">KTP</label>
					<label  class="col-sm-2 control-label"><input type="file" name="scanktp"></label>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Pengalaman Kerja</label>
					<div class="col-sm-8">
					<textarea name="pengalaman" class="form-control" rows="5" value="<?php echo $this->session->userdata('pengalaman');?>"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Keterangan</label>
					<div class="col-sm-8">
					<textarea name="keterangan" class="form-control" rows="5" value="<?php echo $this->session->userdata('keterangan');?>"></textarea>
					</div>
				</div>
				<hr><div class="center gap" align="center">
                      <button type="submit" class="btn btn-theme btn-lg">Update</button>
                </div> 
				</form>
            </div>
        </div>
    </section>