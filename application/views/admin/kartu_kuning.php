<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<table width="894" height="232" border="1">
  <tr>
    <td width="124" height="151"><img src="<?php echo base_url();?>upload/malang hitam putih.jpg.jpeg" width="124" height="147" /></td>
    <td width="754"><p align="right">NO.Registrasi : <?php echo $pencaker->id_pencaker?></p><p align="center">PEMERINTAH KABUPATEN MALANG</p>
    <p align="center">DINAS TENAGA KERJA KABUPATEN MALANG</p></td>
  </tr>
  <tr>
    <td>NO. PENCAKER</td>
    <td>&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>upload/kotak.jpg"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>upload/kotak1.jpg"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="5px" align="center"><?php $thn = date('Y'); echo $thn?></font></td>
  </tr>
  <tr>
    <td>NIK</td>
    <td>: <?php echo $pencaker->no_ktp ;?></td>
  </tr>
  <tr>
    <td width="124" height="151"><img src="<?php echo base_url();?>upload/foto_pencaker/<?php echo $pencaker->foto; ?>" width="124" height="147" /></td>
    <td><p>Keterangan :</p>
      <p>1. Kartu ini berlaku untuk melamar pekerjaan.</p>
      <p>2. Bila ada perubahan data/keterangan lainnya atau telah mendapat pekerjaan harap segera melapor.</p>
      <p>3. Apabila pencari kerja yang bersangkutan telah diterima bekerja maka instansi/Perusahaan yang menerima agar mengembalikan AK/I ini kepada Dinas Tenaga Kerja &amp; Transmigrasi Kab. Malang.</p>
      <p>4. Kartu berlaku 2 tahun dengan keharusan melapor setiap 6 bulan sekali terhitung sejak tanggal pendaftaran.</p>
      <p>5. Apabila kemudian hari terbukti memberikan keterangan yang tidak benar dan atau memiliki lebih dari satu kartu AK/I maka akan dituntut sesuai dengan peraturan perundang-undangan yang berlaku.</p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><table width="785" border="1">
      <tr>
        <td width="222" height="5" valign="middle"><div align="center">Laporan</div></td>
        <td width="253" valign="middle"><div align="center">Tgl - Bln - Thn </div></td>
        <td width="371" valign="middle"><p align="center">Ttd. Petugas</p>
          <p align="center">(Cantumkan NIP)</p></td>
      </tr>
      <tr>
        <td>Pertama </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Kedua</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Ketiga</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Keempat</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>      
      <table width="785" border="1">
        <tr>
          <td width="215">Diterima di</td>
          <td width="620">:</td>
        </tr>
        <tr>
          <td>T.M.T.</td>
          <td>:</td>
        </tr>
      </table>
      <table width="785" border="1">
          <tr>
            <td width="165">NAMA</td>
            <td width="620">: <?php echo $pencaker->nama_lengkap ;?></td>
          </tr>
          <tr>
            <td width="215">TEMPAT TANGGAL LAHIR</td>
            <td width="620">: <?php echo $pencaker->tempat_lahir;?>, <?php echo $pencaker->tanggal_lahir;?></td>
          </tr>
          <tr>
            <td width="165">JENIS KELAMIN</td>
            <td width="620">: <?php echo $pencaker->jk ;?></td>
          </tr>
          <tr>
            <td width="165">STATUS</td>
            <td width="620">: <?php echo $pencaker->status ;?></td>
          </tr>
          <tr>
            <td width="165">AGAMA</td>
            <td width="620">: <?php echo $pencaker->agama ;?></td>
          </tr>
          <tr>
            <td width="165">ALAMAT</td>
            <td width="620">: <?php echo $pencaker->alamat ;?></td>
          </tr>
      </table>
      <table width="785" border="1">
        <tr>PENDIDIKAN FORMAL/NONFORMAL</tr>
        <tr>
          <td width="350">TIDAK TAMAT SD/SAMPAI KELAS</td>
          <td width="620">: <?php echo $pencaker->kls;?>&nbsp;&nbsp;&nbsp;Tahun&nbsp;&nbsp;&nbsp;<?php echo $pencaker->thn;?></td>
        </tr>
        <tr>
          <td width="195">SD/SEDERAJAT</td>
        <td width="620">: SD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tahun&nbsp;&nbsp;&nbsp;<?php echo $pencaker->sd;?></td>
        </tr>
        <tr>
          <td width="195">SMTP/SEDERAJAT</td>
          <td width="620">: SMP&nbsp;&nbsp;&nbsp;Tahun&nbsp;&nbsp;&nbsp;<?php echo $pencaker->smp;?></td>
        </tr>
        <tr>
          <td width="195">SMTA/DI/AKTA I</td>
          <td width="620">: SMA&nbsp;&nbsp;Tahun&nbsp;&nbsp;&nbsp;<?php echo $pencaker->sma;?></td>
        </tr>
        <tr>
          <td width="195">SM/DII/DIII</td>
          <td width="620">: D III&nbsp;&nbsp;&nbsp;Tahun&nbsp;&nbsp;&nbsp;<?php echo $pencaker->d_ii;?></td>
        </tr>
        <tr>
          <td width="195">AKTA II</td>
          <td width="620">: AKTA II&nbsp;&nbsp;&nbsp;Tahun&nbsp;&nbsp;&nbsp;<?php echo $pencaker->akta_ii;?></td>
        </tr>
        <tr>
          <td width="195">AKTA III</td>
          <td width="620">: AKTA III&nbsp;&nbsp;Tahun&nbsp;&nbsp;&nbsp;<?php echo $pencaker->akta_iii;?></td>
        </tr>
        <tr>
          <td width="195">S/PASCA/SI/AKTA IV/D IV</td>
          <td width="620">: SI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tahun&nbsp;&nbsp;&nbsp;<?php echo $pencaker->akta_iv;?></td>
        </tr>
        <tr>
          <td width="195">DOKTOR/SII/AKTA V</td>
          <td width="620">: AKTA V&nbsp;&nbsp;&nbsp;Tahun&nbsp;&nbsp;&nbsp;<?php echo $pencaker->akta_v;?></td>
        </tr>
      </table>
      <table>
      <tr>
          <td width="620">1. : ...................................................................&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;An.CAMAT&nbsp;&nbsp;<?php echo $this->session->userdata('nama_admin');?></td>
        </tr>
        <tr>
          <td width="620">2. : ...................................................................&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEKCAM</td>
        </tr>
        <tr>
          <td width="620">3. : ...................................................................</td>
        </tr>
        <tr>
          <td width="620">&nbsp;</td>
        </tr>
        <tr>
          <td width="620">&nbsp;</td>
        </tr>
        <tr>
          <td width="620">&nbsp;</td>
        </tr>
         <tr>
          <td width="620">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...........................</td>
        </tr>
        <tr>
          <td width="620">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pembina</td>
        </tr>
        <tr>
          <td width="620">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP. </td>
        </tr>
        <tr>
          <td width="620">&nbsp;</td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<a href="#" onclick=window.print();return false  ><span class="glyphicon glyphicon-home"></span>Simpan</a>
</body>
</html>