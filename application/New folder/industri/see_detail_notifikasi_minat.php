<section id="contact-page" class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="center gap">
                      <h4 style="font-weight: normal;font-size: 16px;border: solid 1px #ff0000;padding: 10px;border-radius: 10px;background: rgba(255, 5, 40, 0.07);">Minat Anda Pada Pencari Kerja Dengan Nama <b><?php echo $pencaker->nama_lengkap; ?></b>, Telah Dikonfirmasi Disnaker. Untuk Lebih Lanjut Silahkan Hubungi Pencari Kerja Tersebut</h4>
                    </div>               
                </div>
            </div>
        <div class="row">
                <div class="col-md-12">
                    <table class="table">                                
                                
                                <tr>
                                    <td rowspan="50" width="20%"><img width="250" height="250"src="<?php echo base_url();?>upload/foto_pencaker/<?php echo $pencaker->foto;?>"</td>
                                    
                                </tr>
                                <tr>
                                    <td>Nama : <?php echo $pencaker->nama_lengkap; ?></td>
                                </tr>                                
                                <tr>

                                    <td>Gender : <?php echo $pencaker->jk; ?></td>
                                </tr>
                                <tr>
                                    <td>Agama : <?php echo $pencaker->agama; ?></td>
                                </tr>
                                <tr>
                                    <td>Status : <?php echo $pencaker->status; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>TTL : <?php echo $pencaker->tanggal_lahir; ?>, <?php echo $pencaker->tempat_lahir; ?></td>
                                </tr>

                                <tr>
                                    <td>No Telepon : <?php echo $pencaker->telepon; ?></td>
                                </tr>

                                <tr>
                                    <td>Email : <?php echo $pencaker->email; ?></td>
                                </tr>
                                <tr>
                                    <td>Alamat : <?php echo $pencaker->alamat; ?></td>
                                </tr>

                                <tr>
                                    <td>Tanggal Terdaftar : <?php echo $pencaker->tanggal; ?></td>
                                </tr>
                               <tr>
                                    <td>Kecamatan : <?php echo $pencaker->nama_kecamatan; ?></td>
                                </tr>
                                <tr>
                                    <td>Pendidikan : <?php echo $pencaker->nama_pendidikan; ?></td>
                                </tr>
                                <tr>
                                    <td>Pengalaman : <?php echo $pencaker->pengalaman; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Usia : <?php echo getOld($pencaker->tanggal_lahir); ?> Tahun</td>
                                </tr>
                                <tr>
                                    <td>Bidang Keahlian : <?php echo $pencaker->bidang_keahlian; ?></td>
                                </tr>
                                <tr>
                                    <td>Posisi : <?php echo $pencaker->nama_posisi; ?></td>
                                </tr>
                                <tr>
                                    <td>Gaji : <?php echo $pencaker->gaji; ?></td>
                                </tr>

                                <tr>
                                    <td>Keterangan : <?php echo $pencaker->keterangan; ?></td>
                                </tr>
                                 <tr>
                                    <td>CV : <a href="<?php echo base_url();?>upload/cv/<?php echo $pencaker->cv;?>"><?php echo $pencaker->cv;?></a></td>
                                </tr>
                                    <td>
                                        <a href="<?php echo site_url("industri/see_notifikasi")?>"><span class="glyphicon glyphicon-home"></span>Back</a>
                                    </td>
                                </tr>
                                
                            </table>            
                </div>
            </div>
    </section>