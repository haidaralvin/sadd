<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin extends CI_Model {  
        
        private $table = "admin";

        function get_data_admin($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table}")->row()->total_rows;
                $data['query'] = $this->db->limit($limit, $offset)->get($this->table);
                return $data;
        }
        
        function get_data_admin_by_id($data){
                $this->db->where($data);
                $q=$this->db->get($this->table);
                
                $data=$q->first_row();
                return $data;
        }
        function add_admin($data){
                $q=$this->db->insert($this->table,$data);
                return $q;
        }

        function edit_admin($data){
                $this->db->where('id_admin',$data['id_admin']);
                $q=$this->db->update($this->table,$data);
                return $q;
        }
        
        function delete_admin($data)
        {
                $q=$this->db->delete($this->table,$data);
                return $q;
        }

}?>