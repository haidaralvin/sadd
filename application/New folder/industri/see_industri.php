<section id="contact-page" class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="center gap">
                      <h2><?php echo $this->session->userdata('nama_perusahaan');?></h2>
					</div>               
                </div>
            </div>
		<div class="row">
                <div class="col-md-12">
					<table class="table table-striped">    
                                <tr>
                                        <td rowspan="10"  width="10%"><img width="200" height="200"src="<?php echo base_url();?>upload/foto_industri/<?php echo $this->session->userdata('foto_industri');?>"</td>
                                        
                                </tr>
                                <tr>
                                    <td>Nama Pimpinan : <?php echo $this->session->userdata('nama_pemimpin');?></td>
                                </tr>
                                <tr>
                                    <td>Tahun Berdiri : <?php echo $this->session->userdata('tahun_berdiri');?></td>
                                </tr>
                                <tr>
                                     <td>Email : <?php echo $this->session->userdata('email');?></td>
                                </tr>
                                <tr>
                                    <td>Telepon : <?php echo $this->session->userdata('telepon');?></td>
                                </tr>
								<tr>
                                    <td>Alamat : <?php echo $this->session->userdata('alamat');?></td>
                                </tr>
                                <tr>
                                    <td>Kecamatan : <?php echo $kecamatan->nama_kecamatan?> </td>
                                </tr>
                                <tr>
                                    <td>Kategori : <?php echo $sub_kategori->nama_kategori?> </td>
                                </tr>
                              
                                
                                <tr>
                                    <td colspan="2">
                                         <?php echo $this->session->userdata('keterangan');?>
                                    </td>
                                </tr>
                            </table>        
                </div>
            </div>
		</div>
		
    </section>