                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Admin Info
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                <p class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> Username: <?php echo $this->session->userdata('username');?>
                                </p>
                                <p class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> Nama: <?php echo $this->session->userdata('nama_admin');?>
                                    
                                </p>
                                <?php if($this->session->userdata('level') == "kecamatan"){?>
                                <?php foreach ($kecamatan as $row): ?>
                                    <?php if($row->id_kecamatan == $this->session->userdata('id_kecamatan')){?>
                                                                        
                                <p class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> Kecamatan: <?php echo $row->nama_kecamatan;?>
                                    
                                </p>

                                <?php } endforeach;
                                } else { ?>
                                <p class="list-group-item">
                                    <i class="fa fa-comment fa-fw"></i> Kecamatan: -
                                    
                                </p>
                                <?php } ?>
                            </div>
                            <!-- /.list-group -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url();?>styles_admin/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>styles_admin/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>styles_admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="<?php echo base_url();?>styles_admin/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url();?>styles_admin/js/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?php echo base_url();?>styles_admin/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="<?php echo base_url();?>styles_admin/js/demo/dashboard-demo.js"></script>

</body>

</html>