<section id="contact-page" class="container">
<?php
// buat variable temporary id_industri_tampil supaya industri yang sama hanya tampil 1 kali.
$industri_displayed=array();                            
                                // Memastikan jika data tidak kosong
                               if(count($industri)>0){
                                foreach ($industri as $row) {
                                if(!in_array($row->id_industri, $industri_displayed))
                                {
                                    $industri_displayed[]=$row->id_industri;
                                    ?>
		<div class="row">
                <div class="col-md-9">
					<table class="table">    
                                
                                <tr>
                                    <td rowspan="6"  width="20%"><img width="200" height="200"src="<?php echo base_url();?>upload/foto_industri/<?php echo $row->foto_industri;?>"></td>
                                        
                                </tr>
                                 <tr>
                                    <td>Nama : <a href="<?php echo site_url('pencaker/detail_industri?id_industri='.$row->id_industri)?>"><?php echo $row->nama_perusahaan; ?></a></td>
                                </tr>
                                <tr>
                                    <td>Tahun Berdiri : <?php echo $row->tahun_berdiri;?></td>
                                </tr>
								<tr>
                                    <td>Alamat : <?php echo $row->alamat;?></td>
                                </tr>
                                
                                <tr>
                                    <td>Bidang Usaha : <?php echo $row->nama_sub;?> </td>
                                    
                                </tr>
                               <tr>
                                    <td>
                                        <a href="<?php echo site_url('pencaker/detail_industri?id_industri='.$row->id_industri)?>"><span class="glyphicon glyphicon-zoom-in"></span>Detail</a>
                                    </td>
                                </tr>
                                  
                                <?php 
                                }

                                } } // jika data masih kosong, tampilkan pesan
                                else {
                                    echo "<tr><td align='center'>Data masih kosong !</td></tr>";
                                }
                                ?>
                                
                                
                            </table>   
<!--                             <div class="btn-toolbar" role="toolbar"><div class="btn-group"><?php echo $this->pagination->create_links(); ?></div></div>  
                              -->                             
                        </div>
                    </div>
    </section>