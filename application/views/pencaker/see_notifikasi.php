    <section id="contact-page" class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Notifikasi</h3>
                    </div>
                    <ul class="nav nav-tabs nav-right" role="tablist">
                          <li class="active"><a href="#posisi" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-book"></span> Minat 
<?php if($jumlah_minat->jumlah>0){ ?>
<b class="badge" style="background-color: #CC3300"><?php echo $jumlah_minat->jumlah; ?> </b>
<?php }  ?>
</a></li>

                          <li><a href="#lokasi" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-home"></span>  Lamar 
<?php if($jumlah_lamar->jumlah>0){ ?>
 <b class="badge" style="background-color: #CC3300"><?php echo $jumlah_lamar->jumlah; ?></b>
<?php } ?>
</a></li>
                          
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="posisi">
                            
                            <div class="row">

                                        <div  class="col-md-12">
                                        
                                        <table align="center" class="table">    
                            
                                <?php if(empty($minat)) echo "<center>Tidak Ada Notifikasi</center>";
                                else{ ?>

                                <tr class="success">
                                    <td width="40px">No </td>
                                    <td width="">Isi </td>
                                    <td width="160px">Tanggal </td>
                                    <td width="100px">Aksi</td>

                                </tr>
                               <?php

                               $no=1;
                                
                                foreach($minat as $row)
                            { ?>
                                <tr>                                  
                                    <?php if ($row->is_read_by_pencaker == 1) {?>
                                    <td> <?php echo $no; ?> </td>
                                    <td>                                 
                                    Perusahaan <b><?php echo $row->nama_perusahaan; ?></b> berminat kepada anda.
                                    Selanjutnya, silahkan tunggu perusahaan tersebut menghubungi anda.
                                    </td>
                                    <?php } else {?>
                                     <td><font color="#CC3300"> <?php echo $no; ?> </font></td>
                                    <td><font color="#CC3300">
                                    Perusahaan <b><?php echo $row->nama_perusahaan; ?></b> berminat kepada anda.
                                    Selanjutnya, silahkan tunggu perusahaan tersebut menghubungi anda.
                                    </font> </td>
                                    <?php } ?>
                                     <td> <?php echo $row->tanggal_minat; ?> </td>
                                    <td><a href="<?php echo site_url('pencaker/detail_industri_minat?id_minat='.$row->id_minat)?>"><span class="glyphicon glyphicon-zoom-in"></span> Detail</a></td>
                                </tr>
                                                            
                            <?php $no++;}  }?>
                               
                            </table>      
                                        
                                        </div>
                                   

                                </div>
                            
                        </div>
                        <div class="tab-pane fade" id="lokasi">
                            <div class="row">
 
                                        <div  class="col-md-12">
                                        
                                        <table align="center" class="table">    
                            
                                <?php if(empty($lamar)) echo "<center>Tidak Ada Notifikasi</center>";
                                else{ ?>

                                <tr class="success">
                                    <td width="40px">No </td>
                                    <td width="">Isi </td>
                                    <td width="100px">Tanggal </td>
                                    <td width="100px">Aksi</td>

                                </tr>
                               <?php

                               $no= 1;
                                
                                foreach($lamar as $row)
                            { ?>
                                <tr>
                                <?php if ($row->is_read_by_pencaker == 1) {?>
                                    <td> <?php echo $no; ?> </td>
                                    <td>
                                         Lamaran Anda Untuk Lowongan <b><i><?php echo $row->nama_lowongan; ?></i></b> di perusahaan <b><?php echo $row->nama_perusahaan; ?></b> telah disampaikan atau sudah diteruskan ke perusahaan. 

                                     </td>
                                    <?php } else {?>
                                     <td><font color="#CC3300"> <?php echo $no; ?> </font></td>
                                    <td><font color="#CC3300">
                                        
                                        Lamaran Anda Untuk Lowongan <b><i><?php echo $row->nama_lowongan; ?></i></b> di perusahaan <b><?php echo $row->nama_perusahaan; ?></b> telah disampaikan atau sudah diteruskan ke perusahaan. 

                                    </font></td>
                                    <?php } ?>
                                     <td> <?php echo $row->tanggal_lamar; ?> </td>

                                    <td><a href="<?php echo site_url('pencaker/detail_industri_lamar?id_lamar='.$row->id_lamar)?>"><span class="glyphicon glyphicon-zoom-in"></span> Detail</a></td>
                                </tr>
                                
                             
                            <?php $no++; }  }?>
                               
                            </table>      
                                        
                                        </div>                                   
                                </div>
                            
                        </div>
                       
                </div>
            </div>
        </div>
        </div>
    </section>    

