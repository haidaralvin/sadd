<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kecamatan extends CI_Model {  
        private $table = "kecamatan";

        function get_data_kecamatan(){
                $this->db->where('id_kecamatan != 0');
                $q=$this->db->get('kecamatan');

                $data=$q->result();
                return $data;
        }
        function get_data_kecamatan_paging($limit, $offset = 0){
                $data['total_rows'] = $this->db->query("SELECT COUNT(*) total_rows FROM {$this->table} WHERE id_kecamatan!= 0 ")->row()->total_rows;
                $data['query'] = $this->db->get_where('kecamatan', array('kecamatan.id_kecamatan !='=>0), $limit, $offset);
                return $data;
        }
        function get_data_kecamatan_by_id($data){
                $this->db->where($data);
                $q=$this->db->get($this->table);
                
                $data=$q->first_row();
                return $data;
        }
        function add_kecamatan($data){
                $q=$this->db->insert($this->table,$data);
                return $q;
        }

        function edit_kecamatan($data){
                $this->db->where('id_kecamatan',$data['id_kecamatan']);
                $q=$this->db->update($this->table,$data);
                return $q;
        }
        
        function delete_kecamatan($data)
        {
                $q=$this->db->delete($this->table,$data);
                return $q;
        }

        function ambil_kecamatan($id_kec)
                {
                   $this->db->select('*');
                   $this->db->where('id_kecamatan', $id_kec);
                   $query = $this->db->get('kecamatan');
                   
                   return $query->row();
                }

}?>