<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Perhitungan CBR PENCAKER</h1>
            <p>ini posisinya andre mencari kerja di daerah lawang, kriterianya aku ambil hanya membandingkan kecamatan saja, yang lain tinggal tambah di kriteria saja</p>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php
                        //data pencari kerja (id = 30 Andre) bisa dirubah sendiri
                        $id = 30;
                        $pencaker = $this->db->query("SELECT c.nama_lengkap,c.pendidikan_terakhir, c.pengalaman,k.nama_kecamatan
                                        FROM pencaker c
                                        INNER JOIN kecamatan k ON c.id_kecamatan = k.id_kecamatan
                                        WHERE c.id_pencaker = $id")->row();

                        //data lowongan kerja beserta perusahaan, kecamatan dan posisi
                        $lowongan = $this->db->query("SELECT l.id_lowongan, i.nama_perusahaan, l.nama_lowongan, p.nama_posisi, k.nama_kecamatan
                            FROM lowongan l
                            INNER JOIN posisi p ON l.id_posisi = p.id_posisi
                            INNER JOIN kecamatan k ON l.id_kecamatan = k.id_kecamatan
                            INNER JOIN industri i ON l.id_industri = i.id_industri
                            order by l.id_lowongan ASC")->result();

                        $kriteria = array("nama_kecamatan"); //tambahkan kriteria aku ambil yg kecamatan aja.

                        //Delete Nilai digunakan jika ada perubahan data
                        $this->db->delete('hasil_perhitungan', array('id_pencaker' => $id)); 

                        foreach ($lowongan as $data) {
                                $benar = array();
                                $salah = array();

                                for ($i=0; $i < count($kriteria) ; $i++) { 
                                    
                                    if ($data->$kriteria[$i] == $pencaker->$kriteria[$i]) {
                                        $benar[$kriteria[$i]] = 1;
                                    }else{
                                        $salah[$kriteria[$i]] = 0;
                                    }
                                }

                                $mirip   = count($benar);
                                $beda    = count($salah);

                            //PROSES RUMUS SIMILARITY MEASURE
                                $proses = round($mirip/($mirip+$beda),2);
                                echo "<h3>".$data->id_lowongan.". HASIL PROSES = ".$mirip."/(".$mirip." + ".$beda.") = ".$proses."</h3>";

                            //Insert Nilai
                                $hasil = array(
                                    'id_pencaker' => $id ,
                                    'id_lowongan' => $data->id_lowongan ,
                                    'hasil' => $proses
                                );

                                $this->db->insert('hasil_perhitungan', $hasil); 

                                //break;
                            }



                        //echo "<h1>DATA LOWONGAN</h1>";
                        //echo "<pre>";print_r($lowongan);


                        $hasil_lowongan = $this->db->query("SELECT i.nama_perusahaan, k.nama_kecamatan, p.nama_posisi, COUNT(l.id_lowongan)
                                        FROM hasil_perhitungan h
                                        INNER JOIN lowongan l ON l.id_lowongan = h.id_lowongan
                                        INNER JOIN industri i on l.id_industri = i.id_industri
                                        INNER JOIN kecamatan k ON k.id_kecamatan = l.id_kecamatan
                                        INNER JOIN posisi p ON p.id_posisi = l.id_posisi
                                        WHERE h.hasil = (SELECT MAX(hasil) FROM hasil_perhitungan WHERE id_pencaker = $id)
                                        GROUP BY l.id_lowongan")->result();

                            echo "<pre>";print_r($hasil_lowongan);
                    ?>
                      
                </div>
                <!-- /.panel-body -->
            </div>
        </div>


        <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Perhitungan CBR Perusahaan</h1>
            <p>ini kondisi PT. NEW MINATEX mencari pekerja dengan kriteria yg berkecamatan Lawang, karena kriterianya cuma satu. tinggal menyesuaikan saja field sama kriterianya</p>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <?php
                        $id_perusahaan = 2;

                        $industri = $this->db->query("SELECT i.nama_perusahaan, k.nama_kecamatan 
                                        FROM industri i
                                        INNER JOIN kecamatan k ON i.id_kecamatan = k.id_kecamatan
                                        WHERE i.id_industri = $id_perusahaan")->row();

                        $pencari = $this->db->query("SELECT p.id_pencaker, p.nama_lengkap, k.nama_kecamatan
                                    FROM pencaker p
                                    INNER JOIN kecamatan k ON k.id_kecamatan = p.id_kecamatan")->result();



                        //Delete Nilai digunakan jika ada perubahan data
                        $this->db->delete('hasil_perhitungan', array('id_pencaker' => $id_perusahaan)); 

                        foreach ($pencari as $date) {
                                $benar1 = array();
                                $salah1 = array();

                                for ($y=0; $y < count($kriteria) ; $y++) { 
                                    
                                    if ($date->$kriteria[$y] == $industri->$kriteria[$y]) {
                                        $benar1[$kriteria[$y]] = 1;
                                    }else{
                                        $salah1[$kriteria[$y]] = 0;
                                    }
                                }

                                $mirip1   = count($benar1);
                                $beda1   = count($salah1);


                            //PROSES RUMUS SIMILARITY MEASURE
                                $proses1 = round($mirip1/($mirip1+$beda1),2);
                                echo "<h3>".$date->id_pencaker.". HASIL PROSES = ".$mirip1."/(".$mirip1." + ".$beda1.") = ".$proses1."</h3>";

                            //Insert Nilai
                                $hasil1 = array(
                                    'id_pencaker' => $id_perusahaan ,
                                    'id_lowongan' => $date->id_pencaker ,
                                    'hasil' => $proses1
                                );

                                $this->db->insert('hasil_perhitungan', $hasil1); 

                                //break;
                            }



                        //echo "<h1>DATA LOWONGAN</h1>";
                        //echo "<pre>";print_r($lowongan);


                        $data_pelamar = $this->db->query("SELECT p.nama_lengkap, k.nama_kecamatan
                                        FROM hasil_perhitungan h
                                        INNER JOIN pencaker p ON p.id_pencaker = h.id_lowongan
                                        INNER JOIN kecamatan k ON k.id_kecamatan = p.id_kecamatan
                                        WHERE h.hasil = (SELECT MAX(hasil) FROM hasil_perhitungan WHERE id_pencaker = $id_perusahaan)")->result();

                            echo "<pre>";print_r($data_pelamar);
                    ?>
                      
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
		

