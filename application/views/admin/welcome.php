        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Selamat Datang</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="col-sm-8">
                                <p>Anda masuk sebagai <?php if ($this->session->userdata('level')=='superadmin'){?>Super Admin<?php } elseif ($this->session->userdata('level')=='kecamatan'){?>Admin Kecamatan<?php } elseif ($this->session->userdata('level')=='disnaker'){?>Admin Disnaker<?php } elseif ($this->session->userdata('level')=='disperindag'){?>Admin Disperindag<?php }?>.</p>
                                <button type="button" onclick="window.location.href='<?php echo base_url();?>link/logout'" class="btn btn-primary">Logout</button>
                            </div>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
