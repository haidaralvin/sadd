        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Lowongan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table class="table" align="center">
                                
                                <?php $no = $offset;
                                // Memastikan jika data tidak kosong
                                if ($data['query']->num_rows() > 0) {
                                foreach ($data['query']->result() as $row) {?>
                                <tr>
                                    <td>Judul : <?php echo $row->nama_lowongan; ?></td>
                                </tr>
                                <tr>
                                    <td>Posisi : <?php echo $row->nama_posisi;?> </td>
                                   
                                </tr>
                                <tr>
                                   
                                    <td>Kecamatan : <?php echo $row->nama_kecamatan;?> </td>
                                    
                                </tr>
                                <tr>
                                    <td>Deadline : <?php echo $row->deadline; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url('link/see_detail_lowongan?id_lowongan='.$row->id_lowongan)?>"><span class="glyphicon glyphicon-zoom-in"></span>Detail</a>
                                        <a href="<?php echo site_url('link/delete_lowongan?id_lowongan='.$row->id_lowongan)?>"onclick="return confirm('Yakin ingin menghapus data ini?')"><span class="glyphicon glyphicon-trash"></span>Delete</a>
                                    </td>
                                </tr>
                                  
                                <?php } } // jika data masih kosong, tampilkan pesan
                                else {
                                    echo "<tr><td align='center'>Data masih kosong !</td></tr>";
                                }
                                ?>
                                
                                
                            </table>   
                            <div class="btn-toolbar" role="toolbar"><div class="btn-group"><?php echo $this->pagination->create_links(); ?></div></div>  
                                
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>