<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
	}
	private $limit = 10;
 	public function index()
	{
		$data['industri']=$this->m_industri->get_home_industri();
		$this->load->view('header');
		$this->load->view('home',$data);
		$this->load->view('footer');
        
	}

	public function bursa_kerja($offset=0)
    {    
        $data['offset'] = $offset;
        
        $data["data"] = $this->m_lowongan->get_data_lowongan($this->limit, $offset);
        $config["base_url"] = site_url("home/bursa_kerja");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);

        $this->load->view('header');
        $this->load->view('home_bursa_kerja', $data);
        $this->load->view('footer');
    }

    public function industri($offset=0)
    {    
        $data['offset'] = $offset;
        $data["data"] = $this->m_industri->get_data_industri_disperindag($this->limit, $offset);
        $config["base_url"] = site_url("home/industri");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);

        $this->load->view('header');
        $this->load->view('home_bursa_industri', $data);
        $this->load->view('footer');
    }

    public function pencaker_confirm($offset=0){
        $data['offset'] = $offset;
        
        $data["data"] = $this->m_pencaker->get_data_pencaker_confirmed($this->limit, $offset);
        $config["base_url"] = site_url("home/pencaker_confirm");
        $config["total_rows"] = $data["data"]["total_rows"];
        $config["per_page"] = $this->limit;
        $this->pagination->initialize($config);

        $this->load->view('header');
        $this->load->view('pencaker', $data);
        $this->load->view('footer');
    }

}?>