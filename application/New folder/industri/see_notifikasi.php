    <section id="contact-page" class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Notifikasi</h3>
                    </div>
                    <ul class="nav nav-tabs nav-right" role="tablist">
                          <li class="active"><a href="#posisi" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-book"></span> Minat 
<?php if($jumlah_minat->jumlah>0){ ?>
<b class="badge" style="background-color: #CC3300"><?php echo $jumlah_minat->jumlah; ?> </b>
<?php }  ?>
</a></li>

                          <li><a href="#lokasi" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-home"></span>  Lamar 
<?php if($jumlah_lamar->jumlah>0){ ?>
 <b class="badge" style="background-color: #CC3300"><?php echo $jumlah_lamar->jumlah; ?></b>
<?php } ?>
</a></li>
                          
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="posisi">
                            
                            <div class="row">

                            <div class="row">
                            <br>
                            </div>
                               <ul class="list-group">

                                    
                                        <div  class="col-md-12">
                                        
                                        <table align="center" class="table">    
                            
                                <?php if(empty($minat)) echo "<center>Tidak Ada Notifikasi</center>";
                                else{ ?>

                                <tr class="success">
                                    <td>No </td>
                                    <td>Isi </td>
                                    <td>Aksi</td>

                                </tr>
                               <?php

                               $no=1;
                                
                                foreach($minat as $row)
                            { ?>
                                <tr>                                  
                                    <?php if ($row->is_read == 1) {?>
                                    <td> <?php echo $no; ?> </td>
                                    <td>Pencaker <?php echo $row->nama_lengkap; ?>, Yang Anda Minati. Telah Dikonfirmasi Disnaker</td>
                                    <?php } else {?>
                                     <td><font color="#CC3300"> <?php echo $no; ?> </font></td>
                                    <td><font color="#CC3300">Pencaker <?php echo $row->nama_lengkap; ?>, Yang Anda Minati. Telah Dikonfirmasi Disnaker</font> </td>
                                    <?php } ?>
                                    <td><a href="<?php echo site_url('industri/detail_pencaker_minat?id_minat='.$row->id_minat)?>"><span class="glyphicon glyphicon-zoom-in"></span>Detail</a></td>
                                </tr>
                                                            
                            <?php $no++;}  }?>
                               
                            </table>      
                                        
                                        </div>
                                                                            
                                </ul>
                                                              
                                </div>
                            
                        </div>
                        <div class="tab-pane fade" id="lokasi">
                            <div class="row">

                            <div class="row">
                            <br>
                            </div>
                               <ul class="list-group">
                                    
                                        <div  class="col-md-12">
                                        
                                        <table align="center" class="table">    
                            
                                <?php if(empty($lamar)) echo "<center>Tidak Ada Notifikasi</center>";
                                else{ ?>

                                <tr class="success">
                                    <td>No </td>
                                    <td>Isi </td>
                                    <td>Aksi</td>

                                </tr>
                               <?php

                               $no= 1;
                                
                                foreach($lamar as $row)
                            { ?>
                                <tr>
                                <?php if ($row->is_read == 1) {?>
                                    <td> <?php echo $no; ?> </td>
                                    <td>Pelamar dengan nama <?php echo $row->nama_lengkap; ?> ingin melamar di perusahaan anda, pada lowongan <?php echo $row->nama_lowongan; ?></td>
                                    <?php } else {?>
                                     <td><font color="#CC3300"> <?php echo $no; ?> </font></td>
                                    <td><font color="#CC3300">Lamaran <?php echo $row->nama_lengkap; ?> dengan nama lamaran <?php echo $row->nama_lowongan; ?></font></td>
                                    <?php } ?>
 
                                    <td><a href="<?php echo site_url('industri/detail_pencaker_lamar?id_lamar='.$row->id_lamar)?>"><span class="glyphicon glyphicon-zoom-in"></span>Detail</a></td>
                                </tr>
                                
                             
                            <?php $no++; }  }?>
                               
                            </table>      
                                        
                                        </div>                                   
                                </ul>
                                </div>
                            
                        </div>
                       
                </div>
            </div>
        </div>
        </div>
    </section>