    <section id="contact-page" class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Notifikasi</h3>
                    </div>
                    <ul class="nav nav-tabs nav-right" role="tablist">
                          <li class="active"><a href="#posisi" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-book"></span> Minat 
<?php if($jumlah_minat->jumlah>0){ ?>
<b class="badge" style="background-color: #CC3300"><?php echo $jumlah_minat->jumlah; ?> </b>
<?php }  ?>
</a></li>

                          <li><a href="#lokasi" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-home"></span>  Lamar 
<?php if($jumlah_lamar->jumlah>0){ ?>
 <b class="badge" style="background-color: #CC3300"><?php echo $jumlah_lamar->jumlah; ?></b>
<?php } ?>
</a></li>
                          
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="posisi">
                            
                            <div class="row">

                                        <div  class="col-md-12">
                                        
                                        <table align="center" class="table">    
                            
                                <?php if(empty($minat)) echo "<center>Tidak Ada Notifikasi</center>";
                                else{ ?>

                                <tr class="success">
                                    <td width="50">No </td>
                                    <td>Isi </td>
                                    <td width="180">Tanggal</td>
                                    <td width="100">Aksi</td>

                                </tr>
                               <?php

                               $no=1;
                                
                                foreach($minat as $row)
                            { ?>
                                <tr>                                  
                                    <?php if ($row->is_read_by_industri == 1) {?>
                                    <td> <?php echo $no; ?> </td>
                                    <td>
                                    Minat Anda ke Pencari Kerja <?php echo $row->nama_lengkap; ?> Telah Disampaikan.
                                    
                                    </td>
                                    <?php } else {?>
                                     <td><font color="#CC3300"> <?php echo $no; ?> </font></td>
                                    <td><font color="#CC3300">
                                    Minat Anda ke Pencari Kerja <?php echo $row->nama_lengkap; ?> Telah Disampaikan.
                                    
                                    </font> </td>
                                    <?php } ?>
                                     <td> <?php echo $row->tanggal_minat; ?> </td>
                                    <td><a href="<?php echo site_url('industri/detail_pencaker_minat?id_minat='.$row->id_minat)?>"><span class="glyphicon glyphicon-zoom-in"></span> Detail</a></td>
                                </tr>
                                                            
                            <?php $no++;}  }?>
                               
                            </table>      
                                        
                                        </div>
                                                              
                                </div>
                            
                        </div>
                        <div class="tab-pane fade" id="lokasi">
                            <div class="row">

                                    
                                        <div  class="col-md-12">
                                        
                                        <table align="center" class="table">    
                            
                                <?php if(empty($lamar)) echo "<center>Tidak Ada Notifikasi</center>";
                                else{ ?>

                                <tr class="success">
                                    <td width="50">No </td>
                                    <td>Isi </td>
                                    <td width="180">Tanggal</td>
                                    <td width="100">Aksi</td>

                                </tr>
                               <?php

                               $no= 1;
                                
                                foreach($lamar as $row)
                            { ?>
                                <tr>
                                <?php if ($row->is_read_by_industri == 1) {?>
                                    <td> <?php echo $no; ?> </td>
                                    <td>
                                Pelamar Baru : <b><?php echo $row->nama_lengkap; ?> </b> Melamar untuk lowongan 
                                <b><i><?php echo $row->nama_lowongan; ?></i></b>
                                    <?php } else {?>
                                     <td><font color="#CC3300"> <?php echo $no; ?> </font></td>
                                    <td><font color="#CC3300">
                                        Pelamar Baru : <b><?php echo $row->nama_lengkap; ?> </b> Melamar untuk lowongan 
                                        <b><i><?php echo $row->nama_lowongan; ?></i></b>
                                    </font></td>
                                    <?php } ?>
                                     <td> <?php echo $row->tanggal_lamar; ?> </td>
                                    <td><a href="<?php echo site_url('industri/detail_pencaker_lamar?id_lamar='.$row->id_lamar)?>"><span class="glyphicon glyphicon-zoom-in"></span> Detail</a></td>
                                </tr>
                                
                             
                            <?php $no++; }  }?>
                               
                            </table>      
                                        
                                        </div>                                   
                                </div>
                            
                        </div>
                       
                </div>
            </div>
        </div>
        </div>
    </section>