    <section>
        <div class="container">
        <font style="color:black;">
                    <?php
                    date_default_timezone_set("Asia/Jakarta");
                    /* script menentukan hari */  
                         $array_hr= array(1=>"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
                         $hr = $array_hr[date('N')];
                    /* script menentukan tanggal */    
                        $tgl= date('j');
                    /* script menentukan bulan */ 
                        $array_bln = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");
                        $bln = $array_bln[date('n')];
                    /* script menentukan tahun */ 
                        $thn = date('Y');
                    /* script perintah keluaran*/ 
                        echo "<strong>".$hr . ", " . $tgl . " " . $bln . " " . $thn . "</strong>";
                     ?><div id="time"></div></font>
            <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-striped" align="center">
                                <tr class="success">
                                    <th>Nama Industri</th>
                                    <th>Kecamatan</th>
                                    <th>Komoditi</th>
                                    <th>Tanggal Terdaftar</th>
                                </tr>
                                <?php $no = $offset;
                                // Memastikan jika data tidak kosong
                                if ($data['query']->num_rows() > 0) {
                                foreach ($data['query']->result() as $row) {?>
                                <tr>
                                    <td><?php echo $row->nama_perusahaan; ?></td>
                                    <td><?php echo $row->nama_kecamatan;?> </td>
                                    <td><?php echo $row->nama_sub;?> </td>
                                    <td><?php echo $row->tanggal;?></td>
                                </tr>
                                  
                                <?php } } // jika data masih kosong, tampilkan pesan
                                else {
                                    echo "<tr><td align='center'>Data masih kosong !</td></tr>";
                                }
                                ?>
                                    
                            </table>   
                            <div class="btn-toolbar" role="toolbar"><div class="btn-group"><?php echo $this->pagination->create_links(); ?></div></div>  
                        </div> 
            </div>
        </div> 
    </section>