    <section id="contact-page" class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="center gap">
                      <h2>List Pencaker</h2>
                    </div>               
                </div>
            </div>
        <div class="row">
                <div class="col-md-12">
                    <table class="table" align="center">                                
                                
                                <tr>
                                    <td rowspan="50" width="10%"><img width="250" height="250"src="<?php echo base_url();?>upload/foto_pencaker/<?php echo $pencaker->foto;?>"></td>
                                    
                                </tr>

                                <tr>                                    
                                    <td>Nama : <?php echo $pencaker->nama_lengkap; ?></td>
                                </tr>                                
                                
                                <tr>

                                    <td>Gender : <?php echo $pencaker->jk; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Agama : <?php echo $pencaker->agama; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Status : <?php echo $pencaker->status; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>TTL : <?php echo $pencaker->tanggal_lahir; ?>, <?php echo $pencaker->tempat_lahir; ?></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Tanggal Terdaftar : <?php echo $pencaker->tanggal; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Kecamatan : <?php echo $pencaker->nama_kecamatan; ?></td>
                                </tr>
                                <tr>
                                    <td>Pendidikan : <?php echo $pencaker->nama_pendidikan; ?></td>
                                </tr>
                                <tr>
                                    <td>Pengalaman : <?php echo $pencaker->pengalaman; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Usia : <?php echo getOld($pencaker->tanggal_lahir); ?> Tahun</td>
                                </tr>
                                <tr>
                                    <td>Bidang Keahlian : <?php echo $pencaker->bidang_keahlian; ?></td>
                                </tr>
                                <tr>
                                    <td>Posisi : <?php echo $pencaker->nama_posisi; ?></td>
                                </tr>
                                <tr>
                                    <td>Gaji : <?php echo $pencaker->gaji; ?></td>
                                </tr>

                                <tr>
                                    <td>Keterangan : <?php echo $pencaker->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td>CV : <a href="<?php echo base_url();?>upload/cv/<?php echo $pencaker->cv;?>"><?php echo $pencaker->cv;?></td>
                                </tr>
                                <tr>
                                    <td>Scan Ijazah : <img width="400" src="<?php echo base_url();?>upload/scanijazah/<?php echo $pencaker->scanijazah;?>"></td>
                                </tr>
                                
                                <tr>
                                    <td>Scan KTP : <img width="400" src="<?php echo base_url();?>upload/scanktp/<?php echo $pencaker->scanktp;?>"></td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url("industri/see_pencaker")?>"><span class="glyphicon glyphicon-home"></span> Back</a>&nbsp;<a href="<?php echo site_url('industri/minat_pencaker?id_pencaker='.$pencaker->id_pencaker)?>"><span class="glyphicon glyphicon-ok"></span> Minat</a>&nbsp;
                                    </td>
                                </tr>
                                
                            </table>            
                </div>
            </div>
        </div>
        </div>
    </section>