        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tambah Kategori</h1>
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>link/pro_add_kategori">
								<div class="form-group">
									
									<div class="col-sm-8">
										<input type="text" class="form-control" required="required" name="nama_kategori" placeholder="Tambah Kategori">
									</div>
								</div>
								<div class="col-sm-2 col-sm-offset-2">
										<button type="submit" class="btn btn-primary btn-lg">Submit</button>
								</div>
							</form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
				