	<section id="contact-page" class="container">
        <div class="row">
                
            </div>
		<div class="row">
            <div class="col-sm-12">
			<hr>
				<div class="center gap">
                      <h3 align="center">Buat Lowongan Perusahaan</h3>
                </div>  
                
				 <form class="form-horizontal" role="form" method="post" <?php echo @$error; ?><?php echo form_open_multipart('industri/pro_add_lowongan');?>


				<div class="form-group">
					<label class="col-sm-2 control-label">Judul</label>
					<div class="col-sm-8">
					<input type="text" required="required" name="nama_lowongan" class="form-control" id="inputEmail3" placeholder="Nama Lowongan">
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Isi</label>
					<div class="col-sm-8">
					<textarea name="isi" class="form-control"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Posisi</label>
					<div class="col-sm-8">
					<select name="posisi" class="form-control">
						<?php foreach ($posisi as $row): ?>
                                            <option value="<?php echo $row->id_posisi ;?>">&nbsp;<?php echo $row->nama_posisi; ?></option>
                                         <?php endforeach;?>
					</select>
					</div>
				</div>
				
				<div class="form-group">
					<label  class="col-sm-2 control-label">Pendidikan Terakhir</label>
					<div class="col-sm-8">
					<select required='required' name="pend_terakhir" class="form-control">
						<?php foreach ($pendidikan_terakhir as $row): ?>
                                            <option value="<?php echo $row->id_pendidikan ;?>">&nbsp;<?php echo $row->nama_pendidikan; ?></option>
                                         <?php endforeach;?>


						<!-- <option>SD/MI</option>
						<option>SMP/MTS</option>
						<option>SMA/SMK/MA</option>
						<option>D1</option>
						<option>D2</option>
						<option>D3</option>
						<option>D4</option>
						<option>S1</option>
						<option>S2</option>
						<option>S3</option> -->
					</select>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Bidang Keahlian</label>
					<div class="col-sm-8">
					<select required='required' name="bid_keahlian" class="form-control">
						<?php foreach ($bidang_keahlian as $row): ?>
                                            <option value="<?php echo $row->id_bidang ;?>">&nbsp;<?php echo $row->bidang_keahlian; ?></option>
                                         <?php endforeach;?>


						<!-- <option>Agribisnis & Argoindustri</option>
						<option>Bisnis & Manajemen</option>
						<option>Digital Media</option>
						<option>Kesehatan</option>
						<option>Marketing</option>
						<option>Pariwisata</option>
						<option>Perikanan & Kelautan</option>
						<option>Seni Pertunjukan</option>
						<option>Seni Rupa & Kriya</option>
						<option>Teknologi & Rekayasa</option>
						<option>Teknologi Informasi & Komunikasi</option> -->
					</select>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Pengalaman</label>
					<div class="col-sm-8">
					<select required='required' name="pengalaman" class="form-control">
						<?php foreach ($pengalaman as $row): ?>
                                            <option value="<?php echo $row->id_pengalaman ;?>">&nbsp;<?php echo $row->pengalaman; ?></option>
                                         <?php endforeach;?>
					</select>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Gaji</label>
					<div class="col-sm-8">
					<select required='required' name="gaji" class="form-control">
						<?php foreach ($gaji as $row): ?>
                                            <option value="<?php echo $row->id_gaji ;?>">&nbsp;<?php echo $row->gaji; ?></option>
                                         <?php endforeach;?>
					</select>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Usia</label>
					<div class="col-sm-8">
					<select required='required' name="usia" class="form-control">
						<?php foreach ($usia as $row): ?>
                                            <option value="<?php echo $row->id_usia ;?>">&nbsp;<?php echo $row->usia; ?></option>
                                         <?php endforeach;?>
					</select>
					</div>
				</div>

				
					<input type="hidden" name="kecamatan" class="form-control" value="<?php echo $this->session->userdata('id_kecamatan');?>" id="inputEmail3" placeholder="Nama Lowongan">
					

				
					<input type="hidden" name="industri" class="form-control" value="<?php echo $this->session->userdata('id_industri');?>" id="inputEmail3" placeholder="Nama Lowongan">
					

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Deadline</label>
					<div class="col-sm-8">
					<input type="date" required="required" name="deadline" class="form-control" id="inputEmail3" placeholder="Deadline" >
					</div>	
					</div>
				
				<hr><div class="center gap" align="center">
                      <button type="submit" class="btn btn-theme btn-lg">Daftar</button>
                </div> 
				</form>
            </div>
        </div>
    </section>