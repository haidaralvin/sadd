<section id="contact-page" class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="center gap">
<h3 style="font-weight: normal;font-size: 16px;border: solid 1px #ff0047;padding: 15px;box-shadow: 1px 1px 5px #d6d6d6;background: rgba(255, 255, 255, 0.07);border-left: solid 5px #ff0047;">
               Pelamar Dengan Nama <b><?php echo $pencaker->nama_lengkap; ?></b> telah melamar lowongan <b><?php echo $lowongan->nama_lowongan;?></b>. Jika anda berminat dengan pencari kerja ini, silahkan tekan tombol minat.</h4>
                    </div>               
                </div>
            </div>
        <div class="row">
                <div class="col-md-12">
                    <table class="table">                                
                                
                                <tr>
                                    <td rowspan="50" width="20%"><img width="200" height="250"src="<?php echo base_url();?>upload/foto_pencaker/<?php echo $pencaker->foto;?>"</td>
                                    
                                </tr>
                                <tr>                                    
                                    <td>Tanggal Lamar: <?php echo $lamar->tanggal_lamar; ?></td>
                                </tr>  

                                 <tr>                                    
                                    <td>Nama : <?php echo $pencaker->nama_lengkap; ?></td>
                                </tr>                                
                                
                                <tr>

                                    <td>Gender : <?php echo $pencaker->jk; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Agama : <?php echo $pencaker->agama; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Status : <?php echo $pencaker->status; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>TTL : <?php echo $pencaker->tanggal_lahir; ?>, <?php echo $pencaker->tempat_lahir; ?></td>
                                </tr>
                                
                                
                                <tr>
                                    <td>Tanggal Terdaftar : <?php echo $pencaker->tanggal; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Kecamatan : <?php echo $pencaker->nama_kecamatan; ?></td>
                                </tr>
                                <tr>
                                    <td>Pendidikan : <?php echo $pencaker->nama_pendidikan; ?></td>
                                </tr>
                                <tr>
                                    <td>Pengalaman : <?php echo $pencaker->pengalaman; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Usia : <?php echo getOld($pencaker->tanggal_lahir); ?> Tahun</td>
                                </tr>
                                <tr>
                                    <td>Bidang Keahlian : <?php echo $pencaker->bidang_keahlian; ?></td>
                                </tr>
                                <tr>
                                    <td>Posisi : <?php echo $pencaker->nama_posisi; ?></td>
                                </tr>
                                <tr>
                                    <td>Gaji : <?php echo $pencaker->gaji; ?></td>
                                </tr>

                                <tr>
                                    <td>Keterangan : <?php echo $pencaker->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td>CV : <a href="<?php echo base_url();?>upload/cv/<?php echo $pencaker->cv;?>"><?php echo $pencaker->cv;?></td>
                                </tr>
                                <tr>
                                    <td>Scan Ijazah : <img width="400" src="<?php echo base_url();?>upload/scanijazah/<?php echo $pencaker->scanijazah;?>"></td>
                                </tr>
                                
                                <tr>
                                    <td>Scan KTP : <img width="400" src="<?php echo base_url();?>upload/scanktp/<?php echo $pencaker->scanktp;?>"></td>
                                </tr>
                                                             
                                    <td>
                                    <!-- tambahkan tombol minat -->
                                    <a class="btn btn-large btn-success" href="<?php echo site_url('industri/minat_pencaker?id_pencaker='.$pencaker->id_pencaker)?>"><span class="glyphicon glyphicon-ok"></span> Minat</a>

                                        <a  class="btn btn-large btn-primary" href="<?php echo site_url("industri/see_notifikasi")?>"><span class="glyphicon glyphicon-home"></span> Back</a>


                                    </td>
                                </tr>
                                
                            </table>            
                </div>
            </div>
    </section>