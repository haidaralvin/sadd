        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Kategori</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <button class="btn btn-primary" onclick="window.location.href='<?php echo base_url();?>link/add_kategori'" >Tambah Data</button>
                            <br/><br/>
                            <table class="table table-striped" align="center">
                                <tr class="success">
                                        <th>Nama Kategori</th>
                                        <th>Action</th>
                                </tr>
                                <?php $no = $offset;
                                // Memastikan jika data tidak kosong
                                if ($data['query']->num_rows() > 0) {
                                foreach ($data['query']->result() as $row) {?>
                                <tr>
                                        <td><?php echo $row->nama_kategori; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('link/edit_kategori?id_kategori='.$row->id_kategori)?>"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a href="<?php echo site_url('link/delete_kategori?id_kategori='.$row->id_kategori)?>" onclick="return confirm('Yakin ingin menghapus data ini?')"><span class="glyphicon glyphicon-trash"></span></a>
                                        </td>
                                </tr>
                                <?php } } // jika data masih kosong, tampilkan pesan
                                else {
                                    echo "<tr><td colspan='2' align='center'>Data masih kosong !</td></tr>";
                                }
                                ?>
                                
                                
                            </table>   
                            <div class="btn-toolbar" role="toolbar"><div class="btn-group"><?php echo $this->pagination->create_links(); ?></div></div>  
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>