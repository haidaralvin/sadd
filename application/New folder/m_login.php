<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class M_login extends CI_Model {  

  public function __construct()
  {
    parent::__construct();
  }
  
  
  public function ambilPengguna($username, $password)
  {
    $this->db->select('*');
    $this->db->from('admin');
    $this->db->where('username', $username);
    $this->db->where('password', $password);
    $query = $this->db->get();
    
    return $query->row_array();
  }
    
  public function dataPengguna($username)
  {
   $this->db->select('id_admin');	
   $this->db->select('username');
   $this->db->select('nama_admin');
   $this->db->select('id_kecamatan');
   $this->db->select('level');
   $this->db->where('username', $username);
   $query = $this->db->get('admin');
   
   return $query->row();
  }

}
?>