<section id="contact-page" class="container">

<h3 style="font-weight: normal;font-size: 16px;border: solid 1px #ff0047;padding: 15px;box-shadow: 1px 1px 5px #d6d6d6;background: rgba(255, 255, 255, 0.07);border-left: solid 5px #ff0047;">
Perusahaan <b><?php echo $industri->nama_perusahaan; ?></b> berminat kepada anda, Selanjutnya silahkan tunggu perusahaan tersebut menghubungi anda.</h3>        

<h4>Detail Notifikasi Minat</h4>
        <div class="industri">
                <div class="col-md-9">
    <img class="img-rounded" style="max-height: 100px" src="<?php echo base_url();?>upload/foto_industri/<?php echo $industri->foto_industri;?>"/>
                    <table class="table table-striped" align="center">                                
                                               
                                
                                <tr>
                                    
                                    <td>Tanggal Berminat: <?php echo $minat->tanggal_minat; ?></td>
                                </tr>                                
                                <tr>
                                    
                                    <td>Nama  Perusahaan: <?php echo $industri->nama_perusahaan; ?></td>
                                </tr>                                
                                <tr>
                                    <td>Alamat : <?php echo $industri->alamat; ?></td>
                                </tr>
                                
                                <tr>
                                    <td>Kecamatan : <?php echo $industri->nama_kecamatan;?> </td>
                                    
                                </tr>
                                
                                <tr>
                                    <td>Pendiri : <?php echo $industri->nama_pemimpin; ?></td>
                                </tr>
                                <tr>
                                    <td>Tahun Berdiri : <?php echo $industri->tahun_berdiri; ?></td>
                                </tr>
                                <tr>
                                    <td>Bidang Usaha : <?php echo $industri->nama_sub;?> </td>
                                </tr>
                                <tr>
                                    <td>Keterangan : <?php echo $industri->keterangan; ?></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Terdaftar : <?php echo $industri->tanggal; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="<?php echo site_url("pencaker/see_notifikasi")?>"><span class="glyphicon glyphicon-home"></span> Back</a>
                                    </td>
                                </tr>
                                
                            </table>    
                

                             
                        </div>
                    </div>
    </section>