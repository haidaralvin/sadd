/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : dbbursakerja_test

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2017-03-20 23:15:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id_admin` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(12) NOT NULL,
  `nama_admin` varchar(50) NOT NULL,
  `id_kecamatan` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_admin`),
  KEY `id_kecamatan` (`id_kecamatan`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'superadmin', 'e10adc3949ba59abbe56e057f20f883e', 'superadmin', 'Super Admin', '0');
INSERT INTO `admin` VALUES ('2', 'disnaker', 'e10adc3949ba59abbe56e057f20f883e', 'disnaker', 'Disnaker', '0');
INSERT INTO `admin` VALUES ('3', 'disperindag', 'e10adc3949ba59abbe56e057f20f883e', 'disperindag', 'Disperindag', '0');
INSERT INTO `admin` VALUES ('4', 'ampelgading', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'AMPELGADING', '1');
INSERT INTO `admin` VALUES ('5', 'bantur', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'BANTUR', '2');
INSERT INTO `admin` VALUES ('8', 'ngantang', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'NGANTANG', '17');
INSERT INTO `admin` VALUES ('10', 'bululawang', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'BULULAWANG', '3');
INSERT INTO `admin` VALUES ('11', 'dampit', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'DAMPIT', '4');
INSERT INTO `admin` VALUES ('12', 'dau', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'DAU', '5');
INSERT INTO `admin` VALUES ('13', 'donomulyo', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'DONOMULYO', '6');
INSERT INTO `admin` VALUES ('14', 'gedangan', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'GEDANGAN', '7');
INSERT INTO `admin` VALUES ('15', 'godanglegi', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'GODANGLEGI', '8');
INSERT INTO `admin` VALUES ('16', 'jabung', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'JABUNG', '9');
INSERT INTO `admin` VALUES ('17', 'kalipare', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'KALIPARE', '10');
INSERT INTO `admin` VALUES ('18', 'karangploso', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'KARANGPLOSO', '11');
INSERT INTO `admin` VALUES ('19', 'kasembon', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'KASEMBON', '12');
INSERT INTO `admin` VALUES ('20', 'kepanjen', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'KEPANJEN', '13');
INSERT INTO `admin` VALUES ('21', 'kromengan', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'KROMENGAN', '14');
INSERT INTO `admin` VALUES ('22', 'lawang', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'LAWANG', '15');
INSERT INTO `admin` VALUES ('23', 'ngajum', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'NGAJUM', '16');
INSERT INTO `admin` VALUES ('24', 'pagak', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'PAGAK', '18');
INSERT INTO `admin` VALUES ('25', 'pagelaran', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'PAGELARAN', '19');
INSERT INTO `admin` VALUES ('26', 'pakis', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'PAKIS', '20');
INSERT INTO `admin` VALUES ('27', 'pakisaji', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'PAKISAJI', '21');
INSERT INTO `admin` VALUES ('28', 'poncokusumo', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'PONCOKUSUMO', '22');
INSERT INTO `admin` VALUES ('29', 'pujon', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'PUJON', '23');
INSERT INTO `admin` VALUES ('30', 'sumbermanjingwetan', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'SUMBERMANJING WETAN', '24');
INSERT INTO `admin` VALUES ('31', 'singosari', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'SINGOSARI', '25');
INSERT INTO `admin` VALUES ('32', 'sumberpucung', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'SUMBERPUCUNG', '26');
INSERT INTO `admin` VALUES ('33', 'tajinan', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'TAJINAN', '27');
INSERT INTO `admin` VALUES ('34', 'tirtoyudo', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'TIRTOYUDO', '28');
INSERT INTO `admin` VALUES ('35', 'tumpang', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'TUMPANG', '29');
INSERT INTO `admin` VALUES ('36', 'turen', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'TUREN', '30');
INSERT INTO `admin` VALUES ('37', 'wagir', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'WAGIR', '31');
INSERT INTO `admin` VALUES ('38', 'wajak', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'WAJAK', '32');
INSERT INTO `admin` VALUES ('39', 'wonosari', 'e10adc3949ba59abbe56e057f20f883e', 'kecamatan', 'WONOSARI', '33');

-- ----------------------------
-- Table structure for bidang_keahlian
-- ----------------------------
DROP TABLE IF EXISTS `bidang_keahlian`;
CREATE TABLE `bidang_keahlian` (
  `id_bidang` int(3) NOT NULL,
  `bidang_keahlian` varchar(40) NOT NULL,
  PRIMARY KEY (`id_bidang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bidang_keahlian
-- ----------------------------
INSERT INTO `bidang_keahlian` VALUES ('1', 'Agribisnis dan Argoindustri');
INSERT INTO `bidang_keahlian` VALUES ('2', 'Bisnis dan Manajemen');
INSERT INTO `bidang_keahlian` VALUES ('3', 'Digital Media');
INSERT INTO `bidang_keahlian` VALUES ('4', 'Kesehatan');
INSERT INTO `bidang_keahlian` VALUES ('5', 'Pariwisata');
INSERT INTO `bidang_keahlian` VALUES ('6', 'Perikanan dan Kelautan');
INSERT INTO `bidang_keahlian` VALUES ('7', 'Seni Pertunjukan');
INSERT INTO `bidang_keahlian` VALUES ('8', 'Seni Rupa dan Kriya');
INSERT INTO `bidang_keahlian` VALUES ('9', 'Teknologi dan Rekayasa');
INSERT INTO `bidang_keahlian` VALUES ('10', 'Teknologi Informasi dan Komunikasi');

-- ----------------------------
-- Table structure for gaji
-- ----------------------------
DROP TABLE IF EXISTS `gaji`;
CREATE TABLE `gaji` (
  `id_gaji` int(3) NOT NULL,
  `gaji` varchar(30) NOT NULL,
  PRIMARY KEY (`id_gaji`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gaji
-- ----------------------------
INSERT INTO `gaji` VALUES ('1', 'Gaji < Rp 1 juta');
INSERT INTO `gaji` VALUES ('2', 'Rp 1 juta – Rp 2 juta');
INSERT INTO `gaji` VALUES ('3', 'Rp 2 juta – Rp 3 juta');
INSERT INTO `gaji` VALUES ('4', 'Rp 3 juta – Rp 4 juta');
INSERT INTO `gaji` VALUES ('5', 'Rp 4 juta – Rp 5 juta');
INSERT INTO `gaji` VALUES ('6', 'Rp 5 juta – Rp 6 juta');
INSERT INTO `gaji` VALUES ('7', 'Rp 6 juta – Rp 7 juta');
INSERT INTO `gaji` VALUES ('8', 'Gaji > Rp 7 juta');

-- ----------------------------
-- Table structure for hasil_perhitungan
-- ----------------------------
DROP TABLE IF EXISTS `hasil_perhitungan`;
CREATE TABLE `hasil_perhitungan` (
  `id_pencaker` int(11) DEFAULT NULL,
  `id_lowongan` int(11) DEFAULT NULL,
  `hasil` double(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hasil_perhitungan
-- ----------------------------
INSERT INTO `hasil_perhitungan` VALUES ('30', '1', '1.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '2', '1.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '3', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '4', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '5', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '6', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '7', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '8', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '9', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '10', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '11', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '12', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '13', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '14', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('30', '15', '1.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '1', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '2', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '3', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '4', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '5', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '6', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '7', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '8', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '9', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '10', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '11', '0.17');
INSERT INTO `hasil_perhitungan` VALUES ('39', '12', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '13', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '14', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '15', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '17', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('39', '22', '0.50');
INSERT INTO `hasil_perhitungan` VALUES ('2', '36', '0.40');
INSERT INTO `hasil_perhitungan` VALUES ('2', '37', '0.20');
INSERT INTO `hasil_perhitungan` VALUES ('2', '38', '0.20');
INSERT INTO `hasil_perhitungan` VALUES ('2', '39', '0.20');
INSERT INTO `hasil_perhitungan` VALUES ('40', '1', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '2', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '3', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '4', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '5', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '6', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '7', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '8', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '9', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '10', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '11', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '12', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '13', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '14', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '15', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '17', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('40', '22', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('42', '1', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('42', '2', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('42', '3', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('42', '4', '0.50');
INSERT INTO `hasil_perhitungan` VALUES ('42', '5', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('42', '6', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('42', '7', '0.17');
INSERT INTO `hasil_perhitungan` VALUES ('42', '8', '0.50');
INSERT INTO `hasil_perhitungan` VALUES ('42', '9', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('42', '10', '0.17');
INSERT INTO `hasil_perhitungan` VALUES ('42', '11', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('42', '12', '0.17');
INSERT INTO `hasil_perhitungan` VALUES ('42', '13', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('42', '14', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('42', '15', '0.50');
INSERT INTO `hasil_perhitungan` VALUES ('42', '17', '0.17');
INSERT INTO `hasil_perhitungan` VALUES ('42', '22', '0.50');
INSERT INTO `hasil_perhitungan` VALUES ('36', '1', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('36', '2', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('36', '3', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('36', '4', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('36', '5', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('36', '6', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('36', '7', '0.17');
INSERT INTO `hasil_perhitungan` VALUES ('36', '8', '0.67');
INSERT INTO `hasil_perhitungan` VALUES ('36', '9', '0.33');
INSERT INTO `hasil_perhitungan` VALUES ('36', '10', '0.17');
INSERT INTO `hasil_perhitungan` VALUES ('36', '11', '0.50');
INSERT INTO `hasil_perhitungan` VALUES ('36', '12', '0.17');
INSERT INTO `hasil_perhitungan` VALUES ('36', '13', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('36', '14', '0.00');
INSERT INTO `hasil_perhitungan` VALUES ('36', '15', '0.50');
INSERT INTO `hasil_perhitungan` VALUES ('36', '17', '0.17');
INSERT INTO `hasil_perhitungan` VALUES ('36', '22', '0.67');

-- ----------------------------
-- Table structure for industri
-- ----------------------------
DROP TABLE IF EXISTS `industri`;
CREATE TABLE `industri` (
  `id_industri` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `id_kecamatan` int(3) NOT NULL,
  `tahun_berdiri` int(4) NOT NULL,
  `telepon` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id_subkategori` int(5) NOT NULL,
  `nama_pemimpin` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `foto_industri` varchar(50) NOT NULL,
  `scan_ijin_usaha` varchar(50) NOT NULL,
  `tdp` varchar(50) NOT NULL,
  `ktp` varchar(50) NOT NULL,
  `imb` varchar(50) NOT NULL,
  `ig` varchar(50) NOT NULL,
  `is_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `tgl_conf` date NOT NULL,
  `tgl_siup_exp` date NOT NULL,
  PRIMARY KEY (`id_industri`),
  KEY `id_kecamatan` (`id_kecamatan`),
  KEY `id_subkategori` (`id_subkategori`),
  CONSTRAINT `industri_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`),
  CONSTRAINT `industri_ibfk_2` FOREIGN KEY (`id_subkategori`) REFERENCES `sub_kategori` (`id_subkategori`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of industri
-- ----------------------------
INSERT INTO `industri` VALUES ('1', 'jati', 'e10adc3949ba59abbe56e057f20f883e', 'PT. Jati Mas Indonesia', 'Jl. Randuagung', '25', '1994', '03417683391', 'nisrinazn@gmail.com', '4', 'Santoso Tama', 'Perusahaan Kayu', '2017-02-19 20:33:03', 'jati.jpg', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2017-02-20', '2017-03-31');
INSERT INTO `industri` VALUES ('2', 'minatex', 'e10adc3949ba59abbe56e057f20f883e', 'PT. NEW MINATEX', 'Jalan Indrokilo Utara, Lawang, Malang, Jawa Timur 65215', '15', '1992', '(0341) 42638', 'nisrinazn@gmail.com', '12', 'Randy Purbo', 'PT MInatex, perusahan bergerak di bidang tekstil yang berada di Lawang, Malang.', '2014-09-28 07:56:19', 'minatex.jpg', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2014-09-27', '2017-03-31');
INSERT INTO `industri` VALUES ('3', 'unggul', 'e10adc3949ba59abbe56e057f20f883e', 'PT. Unggul Edi Peni', 'Jl. Raya Mondoroko Singosari Furniture 4', '25', '1992', '0341426380', 'nisrinazn@gmail.com', '3', 'Hidayatulloh', 'PT Unggul Edi Peni, perusahan bergerak di bidang makanan ringan yang berada di Singosari, Malang.', '2014-09-28 07:56:54', 'unggul.jpg', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2014-09-27', '2017-03-31');
INSERT INTO `industri` VALUES ('4', 'bangun', 'e10adc3949ba59abbe56e057f20f883e', 'PT. Bangun Sarana Wreksa', 'Jl. Karangjati Ardimulyo Singosari', '25', '1991', '0341123456', 'nisrinazn@gmail.com', '4', 'Dwi Sandikan Atmaja', 'Perusahaan Furniture', '2014-09-28 16:58:51', 'bangun.jpg', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2014-09-08', '2017-03-31');
INSERT INTO `industri` VALUES ('5', 'beiersdorf', 'e10adc3949ba59abbe56e057f20f883e', 'PT. Beiersdorf Indonesia', 'Jl. Raya Randuagung Singosari', '25', '2000', '0341123456', 'nisrinazn@gmail.com', '7', 'Fardalah Dania', 'Perusahaan Plaster', '2014-09-28 17:02:53', 'beiersdorf.png', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2014-09-08', '2017-03-31');
INSERT INTO `industri` VALUES ('6', 'karya', 'e10adc3949ba59abbe56e057f20f883e', 'PT. Karya Makmur Sentosa', 'Jl. Raya Karangpandan Pakisaji', '25', '2000', '0341123456', 'nisrinazn@gmail.com', '8', 'Jaya Ningrat', 'Perusahaan Biji Kopi', '2014-09-28 17:05:41', 'karya.jpg', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2014-09-08', '2017-03-31');
INSERT INTO `industri` VALUES ('7', 'bentoel', 'e10adc3949ba59abbe56e057f20f883e', 'PT. Bentoel', 'Jl. Raya Karanglo Singosari', '25', '2000', '0341123456', 'nisrinazn@gmail.com', '6', 'Kartiko Sugiono', 'Perusahaan Rokok', '2014-09-28 17:07:43', 'bentoel.jpg', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2014-09-08', '2017-03-31');
INSERT INTO `industri` VALUES ('8', 'green', 'e10adc3949ba59abbe56e057f20f883e', 'PT. Green Fields Indonesia', 'Desa Maduarjo Ngajum', '25', '2000', '0341123456', 'nisrinazn@gmail.com', '15', 'Darmono Aji Pratama', 'Perusahaan Alkohol, Pupuk,\r\n\r\nSpiritus', '2014-09-28 17:10:06', 'green.jpg', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2014-09-08', '2017-03-31');
INSERT INTO `industri` VALUES ('9', 'molindo', 'e10adc3949ba59abbe56e057f20f883e', 'PT. Molindo Raya Industrial', 'Jl. Sumberwaras Lawang', '15', '2000', '0341123456', 'nisrinazn@gmail.com', '14', 'Jaya Ningrat', 'Perusahaan Alkohol, Pupuk,\r\n\r\nSpiritus', '2014-09-28 17:12:35', 'molindo.jpg', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2014-09-08', '2017-03-31');
INSERT INTO `industri` VALUES ('10', 'otsuka', 'e10adc3949ba59abbe56e057f20f883e', 'PT. Otsuka Indonesia', 'Jl. Sumberwaras Lawang', '15', '2000', '0341123456', 'nisrinazn@gmail.com', '15', 'Biandika Dirgantara', 'Perusahaan Cairan Infus,\r\n\r\nPeralatan Medis', '2014-09-28 17:15:02', 'otsuka.jpg', 'siup.jpg', '', 'ktp.jpg', 'imb.jpg', 'ig.jpg', '1', '0', '2014-09-08', '2017-03-31');

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id_kategori` int(5) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(30) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES ('1', 'Administrasi & Layanan');
INSERT INTO `kategori` VALUES ('2', 'Akuntansi & Perpajakan');
INSERT INTO `kategori` VALUES ('3', 'Bank & Keuangan');
INSERT INTO `kategori` VALUES ('4', 'Desain & Kreatif');
INSERT INTO `kategori` VALUES ('5', 'Hospitality');
INSERT INTO `kategori` VALUES ('6', 'Makanan & Minuman');
INSERT INTO `kategori` VALUES ('7', 'Hotel, Travel & Tourism');
INSERT INTO `kategori` VALUES ('8', 'Hukum');
INSERT INTO `kategori` VALUES ('9', 'Kesehatan & Kecantikan');
INSERT INTO `kategori` VALUES ('10', 'Konstruksi & Bangunan');
INSERT INTO `kategori` VALUES ('11', 'Laboratorium');
INSERT INTO `kategori` VALUES ('12', 'Lain-Lain');
INSERT INTO `kategori` VALUES ('13', 'Manajemen Investasi');
INSERT INTO `kategori` VALUES ('14', 'Manufaktur');
INSERT INTO `kategori` VALUES ('15', 'Media & Periklanan');
INSERT INTO `kategori` VALUES ('16', 'Minyak Gas & Energi');
INSERT INTO `kategori` VALUES ('17', 'Pemasaran & Penjualan');
INSERT INTO `kategori` VALUES ('18', 'Pendidikan & Pelatihan');
INSERT INTO `kategori` VALUES ('19', 'Pertanian & Perkebunan');
INSERT INTO `kategori` VALUES ('20', 'Teknik');
INSERT INTO `kategori` VALUES ('21', 'Komputer');
INSERT INTO `kategori` VALUES ('22', 'Telekomunikasi');
INSERT INTO `kategori` VALUES ('23', 'Transpotasi & Logistik');

-- ----------------------------
-- Table structure for kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `kecamatan`;
CREATE TABLE `kecamatan` (
  `id_kecamatan` int(3) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kecamatan
-- ----------------------------
INSERT INTO `kecamatan` VALUES ('0', '0');
INSERT INTO `kecamatan` VALUES ('1', 'Ampelgading');
INSERT INTO `kecamatan` VALUES ('2', 'Bantur');
INSERT INTO `kecamatan` VALUES ('3', 'Bululawang');
INSERT INTO `kecamatan` VALUES ('4', 'Dampit');
INSERT INTO `kecamatan` VALUES ('5', 'Dau');
INSERT INTO `kecamatan` VALUES ('6', 'Donomulyo');
INSERT INTO `kecamatan` VALUES ('7', 'Gedangan');
INSERT INTO `kecamatan` VALUES ('8', 'Godanglegi');
INSERT INTO `kecamatan` VALUES ('9', 'Jabung');
INSERT INTO `kecamatan` VALUES ('10', 'Kalipare');
INSERT INTO `kecamatan` VALUES ('11', 'Karangploso');
INSERT INTO `kecamatan` VALUES ('12', 'Kasembon');
INSERT INTO `kecamatan` VALUES ('13', 'Kepanjen');
INSERT INTO `kecamatan` VALUES ('14', 'Kromengan');
INSERT INTO `kecamatan` VALUES ('15', 'Lawang');
INSERT INTO `kecamatan` VALUES ('16', 'Ngajum');
INSERT INTO `kecamatan` VALUES ('17', 'Ngantang');
INSERT INTO `kecamatan` VALUES ('18', 'Pagak');
INSERT INTO `kecamatan` VALUES ('19', 'Pagelaran');
INSERT INTO `kecamatan` VALUES ('20', 'Pakis');
INSERT INTO `kecamatan` VALUES ('21', 'Pakisaji');
INSERT INTO `kecamatan` VALUES ('22', 'Poncokusumo');
INSERT INTO `kecamatan` VALUES ('23', 'Pujon');
INSERT INTO `kecamatan` VALUES ('24', 'Sumbermanjing Wetan');
INSERT INTO `kecamatan` VALUES ('25', 'Singosari');
INSERT INTO `kecamatan` VALUES ('26', 'Sumberpucung');
INSERT INTO `kecamatan` VALUES ('27', 'Tajinan');
INSERT INTO `kecamatan` VALUES ('28', 'Tirtoyudo');
INSERT INTO `kecamatan` VALUES ('29', 'Tumpang');
INSERT INTO `kecamatan` VALUES ('30', 'Turen');
INSERT INTO `kecamatan` VALUES ('31', 'Wagir');
INSERT INTO `kecamatan` VALUES ('32', 'Wajak');
INSERT INTO `kecamatan` VALUES ('33', 'Wonosari');

-- ----------------------------
-- Table structure for lamar
-- ----------------------------
DROP TABLE IF EXISTS `lamar`;
CREATE TABLE `lamar` (
  `id_lamar` int(10) NOT NULL AUTO_INCREMENT,
  `id_lowongan` int(10) NOT NULL,
  `id_pencaker` int(10) NOT NULL,
  `id_industri` int(5) NOT NULL,
  `is_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `is_read_by_industri` tinyint(1) NOT NULL DEFAULT '0',
  `is_read_by_pencaker` tinyint(1) NOT NULL DEFAULT '0',
  `tanggal_lamar` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_lamar`),
  KEY `id_lowongan` (`id_lowongan`),
  KEY `id_pencaker` (`id_pencaker`),
  KEY `id_industri` (`id_industri`),
  CONSTRAINT `lamar_ibfk_1` FOREIGN KEY (`id_lowongan`) REFERENCES `lowongan` (`id_lowongan`),
  CONSTRAINT `lamar_ibfk_2` FOREIGN KEY (`id_pencaker`) REFERENCES `pencaker` (`id_pencaker`),
  CONSTRAINT `lamar_ibfk_3` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lamar
-- ----------------------------

-- ----------------------------
-- Table structure for lowongan
-- ----------------------------
DROP TABLE IF EXISTS `lowongan`;
CREATE TABLE `lowongan` (
  `id_lowongan` int(10) NOT NULL AUTO_INCREMENT,
  `nama_lowongan` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `id_posisi` int(5) NOT NULL,
  `id_kecamatan` int(3) NOT NULL,
  `id_industri` int(5) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deadline` date NOT NULL,
  `is_deadline` tinyint(1) NOT NULL DEFAULT '0',
  `id_bidang` int(3) NOT NULL,
  `id_gaji` int(3) NOT NULL,
  `id_pendidikan` int(3) NOT NULL,
  `id_pengalaman` int(3) NOT NULL,
  `id_usia` int(3) NOT NULL,
  PRIMARY KEY (`id_lowongan`),
  KEY `id_posisi` (`id_posisi`),
  KEY `id_kecamatan` (`id_kecamatan`),
  KEY `id_industri` (`id_industri`),
  KEY `id_bidang` (`id_bidang`),
  KEY `id_gaji` (`id_gaji`),
  KEY `id_pendidikan` (`id_pendidikan`),
  KEY `id_pengalaman` (`id_pengalaman`),
  KEY `id_usia` (`id_usia`),
  CONSTRAINT `lowongan_ibfk_1` FOREIGN KEY (`id_posisi`) REFERENCES `posisi` (`id_posisi`),
  CONSTRAINT `lowongan_ibfk_2` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`),
  CONSTRAINT `lowongan_ibfk_3` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`),
  CONSTRAINT `lowongan_ibfk_4` FOREIGN KEY (`id_bidang`) REFERENCES `bidang_keahlian` (`id_bidang`),
  CONSTRAINT `lowongan_ibfk_5` FOREIGN KEY (`id_gaji`) REFERENCES `gaji` (`id_gaji`),
  CONSTRAINT `lowongan_ibfk_6` FOREIGN KEY (`id_pendidikan`) REFERENCES `pendidikan_terakhir` (`id_pendidikan`),
  CONSTRAINT `lowongan_ibfk_7` FOREIGN KEY (`id_pengalaman`) REFERENCES `pengalaman` (`id_pengalaman`),
  CONSTRAINT `lowongan_ibfk_8` FOREIGN KEY (`id_usia`) REFERENCES `usia` (`id_usia`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lowongan
-- ----------------------------
INSERT INTO `lowongan` VALUES ('11', 'Dibutuhkan Asisten Manajer ', 'Mengembangkan dan mem-prospect pelanggan,pabrik dan proyek baru. Membangun dan mempertahankan hubungan dengan pelanggan-pelanggan lama', '1', '25', '1', '2017-02-19 20:39:03', '2017-03-31', '0', '10', '3', '6', '2', '4');
INSERT INTO `lowongan` VALUES ('12', 'Dibutuhkan Supervisor bagian gudang', 'Tanggung jawab : Menyiapkan pengiriman Barang ke pelanggan. Mengantar barang ke pelanggan. Mengambil barang dari pelanggan. ', '7', '25', '2', '2017-02-19 21:34:21', '2017-04-22', '0', '9', '2', '6', '3', '5');
INSERT INTO `lowongan` VALUES ('13', 'Dibutuhkan Staff IT', 'Mengembangkan Product Parking System. Bersama tim menganalisa aplikasi yang ada. Bersama tim melakukan perbaikan aplikasi ', '8', '15', '3', '2017-02-18 21:52:23', '2017-04-20', '0', '10', '4', '5', '2', '6');
INSERT INTO `lowongan` VALUES ('14', 'Dibutuhkan Segera Staff Operasional', 'Menghitung biaya promosi, membuat program promosi, melakukan kontrol atas realisasi dan pendistribusian barang ', '8', '15', '4', '2017-02-17 21:54:58', '2017-04-29', '0', '9', '4', '6', '1', '4');
INSERT INTO `lowongan` VALUES ('15', 'Dibutuhkan Buruh Gaji UMR + Tunjangan', 'Usia maksimal 32 tahun. Pendidikan minimal SMA. Komunikatif. Memiliki Integritas Yang Baik Dan Sanggup Bekerja Dibawah Tekanan', '6', '25', '5', '2017-02-13 21:59:14', '2017-04-17', '0', '2', '2', '5', '1', '4');
INSERT INTO `lowongan` VALUES ('24', 'Dibutuhkan Teknisi Listrik ', 'Sebagai seoarng Technical Operator Anda memiliki kesempatan untuk menjalankan/mengoperasikan mesin produksi dengan lancar dan efisien', '6', '25', '6', '2017-02-23 00:44:31', '2017-12-31', '0', '4', '3', '5', '5', '3');
INSERT INTO `lowongan` VALUES ('25', 'Dibutuhkan Staff dibagian Keuangan', '<div itemprop=\"description\" class=\"unselectable wrap-text\" id=\"job_description\"> <div><strong>Deskripsi Pekerjaan:</strong></div>\r\n\r\n<ul>\r\n	<li>Bertanggung jawab untuk mengembangkan penjualan dari&nbsp;bisnis credit life (bundling) melalui kemitraan dengan perusahaan pembiayaan atau lembaga keuangan.</li>\r\n</ul>\r\n\r\n\r\n\r\n </div>', '8', '33', '7', '2017-02-23 00:44:53', '2017-12-31', '0', '4', '3', '5', '5', '3');
INSERT INTO `lowongan` VALUES ('26', 'Dibutuhkan Sekertaris Dan Admin', '<div itemprop=\"description\" class=\"unselectable wrap-text\" id=\"job_description\"> <div>Syarat:</div>\r\n\r\n<div>\r\nMemiliki pemahaman terkait legal drafing dan analisa hukum<br>\r\nPernah menjadi staf legal di sebuah perusahaan minimal 3 tahun<br>\r\nPernah magang di kantor advokat minimal 1 tahun<br>\r\nDapat menyelesaikan kasus hukum perusahaan baik perdata maupun pidana<br>\r\nDapat memberikan legal opini terhadap semua bentuk perjanjian dan langkah perusahaan<br>\r\nDapat bekerja secara team</div> </div>', '8', '25', '8', '2017-02-23 00:45:22', '2017-12-31', '0', '4', '3', '5', '5', '3');
INSERT INTO `lowongan` VALUES ('34', 'Dibutuhkan Asisten Direksi Keuangan Cabang', '<div class=\"col-lg-12 col-md-12 col-sm-12\">\r\n			 			\r\n			<div itemprop=\"description\" class=\"unselectable wrap-text\" id=\"job_description\"> <div><strong>Deskripsi Pekerjaan:</strong></div>\r\n\r\n<ul>\r\n	<li>Bertanggung jawab untuk mengembangkan penjualan dari&nbsp;bisnis credit life (bundling) melalui kemitraan dengan perusahaan pembiayaan atau lembaga keuangan.</li>\r\n</ul>\r\n\r\n<div><strong>Kualifikasi:</strong></div>\r\n\r\n<ul>\r\n	<li>Minimum pendidikan S1 dari semua jurusan</li>\r\n	<li>Mempunyai pengalaman minimum 1 tahun sebagai sales</li>\r\n	<li>Percaya diri, mempunyai komunikasi &amp; presentasi yang sangat baik dan berorientasi kepada target</li>\r\n	<li>Berpengalaman di Asuransi Jiwa</li>\r\n</ul> </div>\r\n					</div>', '10', '32', '9', '2017-02-23 01:07:42', '2017-12-31', '0', '4', '3', '5', '5', '3');
INSERT INTO `lowongan` VALUES ('73', 'Dibutuhkan Teknisi Mesin Mekanik', '<div itemprop=\"description\" class=\"unselectable wrap-text\" id=\"job_description\"> <div><strong>Deskripsi Pekerjaan:</strong></div>\r\n\r\n<ul>\r\n	<li>Bertanggung jawab untuk mengembangkan penjualan dari&nbsp;bisnis credit life (bundling) melalui kemitraan dengan perusahaan pembiayaan atau lembaga keuangan.</li>\r\n</ul>\r\n\r\n\r\n\r\n </div>', '4', '25', '10', '2017-03-05 14:39:17', '2019-12-20', '0', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for minat
-- ----------------------------
DROP TABLE IF EXISTS `minat`;
CREATE TABLE `minat` (
  `id_minat` int(10) NOT NULL AUTO_INCREMENT,
  `id_pencaker` int(10) NOT NULL,
  `id_industri` int(5) NOT NULL,
  `is_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `is_read_by_industri` tinyint(1) NOT NULL DEFAULT '0',
  `is_read_by_pencaker` tinyint(1) NOT NULL DEFAULT '0',
  `tanggal_minat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_minat`),
  KEY `id_pencaker` (`id_pencaker`),
  KEY `id_industri` (`id_industri`),
  CONSTRAINT `minat_ibfk_1` FOREIGN KEY (`id_pencaker`) REFERENCES `pencaker` (`id_pencaker`),
  CONSTRAINT `minat_ibfk_2` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of minat
-- ----------------------------

-- ----------------------------
-- Table structure for pencaker
-- ----------------------------
DROP TABLE IF EXISTS `pencaker`;
CREATE TABLE `pencaker` (
  `id_pencaker` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_ktp` varchar(16) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `jk` varchar(12) NOT NULL,
  `agama` varchar(12) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telepon` varchar(12) NOT NULL,
  `id_pendidikan` int(3) NOT NULL,
  `kls` varchar(10) NOT NULL,
  `thn` varchar(10) NOT NULL,
  `sd` varchar(10) NOT NULL,
  `smp` varchar(10) NOT NULL,
  `sma` varchar(10) NOT NULL,
  `d_ii` varchar(10) NOT NULL,
  `akta_ii` varchar(10) NOT NULL,
  `akta_iii` varchar(10) NOT NULL,
  `akta_iv` varchar(10) NOT NULL,
  `akta_v` varchar(10) NOT NULL,
  `id_pengalaman` int(3) NOT NULL,
  `keterangan` text NOT NULL,
  `status` varchar(15) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cv` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `scanijazah` varchar(50) NOT NULL,
  `scanktp` varchar(50) NOT NULL,
  `id_kecamatan` int(3) NOT NULL,
  `is_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `is_ambil` tinyint(1) NOT NULL DEFAULT '0',
  `tgl_conf` date NOT NULL,
  `id_bidang` int(3) NOT NULL,
  `id_gaji` int(3) NOT NULL,
  `id_posisi` int(3) NOT NULL,
  `usia` varchar(30) NOT NULL,
  PRIMARY KEY (`id_pencaker`),
  KEY `id_kecamatan` (`id_kecamatan`),
  KEY `id_posisi` (`id_posisi`),
  KEY `id_pendidikan` (`id_pendidikan`),
  KEY `id_bidang` (`id_bidang`),
  KEY `id_gaji` (`id_gaji`),
  KEY `id_pengalaman` (`id_pengalaman`),
  CONSTRAINT `pencaker_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`),
  CONSTRAINT `pencaker_ibfk_2` FOREIGN KEY (`id_pendidikan`) REFERENCES `pendidikan_terakhir` (`id_pendidikan`),
  CONSTRAINT `pencaker_ibfk_3` FOREIGN KEY (`id_posisi`) REFERENCES `posisi` (`id_posisi`),
  CONSTRAINT `pencaker_ibfk_4` FOREIGN KEY (`id_bidang`) REFERENCES `bidang_keahlian` (`id_bidang`),
  CONSTRAINT `pencaker_ibfk_5` FOREIGN KEY (`id_gaji`) REFERENCES `gaji` (`id_gaji`),
  CONSTRAINT `pencaker_ibfk_6` FOREIGN KEY (`id_pengalaman`) REFERENCES `pengalaman` (`id_pengalaman`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pencaker
-- ----------------------------
INSERT INTO `pencaker` VALUES ('11', 'parinata', 'e10adc3949ba59abbe56e057f20f883e', '3507220504930001', 'Bangun Parinata', '1993-07-14', 'malang', 'Laki - laki', 'Islam', 'Jl. Tirto Rahayu', 'nisrinazn@gmail.com', '097752617267', '6', '', '', '2005', '2008', '2011', '', '', '', '2015', '', '2', '', 'Belum Menikah', '2017-02-20 07:38:35', 'contoh CV.doc', 'cowok1.jpg', 'Ijazah.jpg', 'ktp_contoh.jpg', '33', '1', '1', '2017-02-20', '2', '2', '1', '0');
INSERT INTO `pencaker` VALUES ('12', 'izzah', 'e10adc3949ba59abbe56e057f20f883e', '3507224102640002', 'Amila Zizah', '1993-07-14', 'malang', 'Perempuan', 'Islam', 'Perum Landungsari Asri C - 58', 'nisrinazn@gmail.com', '081245645432', '5', '', '', '1976', '1979', '1982', '', '', '', '1987', '', '3', '', 'Menikah', '2017-02-15 06:48:46', 'contoh CV.doc', 'cewek1.jpg', 'Ijazah.jpg', 'ktp_contoh.jpg', '32', '1', '0', '2017-02-20', '5', '3', '3', '0');
INSERT INTO `pencaker` VALUES ('13', 'oktavia', 'e10adc3949ba59abbe56e057f20f883e', '3507226011840001', 'Octavia Tursainah', '1993-07-14', 'malang', 'Perempuan', 'Islam', 'Jl. Tirto Utomo', 'nisrinazn@gmail.com', '087852416322', '5', '', '', '1996', '1999', '2002', '', '', '', '2005', '', '2', '', 'Belum Menikah', '2017-02-20 07:10:44', 'contoh CV.doc', 'cewek2.jpg', 'Ijazah.jpg', 'ktp_contoh.jpg', '32', '1', '1', '2017-02-19', '10', '2', '1', '0');
INSERT INTO `pencaker` VALUES ('14', 'suhendra', 'e10adc3949ba59abbe56e057f20f883e', '3507220312940001', 'M Hadi Suhendra', '1993-07-14', 'jombang', 'Laki - laki', 'Islam', 'Jl. Tirto Utomo', 'nisrinazn@gmail.com', '089942513425', '6', '', '', '2006', '2009', '2012', '', '', '', '2017', '', '2', '', 'Belum Menikah', '2017-02-20 07:18:02', 'contoh CV.doc', 'cowok2.jpg', 'Ijazah.jpg', 'ktp_contoh.jpg', '15', '1', '1', '2017-02-20', '10', '3', '1', '0');
INSERT INTO `pencaker` VALUES ('15', 'syaiful', 'e10adc3949ba59abbe56e057f20f883e', '3507221903879002', 'Achmad Syaiful', '1993-07-14', 'malang', 'Laki - laki', 'Islam', 'Jl. Tirto Utomo 94', 'nisrinazn@gmail.com', '081367538928', '6', '', '', '1999', '2002', '2005', '', '', '', '2009', '', '1', '', 'Belum Menikah', '2017-02-20 07:28:19', 'contoh CV.doc', 'cowok3.jpg', 'Ijazah.jpg', 'ktp_contoh.jpg', '15', '1', '0', '2017-02-20', '10', '4', '8', '0');
INSERT INTO `pencaker` VALUES ('18', 'nabila', 'e10adc3949ba59abbe56e057f20f883e', '3507221903870002', 'Nabila Aini', '1993-07-14', 'Malang', 'Perempuan', 'Islam', 'wagir', 'nisrinazn@gmail.com', '085755598020', '5', '10', '2004', '2005', '2007', '2010', '2015', '2016', '', '', '', '1', '', 'Belum Menikah', '2017-02-21 22:45:21', 'contoh CV.doc', 'cewek3.jpg', 'Ijazah.jpg', 'ktp_contoh.jpg', '25', '1', '1', '0000-00-00', '10', '2', '1', '');
INSERT INTO `pencaker` VALUES ('19', 'nisrina', 'e10adc3949ba59abbe56e057f20f883e', '3507221903870203', 'Nis Rina', '1993-07-14', 'Malang', 'Perempuan', 'Islam', 'malang', 'nisrinazn@gmail.com', '085755598020', '6', '', '', '2005', '2007', '2010', '', '', '', '', '', '3', '', 'Menikah', '2017-02-22 22:18:36', 'contoh CV.doc', 'cewek4.jpg', 'Ijazah.jpg', 'ktp_contoh.jpg', '25', '1', '1', '0000-00-00', '10', '2', '8', '');
INSERT INTO `pencaker` VALUES ('20', 'agnes', 'e10adc3949ba59abbe56e057f20f883e', '3507221903877702', 'Agnes Elizabeth', '1993-07-14', 'malang', 'Perempuan', 'Islam', 'malang', 'nisrinazn@gmail.com', '085755598020', '1', '', '2004', '2005', '2007', '2010', '', '', '', '', '', '1', '', 'Menikah', '2017-03-15 12:29:20', 'contoh CV.doc', 'cewek5.jpg', 'Ijazah.jpg', 'ktp_contoh.jpg', '25', '1', '0', '0000-00-00', '1', '1', '1', '');
INSERT INTO `pencaker` VALUES ('21', 'rudi', 'e10adc3949ba59abbe56e057f20f883e', '1200032999202099', 'Rudi Hartono', '1993-07-14', 'Malang', 'Laki-Laki', 'Islam', 'jl sengon rejo', 'nisrinazn@gmail.com', '085755598020', '6', '10', '2004', '2005', '2007', '2010', '2015', '2016', '', '', '', '2', '', 'Belum Menikah', '2017-03-20 20:28:21', 'contoh CV.doc', 'cowok5.jpg', 'Ijazah.jpg', 'ktp_contoh.jpg', '25', '1', '0', '0000-00-00', '2', '1', '1', '');

-- ----------------------------
-- Table structure for pendidikan_terakhir
-- ----------------------------
DROP TABLE IF EXISTS `pendidikan_terakhir`;
CREATE TABLE `pendidikan_terakhir` (
  `id_pendidikan` int(11) NOT NULL,
  `nama_pendidikan` varchar(11) NOT NULL,
  PRIMARY KEY (`id_pendidikan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pendidikan_terakhir
-- ----------------------------
INSERT INTO `pendidikan_terakhir` VALUES ('1', 'SD');
INSERT INTO `pendidikan_terakhir` VALUES ('2', 'SMP/MTS');
INSERT INTO `pendidikan_terakhir` VALUES ('3', 'SMA/SMK/MA');
INSERT INTO `pendidikan_terakhir` VALUES ('4', 'D1/D2');
INSERT INTO `pendidikan_terakhir` VALUES ('5', 'D3');
INSERT INTO `pendidikan_terakhir` VALUES ('6', 'S1');
INSERT INTO `pendidikan_terakhir` VALUES ('7', 'S2');
INSERT INTO `pendidikan_terakhir` VALUES ('8', 'S3');

-- ----------------------------
-- Table structure for pengalaman
-- ----------------------------
DROP TABLE IF EXISTS `pengalaman`;
CREATE TABLE `pengalaman` (
  `id_pengalaman` int(3) NOT NULL,
  `pengalaman` varchar(20) NOT NULL,
  PRIMARY KEY (`id_pengalaman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengalaman
-- ----------------------------
INSERT INTO `pengalaman` VALUES ('1', '0 tahun');
INSERT INTO `pengalaman` VALUES ('2', '1 - 3 tahun');
INSERT INTO `pengalaman` VALUES ('3', '3 - 5 tahun');
INSERT INTO `pengalaman` VALUES ('4', '5 - 7 tahun');
INSERT INTO `pengalaman` VALUES ('5', '7 - 9 tahun');
INSERT INTO `pengalaman` VALUES ('6', '9 - 10 tahun');
INSERT INTO `pengalaman` VALUES ('7', '10 - 12 tahun');
INSERT INTO `pengalaman` VALUES ('8', '> 12 tahun');

-- ----------------------------
-- Table structure for posisi
-- ----------------------------
DROP TABLE IF EXISTS `posisi`;
CREATE TABLE `posisi` (
  `id_posisi` int(3) NOT NULL AUTO_INCREMENT,
  `nama_posisi` varchar(30) NOT NULL,
  PRIMARY KEY (`id_posisi`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of posisi
-- ----------------------------
INSERT INTO `posisi` VALUES ('1', 'Asisten Manajer');
INSERT INTO `posisi` VALUES ('2', 'Kepala Bidang');
INSERT INTO `posisi` VALUES ('3', 'Koordinator');
INSERT INTO `posisi` VALUES ('4', 'General Manager');
INSERT INTO `posisi` VALUES ('5', 'Manajer');
INSERT INTO `posisi` VALUES ('6', 'Buruh');
INSERT INTO `posisi` VALUES ('7', 'Supervisor');
INSERT INTO `posisi` VALUES ('8', 'Karyawan Staf');
INSERT INTO `posisi` VALUES ('9', 'Direktur');
INSERT INTO `posisi` VALUES ('10', 'Asisten Direksi');

-- ----------------------------
-- Table structure for sub_kategori
-- ----------------------------
DROP TABLE IF EXISTS `sub_kategori`;
CREATE TABLE `sub_kategori` (
  `id_subkategori` int(5) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(5) NOT NULL,
  `nama_sub` varchar(30) NOT NULL,
  PRIMARY KEY (`id_subkategori`),
  KEY `id_kategori` (`id_kategori`),
  CONSTRAINT `sub_kategori_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sub_kategori
-- ----------------------------
INSERT INTO `sub_kategori` VALUES ('1', '3', 'Bank');
INSERT INTO `sub_kategori` VALUES ('2', '23', 'Bus');
INSERT INTO `sub_kategori` VALUES ('3', '6', 'Furniture');
INSERT INTO `sub_kategori` VALUES ('4', '4', 'Arsitektur');
INSERT INTO `sub_kategori` VALUES ('5', '7', 'Hotel');
INSERT INTO `sub_kategori` VALUES ('6', '5', 'Rokok');
INSERT INTO `sub_kategori` VALUES ('7', '9', 'Kosmetik');
INSERT INTO `sub_kategori` VALUES ('8', '15', 'Audio dan TV');
INSERT INTO `sub_kategori` VALUES ('9', '21', 'Hardware');
INSERT INTO `sub_kategori` VALUES ('10', '21', 'Software');
INSERT INTO `sub_kategori` VALUES ('11', '14', 'Plastik');
INSERT INTO `sub_kategori` VALUES ('12', '14', 'Tekstil');
INSERT INTO `sub_kategori` VALUES ('13', '14', 'otomotif');
INSERT INTO `sub_kategori` VALUES ('14', '14', 'Bahan Kimia');
INSERT INTO `sub_kategori` VALUES ('15', '6', 'Minuman');

-- ----------------------------
-- Table structure for usia
-- ----------------------------
DROP TABLE IF EXISTS `usia`;
CREATE TABLE `usia` (
  `id_usia` int(3) NOT NULL,
  `usia` varchar(30) NOT NULL,
  `batas_max` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`id_usia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usia
-- ----------------------------
INSERT INTO `usia` VALUES ('1', 'usia < 13 tahun', '12');
INSERT INTO `usia` VALUES ('2', '13 - 15 tahun', '15');
INSERT INTO `usia` VALUES ('3', '16 - 19 tahun', '19');
INSERT INTO `usia` VALUES ('4', '20 - 24 tahun', '24');
INSERT INTO `usia` VALUES ('5', '25 - 29 tahun', '29');
INSERT INTO `usia` VALUES ('6', '30 - 34 tahun', '34');
INSERT INTO `usia` VALUES ('7', '35 - 49 tahun', '49');
INSERT INTO `usia` VALUES ('8', '50 - 64 tahun', '64');
