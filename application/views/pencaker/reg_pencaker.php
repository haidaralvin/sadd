<section id="contact-page" class="container">
        
		<div class="row">
            <div class="col-sm-8">
            <font style="color:black;">
            		<?php
            		date_default_timezone_set("Asia/Jakarta");
                    /* script menentukan hari */  
						 $array_hr= array(1=>"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
						 $hr = $array_hr[date('N')];
					/* script menentukan tanggal */    
						$tgl= date('j');
					/* script menentukan bulan */ 
					  	$array_bln = array(1=>"Januari","Februari","Maret", "April", "Mei","Juni","Juli","Agustus","September","Oktober", "November","Desember");
					  	$bln = $array_bln[date('n')];
					/* script menentukan tahun */ 
						$thn = date('Y');
					/* script perintah keluaran*/ 
					 	echo "<strong>".$hr . ", " . $tgl . " " . $bln . " " . $thn . "</strong>";
					 ?><div id="time"></div></font>
            <hr>
            	<div class="center gap">
                      <h3 align="center">Data Pencari Kerja</h3>
                </div>
				<form class="form-horizontal" role="form" method="post" <?php echo @$error; ?><?php echo form_open_multipart('pencaker/pro_add_pencaker') ;?>
				  
				<div class="form-group">
					<label class="col-sm-2 control-label">Username</label>
					<div class="col-sm-8">
					<input type="text" required='required' name="username" class="form-control" maxlength="25" placeholder="Username">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Password</label>
					<div class="col-sm-8">
					<input type="password" required='required' name="pass" class="form-control" maxlength="25" placeholder="Password">
					</div>
				</div>
			<hr>
				<div class="center gap">
                      <h3 align="center">Data Pribadi</h3>
                </div> 
				<div class="form-group">
					<label  class="col-sm-2 control-label">NIK</label>
					<div class="col-sm-8">
					<input type="text" required='required' name="no_ktp" class="form-control" maxlength="16" placeholder="NIK">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Nama Lengkap</label>
					<div class="col-sm-8">
					<input type="text" required='required' name="nama_lengkap" class="form-control"  placeholder="Nama Lengkap">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Tanggal Lahir</label>
					<div class="col-sm-8">
					<input type="date" required='required' name="tanggal_lahir" class="form-control"  placeholder="yyyy-mm-dd">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Tempat Lahir</label>
					<div class="col-sm-8">
					<input type="text" required='required' name="tempat_lahir" class="form-control"  placeholder="Tempat Lahir">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Jenis Kelamin</label>
					<div class="col-sm-8">
					<label class="radio-inline">
  						<input type="radio" name="jk" value="Laki-Laki" id="inlineRadio1" value="option1" checked> Laki - Laki</label>
  					<label class="radio-inline">
  						<input type="radio" name="jk" value="Perempuan" id="inlineRadio2" value="option2"> Perempuan</label>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Status</label>
					<div class="col-sm-8">
					<select required='required' name="status" class="form-control">
						<option>Menikah</option>
						<option>Belum Menikah</option>
					</select>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Agama</label>
					<div class="col-sm-8">
					<select required='required' name="agama" class="form-control">
						<option>Islam</option>
						<option>Kristen</option>
						<option>Katolik</option>
						<option>Hindu</option>
						<option>Budha</option>
					</select>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Email</label>
					<div class="col-sm-8">
					<input required='required' type="email" name="email" class="form-control"  placeholder="Email">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Telepon</label>
					<div class="col-sm-8">
					<input required='required' type="text" name="telepon" class="form-control" maxlength="12"  placeholder="Telepon">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Alamat</label>
					<div class="col-sm-8">
					<input required='required' type="text" name="alamat" class="form-control"  placeholder="Alamat">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Kecamatan</label>
					<div class="col-sm-8">
					<select required='required' name="id_kecamatan" class="form-control">
						<?php foreach ($kecamatan as $row): ?>
                                            <option value="<?php echo $row->id_kecamatan ;?>">&nbsp;<?php echo $row->nama_kecamatan; ?></option>
                                         <?php endforeach;?>
					</select>

					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Pend.Terakhir</label>
					<div class="col-sm-8">
					<select required='required' name="pendidikan_terakhir" class="form-control">
						<?php foreach ($pendidikan_terakhir as $row): ?>
                                            <option value="<?php echo $row->id_pendidikan ;?>">&nbsp;<?php echo $row->nama_pendidikan; ?></option>
                                         <?php endforeach;?>						
					</select>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Bidang Keahlian</label>
					<div class="col-sm-8">
					<select required='required' name="bidang_keahlian" class="form-control">
						<?php foreach ($bidang_keahlian as $row): ?>
                                            <option value="<?php echo $row->id_bidang ;?>">&nbsp;<?php echo $row->bidang_keahlian; ?></option>
                                         <?php endforeach;?>
					</select>
					</div>
				</div>

				<hr>
				<div class="center gap">
                      <h3 align="center">Pendidikan Formal/Non Formal</h3>
                </div>
                <div class="form-group">
					<label  class="col-sm-2 control-label">Tidak Tamat SD/Sampai Kelas</label>
					<div class="col-sm-2">
					&nbsp;<input type="text" name="kls" class="form-control"  placeholder="Kelas">
					</div>
					<div class="col-sm-2">
					&nbsp;<input type="text" name="thn" class="form-control"  placeholder="Tahun">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">SD/Sederajat</label>
					<div class="col-sm-2">
					<input type="text" name="sd" class="form-control"  placeholder="Tahun">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">SMTP/Sederajat</label>
					<div class="col-sm-2">
					<input type="text" name="smp" class="form-control"  placeholder="Tahun">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">SMTA/DI/
					AKTA&nbsp;I</label>
					<div class="col-sm-2">
					<input type="text" name="sma" class="form-control"  placeholder="Tahun">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">SM/DII/DIII</label>
					<div class="col-sm-2">
					<input type="text" name="d_ii" class="form-control"  placeholder="Tahun">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">AKTA II</label>
					<div class="col-sm-2">
					<input type="text" name="akta_ii" class="form-control"  placeholder="Tahun">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">AKTA III</label>
					<div class="col-sm-2">
					<input type="text" name="akta_iii" class="form-control"  placeholder="Tahun">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">S/PASCA/SI/
					AKTA IV/D IV</label>
					<div class="col-sm-2">
					<input type="text" name="akta_iv" class="form-control"  placeholder="Tahun">
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">DOKTOR/SII/
					AKTA V</label>
					<div class="col-sm-2">
					<input type="text" name="akta_v" class="form-control"  placeholder="Tahun">
					</div>
				</div>
                </hr>
				<hr>
				<div class="center gap">
                      <h3 align="center">Unggah Berkas</h3>
                </div> 
                <font color="red">*) Ukuran File Max.500Kb</font><br>
                <div class="form-group">
					<label  class="col-sm-2 control-label">CV </label>
					<label  class="col-sm-2 control-label"><input type="file" name="cv"></label>
				</div>
				<font color="red">*) Ukuran foto Max.200x200 pixels</font><br>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Foto</label>
					<label  class="col-sm-2 control-label"><input type="file" name="foto"></label>					
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">Ijazah Terakhir</label>
					<label  class="col-sm-2 control-label"><input type="file" name="scanijazah"></label>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">KTP</label>
					<label  class="col-sm-2 control-label"><input type="file" name="scanktp"></label>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Pengalaman</label>
					<div class="col-sm-8">
					<select required='required' name="pengalaman" class="form-control">
						<?php foreach ($pengalaman as $row): ?>
                                            <option value="<?php echo $row->id_pengalaman ;?>">&nbsp;<?php echo $row->pengalaman; ?></option>
                                         <?php endforeach;?>
					</select>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Keterangan</label>
					<div class="col-sm-8">
					<textarea name="keterangan" class="form-control" rows="5"></textarea>
					</div>
				</div>

				<hr>
            	<div class="center gap">
                      <h3 align="center">Yang Diharapkan</h3>
                </div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Posisi</label>
					<div class="col-sm-8">
					<select name="id_posisi" class="form-control">
						<?php foreach ($posisi as $row): ?>
                                            <option value="<?php echo $row->id_posisi ;?>">&nbsp;<?php echo $row->nama_posisi; ?></option>
                                         <?php endforeach;?>
					</select>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">Gaji</label>
					<div class="col-sm-8">
					<select name="gaji" class="form-control">
						<?php foreach ($gaji as $row): ?>
                                            <option value="<?php echo $row->id_gaji ;?>">&nbsp;<?php echo $row->gaji; ?></option>
                                         <?php endforeach;?>
					</select>
					</div>
				</div>


				<hr><div class="center gap" align="center">
                      <button type="submit" class="btn btn-theme btn-lg">Daftar</button>
                </div> 
				</form>
            </div>
        </div>
    </section>